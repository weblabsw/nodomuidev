import { Module } from '/dist/nodomui.min.js'
export class buttonex1 extends Module {
    template(props) {
        return `
            <div>
                <ui-button size="small"  title='small' class="item"/> &nbsp;
                <ui-button size="normal"  title='normal' class="item"/> &nbsp;
                <ui-button size="large"  title='large' class="item"/> &nbsp;
            </div>
            <style>
                .item{
                    margin-right:30px;
                    margin-bottom:10px;
                }
            </style>
        `;
    }
}