import { Module } from "/dist/nodomui.min.js";
export class progressex5 extends Module {
    template() {
        return `
          <div style='padding:15px'>
            <div style='width:300px'>
                <ui-progress percent={{percent}} style='margin-bottom: 10px' format={{format}}/>
            </div>
            <ui-button e-click="subPercent" title='-' />
              <ui-button e-click="addPercent" title='+' />
          </div>
        `
    }
    data() {
        return {
            percent: 10,
            format: (perc) => {
                return `<div style='color:red'>
                <b class='ui-icon-collect-fill'></b>
                <span >{{percent}}%</span>
                </div>`;
            }
        }
    }

    addPercent(model, dom) {
        console.log(this.model.percent);
        this.model.percent += 3;
    }

    subPercent(model, dom) {
        console.log(this.model.percent);
        this.model.percent -= 3;
    }
}
