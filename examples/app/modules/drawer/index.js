import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { drawerex1 } from './ex1.js'
import { drawerex2 } from './ex2.js'
import { drawerex3 } from './ex3.js'
import { drawerex4 } from './ex4.js'
import { drawerex5 } from './ex5.js'

export class MDrawer extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, drawerex1, drawerex2, drawerex3, drawerex4, drawerex5];
    template(props) {
        return `
            <div class='--component'>
                <h1>Drawer 抽屉</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='drawer'/>
                </div>
        
                <h2>属性</h2>
                <h3>UIDrawer/ui-drawer</h3>
                <AttrGrid $rows={{DrawerAttrs}} />


                <h2>事件</h2>
                <h3>UIDrawer/ui-drawer</h3>
                <EventGrid $rows={{DrawerEvents}} />

                <h2>方法</h2>
                <h3>UIDrawer/ui-drawer</h3>
                <MethodGrid $rows={{DrawerMethods}} />
                <ui-messagebox />
            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}