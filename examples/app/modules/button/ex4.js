import { Module } from '/dist/nodomui.min.js'
export class buttonex4 extends Module {
    template(props) {
        return `
            <div>
                <ui-button icon='arrow-right' circle class="item"/>
                <ui-button icon='edit' theme='active' circle class="item"/>
                <ui-button icon='select' theme='success' circle class="item"/>
                <ui-button icon='ashbin' theme='error' circle class="item"/>
                <ui-button icon='warning' theme='warn' circle class="item"/>

                <style>
                    .item{
                        margin-right:30px;
                        margin-bottom:10px;
                    }
                </style>
            </div>
        `;
    }
}