import { Nodom, Module } from "nodom3";
import { UITool } from "./uibase";
/**
 * 数据项
 * $data             菜单结构数据
 * 参数说明
 * vertical:        是否纵向菜单
 * onItemClick:     菜单项点击事件
 * width:           菜单宽度（px，针对纵向菜单或横向菜单二级及以上菜单）,默认150
 * bgcolor:         菜单背景色（可选）
 * color:           菜单文字色（可选）
 * active-bgcolor:   激活菜单项背景色（可选）
 * active-color:     激活菜单项纹紫色（可选）
 */
/**
 * UIMenu 菜单
 * @public
 */
export class UIMenu extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__vertical = props.hasOwnProperty('vertical');
        this.__onItemClick = props['onitemclick'];
        this.__width = props['width'] && props['width'] !== '' ? parseInt(props['width']) : 150;
        //存储样式
        const styleArr1 = [];
        if (props['bgcolor']) {
            styleArr1.push('background-color:' + props['bgcolor']);
        }
        if (props['color']) {
            styleArr1.push('color:' + props['color']);
        }
        const menuStyle = styleArr1.join(';');
        const styleArr2 = [];
        if (props['active-bgcolor']) {
            styleArr2.push('background-color:' + props['active-bgcolor']);
        }
        if (props['active-color']) {
            styleArr2.push('color:' + props['active-color']);
        }
        //激活样式
        let activeClass = '';
        let activeStyle;
        if (styleArr2.length > 0) {
            activeStyle = "(__active?'" + styleArr2.join(";") + "':'" + styleArr1.join(";") + "')";
        }
        else if (styleArr1.length > 0) {
            activeStyle = "(__active?'':'" + styleArr1.join(";") + "')";
        }
        else {
            //默认激活样式
            activeClass = "+ (__active?' ui-menu-active':'')";
        }
        //菜单style
        let style1 = "style={{" + (this.__vertical ? "'padding-left:' + (__level * 20) + 'px;'" : '');
        if (activeStyle) {
            style1 += " + " + activeStyle;
        }
        style1 += "}}";
        const iconStr = `<b x-show={{children&&children.length>0}} class={{'ui-menu-subicon' + (__open?' ui-expand-icon-open':'')}}/>`;
        this.setExcludeProps(['width', 'onitemclick', 'vertical', 'bgcolor', 'color', 'active-bgcolor', 'active-color']);
        //是否纵向
        if (this.__vertical) { //纵向菜单
            return `
                <div class='ui-menu ui-menu-vert' style='${menuStyle}'>
                    <div class='ui-menu-subct-expand'>
                        <for cond={{data}} class='ui-menu-node-wrap' e-click='__clickMenu:nopopo'>
                            <div class={{'ui-menu-node' ${activeClass}}}
                                ${style1}
                                e-click='__clickMenu:nopopo'>
                                <slot innerRender/>
                                ${iconStr}
                            </div>
                            <recur cond='children' class='ui-expand-vertical' style={{'height:' + __cacHeight(__open,children) + 'px'}} >
                                <div style='${menuStyle}'>
                                    <for cond={{children}}  class='ui-menu-node-wrap'  e-click='__clickMenu:nopopo'>
                                        <div class={{'ui-menu-node' ${activeClass}}} 
                                            ${style1}>
                                            <slot innerRender/>
                                            ${iconStr}
                                        </div>
                                        <recur ref />
                                    </for>
                                </div>
                            </recur>
                        </for>
                    </div>
                </div>
            `;
        }
        else { //横向菜单
            return `
                <div class='ui-menu' style='${menuStyle}'>
                    <div class='ui-menu-hori-first'>
                        <for cond={{data}} 
                            class='ui-menu-node-wrap ui-menu-first' 
                            e-mouseenter='__expandMenu'  
                            e-mouseleave='__closeMenu'
                            e-click='__clickMenu:nopopo'>
                            <div class={{'ui-menu-node' ${activeClass}}}
                                ${style1}
                                e-click='__clickMenu:nopopo'>
                                <slot innerRender/>
                            </div>
                            <recur cond='children'
                                e-mouseenter='__expandMenu'
                                e-mouseleave='__closeMenu'
                                e-click='__clickMenu:nopopo'
                                class='ui-menu-subct-wrap'
                                x-animationbox
                                style={{__genPopStyle(children,__open,__x,__y)}}>
                                <div class='ui-menu-subct-pop'>
                                    <div style='${menuStyle}'>
                                        <for cond={{children}}  
                                            class='ui-menu-node-wrap'  
                                            e-mouseenter='__expandMenu'  
                                            e-mouseleave='__closeMenu'
                                            e-click='__clickMenu:nopopo' >
                                            <div class={{'ui-menu-node' ${activeClass}}} 
                                                ${style1}>
                                                <slot innerRender/>
                                                ${iconStr}
                                            </div>
                                            <recur ref />
                                        </for>
                                    </div>
                                </div>
                            </recur>
                        </for>
                    </div>
                </div>
            `;
        }
    }
    /**
     * 点击item事件
     * @param model - 当前dom对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e -event对象
     */
    __clickMenu(model, dom, evObj, e) {
        //处理显示和隐藏
        if (model.children && this.__vertical) {
            model['__open'] = !model['__open'];
        }
        //激活菜单（纵向菜单有效）
        this.__setActive(model);
        //click事件
        if (this.__onItemClick) {
            this.invokeOuterMethod(this.__onItemClick, model, dom, evObj, e);
        }
    }
    /**
     * 叶子结点激活
     * @param model - 对应model
     */
    __setActive(model) {
        if (!model.children) {
            if (this.__activeModel) {
                this.__activeModel['__active'] = false;
            }
            model['__active'] = true;
            this.__activeModel = model;
        }
    }
    /**
     * 计算展开菜单高度
     * @param open - 是否展开
     * @param children - 孩子节点
     * @returns     容器高度
     */
    __cacHeight(open, children) {
        let height = 0;
        const oneHeight = 40;
        if (!open || !children) {
            return height;
        }
        cac(children);
        return height;
        function cac(chd) {
            for (const c of chd) {
                height += oneHeight;
                if (c.__open && c.children) {
                    cac(c.children);
                }
            }
        }
    }
    /**
     * 生成popmenu style
     * @param model - 对应model
     * @returns     style串
     */
    __genPopStyle(children, open, x, y) {
        const height = open && children && children.length ? children.length * 40 : 0;
        return `width:${this.__width}px;height:${height || 0}px;left:${x || 0}px;top:${y || 0}px`;
    }
    /**
     * 展开关闭节点
     * @param model - 当前dom对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __expandMenu(model, dom, evObj, e) {
        if (model['__open'] || !model['children'] || model['children'].length === 0) {
            return;
        }
        model['__open'] = true;
        if (!this.__vertical) {
            this.__cacLoc(model, dom, evObj, e);
        }
    }
    /**
     * 关闭子菜单
     * @param model - 当前dom对应model
     */
    __closeMenu(model) {
        model['__open'] = false;
    }
    /**
     * 计算位置
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e - html event
     */
    __cacLoc(model, dom, evObj, e) {
        if (!model.children) {
            return;
        }
        //横向菜单 一级菜单的子菜单为下侧，否则为右侧
        const pos = UITool.cacPosition(e, dom.props['class'].includes('-first') ? 1 : 2, this.__width, model.children.length * 30 + 20);
        model.__x = pos[0];
        model.__y = pos[1];
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender() {
        const me = this;
        //设置level
        if (!this.__vertical || !this.model['data']) {
            return;
        }
        for (const d of this.model['data']) {
            if (!d.__level) {
                setLevel(d, 1);
            }
        }
        //设置active
        if (!this.__activeModel) {
            for (const d of this.model['data']) {
                if (setActive(d)) {
                    return;
                }
            }
        }
        /**
         * 设置level
         * @param data - 菜单结构数据
         * @param level - 层级
         * @returns
         */
        function setLevel(data, level) {
            if (data.__level) {
                return;
            }
            data.__level = level;
            if (data.children) {
                for (const d of data.children) {
                    setLevel(d, level + 1);
                }
            }
        }
        /**
         * 设置激活对象
         * @param data - 菜单结构数据
         * @returns
         */
        function setActive(data) {
            if (data.__active) {
                me.__setActive(data);
                return data;
            }
            if (data.children) {
                for (const d of data.children) {
                    setActive(d);
                }
            }
        }
    }
}
//注册模块
Nodom.registModule(UIMenu, 'ui-menu');
//# sourceMappingURL=menu.js.map