import { Module } from '/dist/nodomui.min.js'

export class cardex3 extends Module {
    template(props) {
        return `
                <div style="display: flex;justify-content: space-around;">
                    <ui-card width='400px' height='240px' body-style="height: 100px;width: 200px;justify-content:center;">
                        <div x-slot='header'>
                            Card Header
                        </div>
                        <img src="/examples/app/modules/card/avatar.png" style="height: 90%"/>
                    </ui-card>

                    <ui-card width='400px' height='240px' body-style="height: 200px;width: 400px;justify-content:center;">
                        <div x-slot='header'>
                            Card Header
                        </div>
                        <img src="/examples/app/modules/card/avatar.png" style="height: 90%"/>
                    </ui-card>
                </div>
            `;
    }

}