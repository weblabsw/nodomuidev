import { Module } from "nodom3";
/**
 * drawer插件
 * 参数说明
 * title:           标题
 * closable:        是否显示close按钮
 * modal:           控制是否显示遮罩层
 * showconfirm:     控制是否显示确认关闭消息框
 * size:            控制抽屉的大小
 * direction:       控制抽屉弹出的方向
 * onInner          点击抽屉时事件
 * onOpen:          drawer打开后事件方法名
 * onClose:         drawer关闭后事件方法名
 * onBeforeOpen:    drawer打开前事件名，如果对应方法返回true，则不打开
 * onBeforeClose:   drawer关闭前事件名，如果对应方法返回true，则不关闭
 */
/**
 * UIDrawer 抽屉
 * @beta
 */
export declare class UIDrawer extends Module {
    /**
     * 点击抽屉事件
     */
    private __onInner;
    /**
     * 打开时事件
     */
    private __onOpen;
    /**
     * 关闭时事件
     */
    private __onClose;
    /**
     * 打开前事件方法名
     */
    private __onBeforeOpen;
    /**
     * 关闭前事件方法名
     */
    private __onBeforeClose;
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        __show: boolean;
        __open: boolean;
    };
    /**
     * 点击drawer外部时，弹出消息框询问是否关闭
     */
    private __showConfirm;
    /**
     * 点击drawer时触发
     */
    __inner(): void;
    /**
     * 关闭drawer
     */
    __close(): void;
    /**
     * 打开drawer
     */
    __open(): void;
    /**
     * 检查关闭
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __checkClose(model: any, dom: any, evObj: any, e: any): void;
}
