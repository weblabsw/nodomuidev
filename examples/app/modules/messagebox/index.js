import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import{EXCONFIG} from './config.js'
import{messageboxex1} from './ex1.js'
import{messageboxex2} from './ex2.js'
import{messageboxex3} from './ex3.js'


export class MMessageBox extends Module{
    modules=[ExModule,AttrGrid,EventGrid,MethodGrid,messageboxex1,messageboxex2,messageboxex3];
    template(props){
        return `
            <div class='--component'>
                <h1>MessageBox 弹框</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='messagebox'/>
                </div>
        
                <h2>方法</h2>
                <h3>此组件通过nuiconfirm，nuialert，nuiprompt等方法定义</h3>
                <MethodGrid $rows={{messageboxMethods}} />
                
                <ui-messagebox />
            </div>
        `
    }
    data(){
        return EXCONFIG;
    }
}