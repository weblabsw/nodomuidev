import { BaseInput } from "./baseinput";
/**
 * 参数说明
 * $data:           rows和cols定义
 * $value:          对应的数据数组
 * display-field:    用于显示的字段，rows和cols保持已知
 * value-field:      值字段，rows和cols保持已知
 */
/**
 * UIRelationMap 关系图
 * @public
 */
export declare class UIRelationMap extends BaseInput {
    /**
     * 横轴纵轴值属性名，默认id
     */
    __valueField: string;
    /**
     * 横轴纵轴显示属性名，默认title
     */
    __displayField: string;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 初始化值
     */
    protected __initValue(): void;
    /**
     * 点击dom
     * @param model - dom对应model
     */
    private __clickItem;
    private __hover;
    private __leave;
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender(): void;
}
