import{Module} from '/dist/nodomui.min.js'
export class ratingex6 extends Module{
    template() {
        return `
            <div>
                <ui-rating field="score4" touchable>{{score4}}</ui-rating>
            </div>
        `
    }
    data() {
        return {
            score4: 2
        }
    }
}