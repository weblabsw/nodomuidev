
import{Module} from '/dist/nodomui.min.js'
export class gridex13 extends Module{
    template(){
        return `
            <ui-grid $data={{rows}} style='height:200px;' grid-line='rows' fix-head checkable>
                <ui-grid-col width='80' title='编号' fixed>{{id}}</ui-grid-col>
                <ui-grid-col width='200' title='框架'>{{name}}</ui-grid-col>
                <ui-grid-col width='150' title='发布时间'>{{pubTime}}</ui-grid-col>
                <ui-grid-col width='300' title='描述' >{{desc}}</ui-grid-col>
                <ui-grid-col width='150' title='操作' fixed='right'>
                    <ui-button theme='success' title='编辑' e-click='edit' />&nbsp;&nbsp;
                    <ui-button theme='error' title='删除' e-click='del' />
                </ui-grid-col>
            </ui-grid>
        `;
    }
    data(){
        return{
            rows:[
                {id:1,name:'nodom',pubTime:'2017年11月',desc:'Web技术实验室发布的web前端框架'},
                {id:2,name:'noomi',pubTime:'2019年10月',desc:'Web技术实验室发布的node.js服务端框架'},
                {id:3,name:'relaen',pubTime:'2020年10月',desc:'Web技术实验室发布的node.js ORM框架'},
                {id:4,name:'react',pubTime:'2013年5月',desc:'Facebook开源发布的web前端框架'},
                {id:5,name:'vue',pubTime:'2013年2月',desc:'国内优秀web前端框架'},
                {id:6,name:'angular',pubTime:'2010年2月',desc:'Goole发布的web前端框架'}
            ]
        }
    }

    edit(model){
        console.log('edit data is:',model);
    }

    del(model){
        console.log('delete data is:',model);
    }
}