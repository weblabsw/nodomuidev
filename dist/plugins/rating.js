import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
/**
 * 参数说明
 * size： 尺寸，normal,small,large
 * max： 最大评分值
 * readonly： 只读
 * allow-half：允许半选
 * touchable：支持滑动手势
 * color：自定义颜色
 */
/**
 * UIRating 评分
 * @beta
 */
export class UIRating extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.model['__starsArray'] = [];
        this.__size = props['size'] || 'normal';
        this.__max = props['max'] || 5;
        this.__color = props['color'];
        if (props.hasOwnProperty('readonly')) {
            this.__readonly = true;
        }
        if (props.hasOwnProperty('allow-half')) {
            this.__allowHalf = true;
        }
        if (props.hasOwnProperty('touchable')) {
            this.__touchable = true;
        }
        this.__pushStarsArr();
        //设置不渲染到attribute的属性
        this.setExcludeProps(['size', 'max', 'readonly', 'allow-half', 'touchable', 'color']);
        return `
          <div class='container'> 
              <div class='ui-rating'>
                  <span x-repeat={{__starsArray}} index="idx" ${this.__readonly ? '' : 'e-mousedown="__drag"'}  ${this.__touchable ? 'e-mousemove="__drag"' : ''}>
                    <input type='radio' class='{{this.__renderStars(idx)}}' style='{{this.__changeColor(idx)}}'/>
                  </span>
              </div>
              <span class='ui-rating-title'>
                  <slot/>
              </span>
          </div>
      `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __starsArray: [],
        };
    }
    /**
     * 生成评分星星数组
     */
    __pushStarsArr() {
        let star = 1;
        if (this.__allowHalf)
            star = 0.5;
        for (let i = star; i <= this.__max; i += star) {
            this.model['__starsArray'].push({ i });
        }
    }
    /**
     * 渲染激活的评分星星
     * @param idx - 当前节点索引
     * @returns  激活的评分星星class
     */
    __renderStars(idx) {
        return `ui-rating-star ${this.model['__starsArray'][idx].i <= this.model['__value'] ? 'ui-rating-star-filled' : ''}  ${'ui-rating-' + this.__size} ${this.__allowHalf ? 'ui-rating-star-half-filled' : ''}`;
    }
    /**
     * 自定义颜色
     * @param idx -   当前节点索引
     * @returns  自定义颜色style
     */
    __changeColor(idx) {
        return `${this.model['__starsArray'][idx].i <= this.model['__value'] && this.__color ? `color: ${this.__color}` : ''}`;
    }
    /**
     * 鼠标点击
     * @param model - 当前节点对应model
     */
    __drag(model) {
        this.model['__value'] = model.i;
    }
}
Nodom.registModule(UIRating, 'ui-rating');
//# sourceMappingURL=rating.js.map