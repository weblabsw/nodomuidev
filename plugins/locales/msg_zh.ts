/*
 * 消息js文件 中文文件
 */
export const NodomUIMessage_zh = {
    uploadingText:'上传中...',
    pleaseSelect:'请选择...',
    ok:'确定',
    cancel:'取消',
    //date
    sunday:'日',
    monday:'一',
    tuesday:'二',
    wednesday:'三',
    thursday:'四',
    friday:'五',
    saturday:'六',
    today:'今天',
    //pagination
    total:'共',
    totalUnit:'条',
    pagePre:'第',
    page:'页',
    pageSize:'条/页',
    noData:'无数据',
    //表单验证
    form:{
        type: "请输入有效的{0}",
        unknown: "输入错误",
        require: "不能为空",
        number:"请输入数字",
        min: "值最小为{0}",
        max: "值最大为{0}",
        between:"值必须在{0}-{1}之间",
        maxLength:"长度不能大于{0}",
        minLength:"长度不能小于{0}",
        betweenLength:'长度必须在{0}-{1}之间',
        email:'请输入有效的email',
        url:'请输入有效的url',
        mobile:'请输入有效的手机号',
        date:"请输入有效日期格式，如：2022-01-01",
        time:"请输入有效时间格式，如：12:30:30",
        datetime:"请输入有效日期时间格式，如：2022-01-01",
        idno:'请输入18位有效身份证号',
        range:'第二个值不允许小于第一个值'
    }
}
