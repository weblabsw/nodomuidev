import { Module, Nodom } from "nodom3";
/**
 * 标记插件
 * 配置参数
 * value:       标记内容
 * max:         展示max+字样
 * dot:         显示小红点
 * active:      右上角标记带跳动效果
 * type:        标记背景颜色
 * icon         自定义标记图标，值参考图教程
 * color        自定义标记背景颜色
 */
/**
 * UIBadge 标记
 * @beta
 */
export class UIBadge extends Module {
    /**
模板函数
     * @privateRemarks
     */
    template(props) {
        //基础样式
        const clsArr = ['ui-badge-sup'];
        this.__value = props['value'];
        //自定义颜色
        let selfColor = '';
        if (props.hasOwnProperty('dot')) {
            clsArr.push("dot");
        }
        if (props.hasOwnProperty("active")) {
            clsArr.push("active");
        }
        if (props['max']) {
            this.__value = `${props['max']}+`;
        }
        //如果传递过来的是图标，不设置背景
        if (props.hasOwnProperty("icon")) {
            this.__icon = true;
        }
        else {
            this.__icon = false;
            //如果传入了自定义颜色，则设置
            if (props['color']) {
                selfColor += `background-color:${props['color']};`;
            }
            else {
                //如果没有传color，看是否有type,没有就默认颜色
                clsArr.push(`ui-badge-sup-${props['type'] || 'error'}`);
            }
        }
        this.setExcludeProps(['value', 'type', 'dot', 'max', 'active', 'icon', 'color']);
        return `
            <div class="ui-badge">
                <slot/>
                <sup class='${clsArr.join(" ")}' style='${selfColor}' x-if={{!this.__icon}}>
                    ${this.__value || ''} 
                </sup>
                <sup class='${clsArr.join(" ")}' style='${selfColor}' x-else>
                   <b class="${props['icon']}">
                </sup>
            </div>
        `;
    }
}
Nodom.registModule(UIBadge, 'ui-badge');
//# sourceMappingURL=badge.js.map