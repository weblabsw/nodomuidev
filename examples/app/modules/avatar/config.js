export const EXCONFIG = {
    name: 'Avatar',
    lowerCaseName: 'avatar',
    desc: '使用ui-avatar组件进行设置。',
    popoverAttrs: [{
        name: 'size',
        desc: 'Avatar 大小',
        type: 'string',
        values: 'small | normal | big',
        defaults: 'normal'
    }, {
        name: 'icon',
        desc: '设置 Avatar 的图标类型',
        type: 'string',
        values: '参考 Icon 组件',
        defaults: 'user'
    }, {
        name: 'shape',
        desc: 'Avatar 形状',
        type: 'string',
        values: 'circle | square',
        defaults: 'circle'
    }, {
        name: 'src',
        desc: 'Avatar 图片的源地址',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'alt',
        desc: '图片 Avatar 的原生 alt 属性',
        type: 'string',
        values: '-',
        defaults: '-'
    }],
    examples: [{
        title: '基础用法',
        desc: '使用 shape 和 size 属性来设置 Avatar 的形状和大小',
        name: 'ex1'
    }, {
        title: '展示类型',
        desc: '支持使用图标、图片或者文字作为 Avatar。其中文字作为 Avatar 的展示类型时是默认插槽实现',
        name: 'ex2'
    },
    ]
}