import { Module, Nodom, Router } from "nodom3";
/**
 * bgcolor          默认t背景色
 * color            默认前景色
 * active-bgcolor    激活背景色
 * active-color      激活前景色
 * tabs             页签对象数组
 *  包括：
 *      title       页签标题
 *      active      是否激活
 *      closable    是否可关闭
 *      path        路由路径（需先定义路由）
 * onChange         tab切换时事件，this指向RouterTab插件
 *      参数1：      切换前tab，可能为undefined
 *      参数2:       切换后tab
 * onTabClose       tab关闭事件，this指向RouterTab插件
 *      参数1：      被关闭tab
 */
/**
 * UIRouterTab 路由标签页
 * @public
 */
export class UIRouterTab extends Module {
    constructor() {
        super(...arguments);
        /**
         * 拖动参数
         */
        this.__dragParam = { dx: 0, x: 0 };
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__onTabClose = props.ontabclose;
        this.__onTabChange = props.ontabchange;
        let tabStyle = '';
        let activeStyle = '';
        if (props['active-bgcolor']) {
            activeStyle += 'background-color:' + props['active-bgcolor'];
        }
        //激活前景色，如果没设置，但是设置了默认前景色，则使用默认前景色
        const ac = props['active-color'] || props.color;
        if (ac) {
            if (activeStyle !== '') {
                activeStyle += ';';
            }
            activeStyle += 'color:' + ac;
        }
        let defaultStyle = '';
        if (props['bg-color']) {
            defaultStyle += 'background-color:' + props['bg-color'];
        }
        if (props.color) {
            if (defaultStyle !== '') {
                defaultStyle += ';';
            }
            defaultStyle += 'color:' + props.color;
        }
        if (activeStyle) {
            tabStyle = `style={{active?'${activeStyle}':'${defaultStyle}'}}`;
        }
        else if (defaultStyle) {
            tabStyle = `style={{!active?'${props.defaultstyle}':''}}`;
        }
        const headBg = props.bgcolor ? "style='background-color:" + props.bgcolor + "'" : '';
        this.setExcludeProps(['active-bgcolor', 'active-color', 'bgcolor', 'color', 'onchange', 'ontabclose']);
        return `
            <div class='ui-tab'>
                <div class='ui-tab-headct' ${headBg}>
                    <div class='ui-tab-head' e-drag='__dragHead' e-mousedown='__dragStart' e-mousemove='__drag' e-mouseup='__dragEnd' e-mouseleave='__dragEnd'>
                        <for cond={{tabs}} class={{'ui-tab-item' + (active?' ui-tab-item-active':'')}}  x-route={{path}} active='active' ${tabStyle}>
                            {{title}}
                            <b class="ui-tab-close" x-show={{closable}} e-click='__closeTab:nopopo'/>
                        </for>
                    </div>
                </div>
                <div class='ui-tab-body' x-router>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            tabs: []
        };
    }
    /**
     * 头部拖动开始
     * @param model - 对应model
     * @param dom - Virtual dom
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragStart(model, dom, evObj, e) {
        const el = e.currentTarget;
        const pel = el.parentElement;
        //设置宽度
        let w = 0;
        for (const d of el.children) {
            w += d.offsetWidth;
        }
        el.style.width = (w + 1) + 'px';
        //不比父宽，不处理
        if (el.offsetWidth < pel.offsetWidth) {
            return;
        }
        this.__dragParam.x = e.pageX;
    }
    /**
     * 拖动
     * @param model - 对应model
     * @param dom - Virtual dom
     * @param evObj - NEvent对象
     * @param e - event对象
     * @returns
     */
    __drag(model, dom, evObj, e) {
        if (!this.__dragParam.x) {
            return;
        }
        this.__move(e);
    }
    /**
     * 拖动停止
     * @param model - 对应model
     * @param dom - Virtual dom
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragEnd(model, dom, evObj, e) {
        this.__move(e);
        delete this.__dragParam.x;
    }
    /**
     * 移动
     * @param e - event对象
     * @returns
     */
    __move(e) {
        if (!this.__dragParam.x) {
            return;
        }
        const dx = e.pageX - this.__dragParam.x;
        if (Math.abs(dx) < 2) {
            return;
        }
        this.__dragParam.dx += dx;
        if (this.__dragParam.dx > 0) {
            this.__dragParam.dx = 0;
        }
        else {
            const el = e.currentTarget;
            const pel = el.parentElement;
            if (el.offsetWidth + this.__dragParam.dx < pel.offsetWidth) {
                this.__dragParam.dx = pel.offsetWidth - el.offsetWidth;
            }
        }
        this.__dragParam.x = e.pageX;
        e.currentTarget.style.transform = 'translateX(' + this.__dragParam.dx + 'px)';
    }
    /**
     * 清除多余active
     */
    __cleanActive() {
        const tabs = this.model['tabs'];
        let atab;
        if (tabs && tabs.length > 0) {
            for (let i = tabs.length - 1; i >= 0; i--) {
                const tab = tabs[i];
                // 最后一个active tab优先
                if (tab.active) {
                    if (!atab) { // 
                        atab = tab;
                    }
                    else { //找到activetab后，另外的active置false
                        tab.active = false;
                    }
                }
            }
            //如果没有设置active tab，默认第一个
            this.__changeTo(atab || tabs[0]);
        }
    }
    /**
     * 切换到目标tab
     * @param tab - 目标tab
     * @returns
     */
    __changeTo(tab) {
        if (!tab || tab === this.__currentTab) {
            return;
        }
        if (this.__currentTab) {
            this.__currentTab.active = false;
        }
        tab.active = true;
        if (this.__onTabChange) {
            this.invokeOuterMethod(this.__onTabChange, this.__currentTab, tab);
        }
        this.__currentTab = tab;
        if (!Nodom['$Router']) {
            Nodom.use(Router);
        }
        Nodom['$Router'].go(tab.path);
    }
    /**
     * 手动active
     * @param data - model或title或索引号
     */
    __activeTab(data) {
        let tab;
        switch (typeof data) {
            case 'number':
                tab = this.model['tabs'][data];
                break;
            case 'string':
                tab = this.model['tabs'].find((item) => item.title === data);
                break;
            default:
                tab = this.model['tabs'].find((item) => item === data);
        }
        this.__changeTo(tab);
    }
    /**
     * 关闭页签
     * @param model - TabItem
     * @returns
     */
    __closeTab(model) {
        //最后一个不删除
        if (this.model['tabs'].length === 1) {
            return;
        }
        const index = this.model['tabs'].findIndex(item => item === model);
        if (index >= 0 || index < this.model['tabs'].length) {
            model = this.model['tabs'][index];
            //执行tabclose事件
            if (this.__onTabClose) {
                this.invokeOuterMethod(this.__onTabClose, this.model['tabs'][index]);
            }
            //移除
            this.model['tabs'].splice(index, 1);
            //被删除为当前tab，需要切换当前tab
            if (model === this.__currentTab) {
                if (index === this.model['tabs'].length) { //最后一个
                    this.__changeTo(this.model['tabs'][index - 1]);
                }
                else { //取后面一个
                    this.__changeTo(this.model['tabs'][index]);
                }
            }
        }
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender() {
        this.__cleanActive();
    }
}
//注册模块
Nodom.registModule(UIRouterTab, 'ui-routertab');
//# sourceMappingURL=routertab.js.map