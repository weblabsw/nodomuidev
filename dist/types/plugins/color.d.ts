/**
 * Color配置项
 */
interface ColorOptions {
    /**
     * 是否启用透明度
     */
    enableAlpha: boolean;
    /**
     * 颜色格式
     */
    format: string;
    /**
     * 默认值
     */
    value: string;
}
/**
 * color工具类，作为转换颜色格式
 * @alpha
 */
export default class Color {
    /**
     * 色相
     */
    private _hue;
    /**
     * 饱和度
     */
    private _saturation;
    /**
     * 亮度
     */
    private _value;
    /**
     * 透明度
     */
    private _alpha;
    enableAlpha: boolean;
    format: string;
    value: string;
    constructor(options: ColorOptions);
    set<T>(prop: string | Record<string, T>, value?: T): void;
    get(propName: string): any;
    toRgb(): {
        r: number;
        g: number;
        b: number;
    };
    fromString(value: string): void;
    doOnChange(): void;
    compare(color: this): boolean;
}
export {};
