import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import{EXCONFIG} from './config.js'
import{tabex1} from './ex1.js'
import{tabex2} from './ex2.js'
import{tabex3} from './ex3.js'
import{tabex4} from './ex4.js'
import{tabex5} from './ex5.js'
import{tabex6} from './ex6.js'
import{tabex7} from './ex7.js'
import{tabex8} from './ex8.js'
import{tabex9} from './ex9.js'
import{tabex10} from './ex10.js'


export class MTab extends Module{
    modules=[ExModule,AttrGrid,EventGrid,MethodGrid,tabex1,tabex2,tabex3,tabex4,tabex5,tabex6,tabex7,tabex8,tabex9,tabex10];
    template(props){
        return `
            <div class='--component'>
                <h1>Tab 标签页</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='tab'/>
                </div>
        
                <h2>属性</h2>
                <h3>UITab/ui-tab</h3>
                <AttrGrid $rows={{tabAttrs}} />

                <h3>UItabItem/ui-tab-item</h3>
                <AttrGrid $rows={{tabItemAttrs}} />

                <h2>事件</h2>
                <h3>UITab/ui-tab</h3>
                <EventGrid $rows={{tabEvents}} />

                <h2>方法</h2>
                <h3>UITab/ui-tab</h3>
                <MethodGrid $rows={{tabMethods}} />
            </div>
        `;
    }
    data(){
        return EXCONFIG;
    }
}