import { Module } from '/dist/nodomui.min.js'

export class popoverex2 extends Module {
    template(props) {
        return `
                <div>
                    <ui-popover>
                        <div x-slot='trigger'>
                            <ui-avatar icon="user"/>
                        </div>
                        <div class="popover">
                            弹出内容
                            <ui-button title="test1" theme="active" icon="edit" />
                        </div>
                    </ui-popover>

                    <ui-popover placement="top">
                        <div x-slot='trigger'>
                            <ui-avatar icon="user"/>
                        </div>
                        <div class="popover">
                            弹出内容
                            <ui-button title="test1" theme="active" icon="edit" />
                        </div>
                    </ui-popover>

                    <ui-popover placement="left">
                        <div x-slot='trigger'>
                            <ui-avatar icon="user"/>
                        </div>
                        <div class="popover">
                            弹出内容
                            <ui-button title="test1" theme="active" icon="edit" />
                        </div>
                    </ui-popover>

                    <ui-popover placement="right">
                        <div x-slot='trigger'>
                            <ui-avatar icon="user"/>
                        </div>
                        <div class="popover">
                            弹出内容
                            <ui-button title="test1" theme="active" icon="edit" />
                        </div>
                    </ui-popover>
                    
                </div>

                <style>
                    .popover{
                        display:flex;
                        flex-direction:column;
                        align-items:center;
                        justify-content:space-around;
                        height:100px;
                    }

                    .ui-popover{
                        margin-left:30px;
                    }
                </style>
            `;
    }
}