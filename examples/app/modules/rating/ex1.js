import{Module} from '/dist/nodomui.min.js'
export class ratingex1 extends Module{
    template() {
        return `
            <div>
                <ui-rating field="score">{{score}}</ui-rating>
            </div>
        `
    }
    data() {
        return {
            score: 3,
        }
    }
}