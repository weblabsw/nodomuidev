import{Module,nuiloading} from '/dist/nodomui.min.js'
export class loadingex7 extends Module{
    template(){
        return `
            <div>
                <ui-button e-click='show' title='打开' />
                <ui-loading color='red' radius={{rad}}/>
            </div>
        `
    }
    data(){
        return{
            rad : 100
        }
    }
    show(){
        nuiloading().__show();
        setTimeout(()=>{
            nuiloading().__close(true);
        },4000)
    }
}