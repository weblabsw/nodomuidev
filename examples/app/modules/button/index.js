import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'

import{EXCONFIG} from './config.js'
import{buttonex1} from './ex1.js'
import{buttonex2} from './ex2.js'
import{buttonex3} from './ex3.js'
import{buttonex4} from './ex4.js'
import{buttonex5} from './ex5.js'
import{buttonex6} from './ex6.js'


export class MButton extends Module{
    modules=[ExModule,AttrGrid,buttonex1,buttonex2,buttonex3,buttonex4,buttonex5,buttonex6];
    template(props){
        return `
            <div class='--component'>
                <h1>Button 按钮</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='button'/>
                </div>
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{buttonAttrs}} />
            </div>
        `
    }
    data(){
        return EXCONFIG;
    }
}