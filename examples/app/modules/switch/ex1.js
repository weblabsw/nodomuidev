import { Module } from '/dist/nodomui.min.js'
export class switchex1 extends Module {
    template(props) {
        return `
            <div>开关值：{{status}}</div>
            <ui-switch field="status" />
        `;
    }
    data() {
        return {
            status: true
        }
    }
}