import{Module} from '/dist/nodomui.min.js'
export class dateex2 extends Module{
    template(props){
        return `
            <div>
                <div>日期：{{date}}</div>
                <ui-date field="date" use-timestamp format='yy/MM/dd'/>
            </div>
        `;
    }
    data(){
        return {
            date:Date.now()
        }
    }
}