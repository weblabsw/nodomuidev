
import { Module } from '/dist/nodomui.min.js'
export class sliderex1 extends Module {
    template(props) {
        return `
            <div>
                <span>拖动进度：{{outValue}}</span>
                <ui-slider  field='outValue'  style="width:200px;left:100px" />
            </div>
        `;
    }
    data() {
        return {
            outValue: 10,
        }
    }
}