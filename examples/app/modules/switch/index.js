import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { switchex1 } from './ex1.js'
import { switchex2 } from './ex2.js'
import { switchex3 } from './ex3.js'
export class MSwitch extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, switchex1, switchex2,switchex3];
    template(props) {
        return `
            <div class='--component'>
                <h1>Switch 开关</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='switch'/>
                </div>
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{gridAttrs}} />
            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}