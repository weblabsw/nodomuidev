import { BaseInput } from "./baseinput";
/**
 * 参数说明
 * size： 尺寸，normal,small,large
 * max： 最大评分值
 * readonly： 只读
 * allow-half：允许半选
 * touchable：支持滑动手势
 * color：自定义颜色
 */
/**
 * UIRating 评分
 * @beta
 */
export declare class UIRating extends BaseInput {
    /**
     * 尺寸
     */
    private __size;
    /**
     * 最大评分值
     */
    private __max;
    /**
     * 只读
     */
    private __readonly;
    /**
     * 允许半选
     */
    private __allowHalf;
    /**
     * 支持滑动手势
     */
    private __touchable;
    /**
     * 自定义颜色
     */
    private __color;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: object): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        __starsArray: any[];
    };
    /**
     * 生成评分星星数组
     */
    private __pushStarsArr;
    /**
     * 渲染激活的评分星星
     * @param idx - 当前节点索引
     * @returns  激活的评分星星class
     */
    private __renderStars;
    /**
     * 自定义颜色
     * @param idx -   当前节点索引
     * @returns  自定义颜色style
     */
    private __changeColor;
    /**
     * 鼠标点击
     * @param model - 当前节点对应model
     */
    private __drag;
}
