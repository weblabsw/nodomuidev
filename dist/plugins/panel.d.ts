import { Module } from 'nodom3';
import { UIButton } from './button';
/**
 * 参数说明
 * title:   标题
 * buttons: 按钮，以','分割，按钮事件以'|'分割,如：minus|clickMinus,close|clickClose
 */
/**
 * UIPanel 面板
 * @public
 */
export declare class UIPanel extends Module {
    /**
     * @example
     * 事件对象
     * key:按钮名
     * value:事件名
     */
    private eventMap;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 点击按钮
     * @param model - 对应model
     * @param dom - Virtual dom
     */
    clickButton(model: any, dom: any): void;
}
/**
 * UIPanelBody 面板主体
 * @public
 */
export declare class UIPanelBody extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): string;
}
/**
 * UIToolbar 面板工具栏
 * @public
 */
export declare class UIToolbar extends Module {
    /**
     * 子按钮
     */
    modules: (typeof UIButton)[];
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): string;
}
/**
 * UIButtonGroup 面板按钮集
 * @public
 */
export declare class UIButtonGroup extends Module {
    /**
     * 子按钮
     */
    modules: (typeof UIButton)[];
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): string;
}
