import { Module } from "nodom3";
export declare class UICard extends Module {
    /**
     * 高度
     */
    private height;
    /**
     * 宽度
     */
    private width;
    /**
     * 阴影
     */
    private shadow;
    template(props?: object): string;
    judgeShadow(shadow: string): string;
}
