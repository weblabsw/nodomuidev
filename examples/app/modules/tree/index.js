import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { treeex1 } from './ex1.js'
import { treeex2 } from './ex2.js'
import { treeex3 } from './ex3.js'
import { treeex4 } from "./ex4.js";
import { treeex5 } from "./ex5.js";
import { treeex6 } from "./ex6.js";
export class MTree extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, treeex1, treeex2, treeex3, treeex4, treeex5, treeex6];
    template(props) {
        return `
            <div class='--component'>
                <h1>Tree 树形插件</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='tree'/>
                </div>
                
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{treeAttrs}} />
                
                <h2>事件</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <EventGrid $rows={{treeEvents}} />
            </div>
        `;
    }
    data() {
        return EXCONFIG;
    }
}