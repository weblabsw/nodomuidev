import { Module, nuitip } from '/dist/nodomui.min.js'
export class tipsex6 extends Module {
    template(props) {
        return `
            <div>
                <ui-button e-click='showtip6' title="排他以及随机宽度显示"></ui-button>
                <ui-tip />
            </div>
        `;
    }
    data() {
        return {
            index: 1
        }
    }

    showtip6(model) {
        nuitip({
            text: 'random width' + model.index++,
            position: 'bottom',
            closable: true,
            icon: 'insurance',
            width: 250 + Math.random() * 50 | 0,
            exclusive: true,
        });

    }
}