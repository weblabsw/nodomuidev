import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";

/**
 * 参数说明
 * $data:           rows和cols定义
 * $value:          对应的数据数组
 * display-field:    用于显示的字段，rows和cols保持已知
 * value-field:      值字段，rows和cols保持已知
 */


/**
 * UIRelationMap 关系图
 * @public
 */
export class UIRelationMap extends BaseInput {
    /**
     * 横轴纵轴值属性名，默认id
     */
    __valueField: string;

    /**
     * 横轴纵轴显示属性名，默认title
     */
    __displayField: string;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string {
        super.template(props);
        this.__valueField = props['value-field'];
        this.__displayField = props['display-field'];
        this.setExcludeProps(['display-field', 'value-field','left-title','top-title']);
        return `
            <table class='ui-relationmap' x-model='arr' >
                <tr class="ui-relationmap-head">
                    <td>
                        <span>${props['left-title']}</span>
                        \\
                        <span>${props['top-title']}</span>
                    </td> 
                    <td x-repeat={{cols}}>{{${this.__displayField}}}</td>
                </tr>
                <tr class='ui-relationmap-row' x-repeat={{rows}}>
                    <td>{{${this.__displayField}}}</td>
                    <td x-repeat={{__valueArray}} e-click='__clickItem'>
                        <b class={{__checked?'ui-icon-select':''}}/>
                    </td>
                </tr>
            </table>
        `;
    }

    /**
     * 初始化值
     */
    protected __initValue() {
        const model = this.model;
        if (!model['data']) {
            return;
        }
        if (!model['__value']) {
            model['__value'] = [];
        }
        const rowsArray = [];
        for (let row of model['data'].rows) {
            // 不能新建引用类型数据，否则死循环，原因未知
            // row['__valueArray'] = [];
            const valueArray = [];
            for (const col of model['data'].cols) {
                let find = false;
                for (const v of model['__value']) {
                    if (v && row[this['__valueField']] === v[0] && col[this['__valueField']] === v[1]) {
                        find = true;
                        break;
                    }
                }    
                valueArray.push({ __checked: find, row: row[this['__valueField']], col: col[this['__valueField']] });
                row = { id: row.id, title: row.title, __valueArray: valueArray };
            }
            rowsArray.push(row)
        }
        model['arr'] = {cols:model['data'].cols,rows:rowsArray};

    }

    /**
     * 点击dom
     * @param model - dom对应model
     */
    private __clickItem(model) {
        const index = this.model['__value'].findIndex(item => item[0] === model.row && item[1] === model.col);
        if (model.__checked) {
            if (index !== -1) {
                this.model['__value'].splice(index, 1);
            }
        } else if (index === -1) {
            this.model['__value'].push([model.row, model.col]);
        }
    }
    
    private __hover(item){

    }

    private __leave(){

    }
    
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender() {
        this.__initValue();
    }


}
//注册模块
Nodom.registModule(UIRelationMap, 'ui-relationmap');