import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { EXCONFIG } from './config.js'
import { ex1 } from './ex1.js'
export class MIcon extends Module {
    modules = [ExModule, ex1];
    template(props) {
        return `
            <div class='--component'>
                <h1>Icon 图标</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>内置图标</h2>
                <ex1/>
                <h2>自定义图标</h2>
                <div class='--component-desc'>
                    如果需要新增图标，可以从<a href='http://www.iconfont.cn' target='__blank'>iconfont官网</a>下载，
                    把下载的iconfont.ttf文件放置在path指定的路径，编写css文件并引入，css示例如下：
                    <pre>
@font-face {
    font-family: "myiconfont";
    src:url('path/iconfont.ttf');
}
/*定义一个名称为myicon的图标*/
.ui-icon-myicon {
    font-family: myiconfont;
}
.ui-icon-myicon:before {
    content: "\\e6a6";
}
                    </pre>
                </div>
            </div>
        `;
    }
    data() {
        return EXCONFIG;
    }
}