
import {Nodom} from "nodom3";
import { BaseInput } from "./baseinput";
import { UITool } from "./uibase";

/**
 * checkbox插件
 * 配置说明
 * field:       绑定父模块的字段
 * yes-value:   选中的值（可选），默认true
 * no-value:    不选中的值（可选），默认false
 * onChanged:    change事件方法
 */

/**
 * UICheckbox 复选框
 * @public
 */
export class UICheckbox extends BaseInput{
    /**
     * 选中时value，默认true
     */
    private __yesValue:string;
    /**
     * 未选中时value，默认false
     */
    private __noValue:string;
    /**
     * 选中时事件
     */
    private __onChanged:string;
    /**
     * 模板函数
     * @privateRemarks
     */
    public template(props){
        super.template(props);
        this.__yesValue = props['yes-value'] || true;
        this.__noValue = props['no-value'] || false;
        this.setExcludeProps(['field','yes-value','no-value']);
        this.__onChanged = props['onchanged'];
        this.setExcludeProps(['onchanged'])
        return `
            <span class={{'ui-checkbox' + (__value==this.__yesValue?' ui-checkbox-checked':'')}}>
                <span class='ui-checkbox-box' e-click='__clickCheck'>
                    <span class='ui-checkbox-inner' />
                </span>
                <span class={{'ui-checkbox-title'}}>
                    <slot/>
                </span>
            </span>
        `;
    }
    /**
     * 点击事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    public __clickCheck(model,dom,evObj,e){
        model['__value'] = model['__value'] == this.__yesValue?this.__noValue:this.__yesValue;
        if(this.__onChanged){
            UITool.invokePropMethod(this,this.__onChanged,model,dom,evObj,e);
        }
    }
}
Nodom.registModule(UICheckbox,'ui-checkbox');