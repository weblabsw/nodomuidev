import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
import { UITool } from "./uibase";

/**
 * 数据项
 * $data            树结构数据
 * 参数说明
 * field            如果树作为输入模式，需要设置对应字段，同时会显示复选框
 * value-field      数据项中用于取值的属性名，field存在时，不为空
 * icons            树节点图标，依次为为非叶子节点关闭状态，打开状态，叶子节点，如果只有两个，则表示非叶子节点和叶子节点，如果1个，则表示非叶子节点
 * onItemClick      节点点击事件
 */


/**
 * 树形控件
 * @public
 */
export class UITree extends BaseInput {
    /**
     * 值字段名
     */
    private __valueField: string;

    /**
     * 节点图标
     */
    private __icons: string[];

    /**
     * 节点点击事件名
     */
    private __onItemClick: string;

    /**
     * 节点展开事件
     */
    private __onExpand: string;

    /**
     * 节点闭合事件
     */
    private __onCollapse: string;

    /**
     * 模版函数
     * @privateRemarks
     */
    template(props?: object): string {
        super.template(props);
        this.__valueField = props['value-field'];
        this.__onItemClick = props['onitemclick'];
        this.__onExpand = props['onexpand'];
        this.__onCollapse = props['oncollapse'];
        this.__icons = props['icons'] ? props['icons'].split(',').map(item => item.trim()) : undefined;
        const checkStr = props['field'] ? `
            <span class={{__genCheckCls(__state)}}  e-click='__checkItem'>
                <span class='ui-checkbox-box' >
                    <span class='ui-checkbox-inner' />
                </span>
            </span>
        `: '';
        //设置不渲染到attribute的属性
        this.setExcludeProps(['icons', 'onitemclick', 'value-field', 'display-field', 'onexpand', 'oncollapse']);
        return `
            <div class='ui-tree' x-model='data'>
                <for cond={{children}} class='ui-tree-node-wrap'>
				    <div class='ui-tree-node'>
                        <b class={{__genArrowCls($model)}} e-click='__expandClose'></b>
                        ${props['icons'] ? "<b class={{__genFolderCls($model)}}></b>" : ""}
                        ${checkStr}
                        <span e-click='__clickItem'>
                            <slot innerRender/>
                        </span>
                    </div>
                    <recur cond='children' class='ui-expand-vertical' style={{'height:' + __cacHeight($model) + 'px'}}>
                        <div>
                            <div class='ui-tree-sub-wrap'>
                                <for cond={{children}} class='ui-tree-node-wrap'>
                                    <div class='ui-tree-node'>
                                        <b class={{__genArrowCls($model)}} e-click='__expandClose'></b>
                                        ${props['icons'] ? "<b class={{__genFolderCls($model)}}></b>" : ""}
                                        ${checkStr}
                                        <span e-click='__clickItem'>
                                            <slot innerRender/>
                                        </span>
                                    </div>
                                    <recur ref />
                                </for>
                            </div>
                        </div>
                    </recur>
                </for>
            </div>
        `;
    }

    /**
     * 创建选择框class
     * @param checked - 选中标识 true:选中  false:未选中
     * @returns         选择框class
     */
    private __genCheckCls(checked) {
        const arr = ['ui-checkbox'];
        if (!checked) {
            arr.push('ui-checkbox-uncheck');
        } else if (checked === 1) {
            arr.push('ui-checkbox-checked');
        } else {
            arr.push('ui-checkbox-partchecked');
        }
        return arr.join(' ');
    }

    /**
     * 创建树左侧箭头class
     * @param model - 节点对应model
     * @returns         箭头(展开收拢)图标class
     */
    private __genArrowCls(model) {
        const arr = [];
        if (model.children && model.children.length > 0) {
            arr.push('ui-expand-icon');
        }
        if (model.__open) {
            arr.push('ui-expand-icon-open');
        }
        return arr.join(' ');
    }

    /**
     * 显示文件夹图标
     * @param model - 节点对应model
     * @returns         文件夹图标class
     */
    private __genFolderCls(model) {
        if (!this.__icons || this.__icons.length === 0) {
            return;
        }
        const isLeaf = !model.children || model.children.length === 0;
        const isOpen = model.__open;
        const arr = this.__icons;
        const arr1 = ['ui-tree-icon'];
        if (arr.length === 1) {
            arr1.push(isLeaf ? '' : 'ui-icon-' + arr[0]);
        } else if (arr.length === 2) {
            arr1.push('ui-icon-' + (isLeaf ? arr[1] : arr[0]));
        } else if (arr.length === 3) {
            if (isOpen) {
                arr1.push('ui-icon-' + (isLeaf ? arr[2] : arr[1]));
            } else {
                arr1.push('ui-icon-' + (isLeaf ? arr[2] : arr[0]));
            }
        }
        return arr1.join(' ');
    }

    /**
     * 计算展开菜单高度
     * @param model - 节点对应model 
     * @returns     容器高度
     */
    private __cacHeight(model) {
        let height = 0;
        const oneHeight = 26;
        if (!model.__open || !model.children) {
            return height;
        }
        cac(model.children);
        return height;
        /**
         * 计算子节点容器高度
         * @param chd - 子节点Array
         */
        function cac(chd) {
            for (const c of chd) {
                height += oneHeight;
                if (c.__open && c.children) {
                    cac(c.children);
                }
            }
        }
    }

    /**
     * 点击item事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __clickItem(model, dom, evObj, e) {
        if (this.__onItemClick) {
            UITool.invokePropMethod(this,this.__onItemClick, model, dom, evObj, e);
        }
    }

    /**
     * 展开关闭节点
     * @param model - 当前节点对应model 
     */
    private __expandClose(model) {
        model['__open'] = !model['__open'];
        if (this.__onExpand && model['__open']) {
            UITool.invokePropMethod(this,this.__onExpand, model);
        } else if (this.__onCollapse && !model['__open']) {
            UITool.invokePropMethod(this,this.__onCollapse, model);
        }
    }

    /**
     * checkbox 点击
     * @param model -     当前节点对应model 
     */
    private __checkItem(model) {
        let state = model.__state;
        state = state === 0 || state === 2 ? 1 : 0;
        this.__changeState(model, state);
        this.__updateState();
    }

    /**
     * 首次渲染事件
     */
    protected __initValue() {
        if (!this.__field || !this.__valueField) {
            return;
        }
        super.__initValue();
        this.__finishState();
        this.__updateState();
    }

    /**
     * 补全整棵树选中值(state=1)
     */
    private __finishState() {
        const value = this.model['__value'];
        const valueField = this.__valueField;
        handleOne(this.model['data']);
        /**
         * 处理单个节点
         * @param m - model
         */
        function handleOne(m) {
            if (value.includes(m[valueField])) {
                if (m.children) {
                    for (const m1 of m.children) {
                        const v = m1[valueField];
                        if (!value.includes(v)) {
                            value.push(v);
                        }
                    }
                }
            }
            if (m.children) {
                for (const m1 of m.children) {
                    handleOne(m1);
                }
            }
        }
    }

    /**
     * 修改模型状态
     * @param model - 模型
     * @param state - 状态 0/1
     */
    private __changeState(model, state) {
        const value = this.model['__value'];
        const valueField = this.__valueField;

        updValue(model);
        handleSub(model);
        handleParent(model);

        /**
         * 处理子节点
         * @param m - model
         */
        function handleSub(m) {
            if (m.children) {
                for (const m1 of m.children) {
                    updValue(m1);
                    handleSub(m1);
                }
            }
        }

        /**
         * 处理祖先
         * @param m - model
         * @returns 
         */
        function handleParent(m) {
            if (!m.__parent || !m.__parent.__parent) {
                return;
            }
            m = m.__parent.__parent;
            if (state === 0) {  //删除value中的父
                updValue(m);
            } else {  //判断并确定是否添加父
                const all1 = m.children.find(item => !value.includes(item[valueField]));
                //子全为1，则添加值
                if (!all1) {
                    updValue(m);
                }
            }
            handleParent(m);
        }

        /**
         * 更新插件value
         * @param m - model
         */
        function updValue(m) {
            const v = m[valueField];
            const index = value.indexOf(v);
            if (state === 0) {
                if (index !== -1) {
                    value.splice(index, 1);
                }
            } else if (index === -1) {
                value.push(v);
            }
        }
    }

    /**
     * 更新整棵树
     */
    private __updateState() {
        const value = this.model['__value'];
        const valueField = this.__valueField;
        updOne(this.model['data']);

        function updOne(model) {
            if (value.includes(model[valueField])) {
                model.__state = 1;
                //逆向处理父为state=2
                for (let m = model; m.__parent && m.__parent.__parent;) {
                    const p = m.__parent.__parent;
                    if (p.__state === 1) {
                        break;
                    } else {
                        p.__state = 2;
                    }
                    m = p;
                }
            } else {
                model.__state = 0;
            }

            if (model.children) {
                for (const d of model.children) {
                    updOne(d);
                }
            }
        }
    }
}
//注册模块
Nodom.registModule(UITree, 'ui-tree');