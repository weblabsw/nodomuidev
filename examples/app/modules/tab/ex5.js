import{Module} from '/dist/nodomui.min.js'
export class tabex5 extends Module{
    template(props){
        return `
			<div>
                <ui-button title='添加tab' theme='active' e-click='add'/> &nbsp;
                <br/>
                <ui-tab onTabClose='closeTab'>
                    <ui-tab-item x-repeat={{tabs}} title={{title}} active={{active}} closable={{closable}}>
                        <p>{{content}}</p>
                    </ui-tab-item>
                </ui-tab>
            </div>
		    `
    }

    data(){
        return {
            index:5,
            tabs:[
                {title:'tab1',content:'这是tab1'},
                {title:'tab2',content:'这是tab2',closable:true},
                {title:'tab3',content:'这是tab3',closable:true,active:true},
                {title:'tab4',content:'这是tab4',closable:true},
                {title:'tab6',content:'这是tab6',closable:true},
                {title:'tab7',content:'这是tab7',closable:true},
                {title:'tab8',content:'这是tab8',closable:true},
                {title:'tab9',content:'这是tab9',closable:true},
            ]
        }
    }
    add(){
        this.model.tabs.push({title:'新增tab' + this.model.index,content:'新增tab内容' + this.model.index++,active:true,closable:true});
    }

}