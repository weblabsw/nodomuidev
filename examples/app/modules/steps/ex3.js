import { Module } from '/dist/nodomui.min.js'
export class stepsex3 extends Module {
    template(props) {
        return `
            <div >
                <ui-steps 
                    current={{current}} 
                    $data={{steps}}
                    finish-status="active"
                    process-status="error"
                    success-select
                    >
                </ui-steps>    
                <ui-button e-click='next' title="下一步"></ui-button>
            </div>
        `;
    }
    data() {
        return {
            current: 1,
            steps: [
                {
                    title: "Step 1",
                }, {
                    title: "Step 2",
                }, {
                    title: "Step 3"
                }
            ],
        }
    }
    next() {
        this.model['current'] += 1;
    }
}