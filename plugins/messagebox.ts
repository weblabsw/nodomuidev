import { EventMethod, Module, ModuleFactory,Nodom, UnknownMethod, VirtualDom,Directive} from "nodom3";
import { NodomUI } from "./nodomui";

/**
 * messagebox按钮类型
 * @public
 */
type UIMessageButton = {
    callback:EventMethod|string;
    text:string;
    methodName?:string;
    theme?:string;

}
/**
 * UIMessageBox 消息框
 * @public
 */
export class UIMessageBox extends Module{
    /**
     * 应用组件的按钮
     */
    public __buttons:UIMessageButton[];
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): string {
        let btnStr = '';
        let index=0;
        if(this.__buttons){
            for(const b of this.__buttons){
                let methodName = b.callback;
                if(typeof b.callback === 'function'){
                    //设定方法名和回调函数
                    methodName = '__genmethod__' + index++;
                    this[methodName] = (model,dom,ev,e)=>{
                        (<UnknownMethod>b.callback).call(this,this.model,dom,ev,e);
                        this.__close();
                    }
                }
                btnStr += `<ui-button title='${b.text}' theme='${b.theme||"default"}' e-click='${methodName}'></ui-button>`;
            }
        }
        return `
            <div class={{'ui-dialog ui-messagebox' + (__show?'':' ui-dialog-hide')}}>
                <div class={{'ui-mask' + (__open?'':' ui-mask-hide')}} />
                <div x-animationbox class='ui-messagebox-wrap' e-transitionend='__checkClose'>
                    <div>
                        <div class='ui-messagebox-body'>
                            <div class='ui-messagebox-content'>
                                <div x-if={{type==='prompt'}}>
                                    <div class='ui-messagebox-text'>{{text}}</div>
                                    <ui-input field='answer'/>
                                </div>
                                <div x-else>
                                    <div class='ui-messagebox-text'>{{text}}</div>
                                </div>
                            </div>
                            <div class='ui-messagebox-btn-wrap'>
                                ${btnStr}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __show:false,
            __open:false,
            answer:''
        }
    }

    /**
     * 关闭窗口
     */
    __close(){
        this.model['__open'] = false;
    }

    /**
     * 检查关闭状态（transition结束后）
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __checkClose(model,dom,evObj,e){
        if(e.target !== this.domManager.renderedTree.children[1].node){
            return;
        }
        if(!this.model['__open']){
            this.model['__show'] = false;
        }
    }
}

/**
 * UIMessageBoxManager 消息框管理器
 * @public
 */
export class UIMessageBoxManager{
    /**
     * confirm 对话框
     * @param cfg - confirm 配置
     * @returns 
     */
    public static confirm(cfg){
        const mdl:UIMessageBox = <UIMessageBox>ModuleFactory.getMain().getModule('UIMessageBox');
        if(!mdl){
            return;
        }
        //修改状态，避免不被渲染
        mdl.model['text'] = cfg.text;
        mdl.model['type'] = 'confirm';
        mdl.model['__open'] = true;
        mdl.model['__show'] = true;
        mdl.__buttons = [{
            text:cfg.cancelTitle||NodomUI.getText('cancel'),
            theme:'default',
            callback:'__close'
        }];
        if(Array.isArray(cfg.buttons)){
            mdl.__buttons = mdl.__buttons.concat(cfg.buttons);
        }
    }

    /**
     * alert 对话框
     * @param cfg - alert 配置
     * @returns 
     */
    public static alert(cfg){
        const mdl:UIMessageBox = <UIMessageBox>ModuleFactory.getMain().getModule('UIMessageBox');
        if(!mdl){
            return;
        }
        //修改状态，避免不被渲染
        mdl.model['text'] = cfg.text;
        mdl.model['type'] = 'alert';
        mdl.model['__open'] = true;
        mdl.model['__show'] = true;
        mdl.__buttons = [{text:NodomUI.getText('ok'),theme:'active',callback:'__close'}];
    }

    /**
     * prompt 对话框
     * @param cfg - prompt配置
     */
    public static prompt(cfg){
        const mdl:UIMessageBox = <UIMessageBox>ModuleFactory.getMain().getModule('UIMessageBox');
        if(!mdl){
            return;
        }
        //修改状态，避免不被渲染
        mdl.model['text'] = cfg.text;
        mdl.model['answer'] = cfg.answer;
        mdl.model['type'] = 'prompt';
        mdl.model['__open'] = true;
        mdl.model['__show'] = true;
        mdl.__buttons = [{text:cfg.cancelTitle||NodomUI.getText('cancel'),theme:'default',callback:'__close'}];
        if(Array.isArray(cfg.buttons)){
            mdl.__buttons = mdl.__buttons.concat(cfg.buttons);
        }
    }
}
/**
 * confirm创建函数
 * @param cfg - 配置项
 * @example
 * ```js
 *  {
 *      text:消息内容,
 *      icon:消息图标,
 *      buttons:[{
 *          text:按钮标题,
 *          callback:回调函数
 *      },]
 *  }
 * ```
 * @public
 */
export function nuiconfirm(cfg){
    UIMessageBoxManager.confirm(cfg);
}

/**
 * alert 创建函数
 * @param cfg - 配置项
 * @example
 * ```js
 *  {
 *      text:消息内容,
 *      icon:消息图标
 *  }
 * ```
 * @public
 */
export function nuialert(cfg){
    UIMessageBoxManager.alert(cfg);
}

/**
 * confirm创建函数
 * @param cfg - 配置项
 * @example
 * ```json
 * {
 *      text:消息内容,
 *      answer:默认回答,
 *      icon:消息图标,
 *      buttons:[{
 *          text:按钮标题,
 *          callback:回调函数
 *      },...]
 *  }
 * ```
 * @public
 */
export function nuiprompt(cfg){
    UIMessageBoxManager.prompt(cfg);
}

//注册插件
Nodom.registModule(UIMessageBox,'ui-messagebox');