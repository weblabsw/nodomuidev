import { BaseInput } from "./baseinput";
/**
 * 配置说明
 * $data：          列表数据数组
 * field:           绑定父模块的字段
 * value-field：    值字段名
 * disable-field：  禁用字段名
 * onItemClick：    点击事件
 */
/**
 * UIList 列表
 * @public
 */
export declare class UIList extends BaseInput {
    /**
     * 绑定字段名
     */
    private __valueField;
    /**
     * 禁用字段名
     */
    private __disableField;
    /**
     * 是否多选
     */
    private __multiple;
    /**
     * 点击选项事件
     */
    private __onItemClick;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 点击item
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __clickItem(model: any, dom: any, evObj: any, e: any): void;
    /**
     * 设置值
     */
    protected __initValue(): void;
}
