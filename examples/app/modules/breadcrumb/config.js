export const EXCONFIG = {
    name: 'Breadcrumb',
    lowerCaseName: 'breadcrumb',
    desc: '使用ui-breadcrumb/ui-breadcrumb-item组件进行设置。显示当前页面的路径，快速返回之前的任意页面。',
    breadcrumbAttrs: [{
        name: 'separator',
        desc: '分隔符',
        type: 'string',
        values: '-',
        defaults: '/'
    }, {
        name: 'icon-separator',
        desc: '图标分隔符',
        type: 'string',
        values: '参考 Icon 组件',
        defaults: '-'
    }],
    breadcrumbItemAttrs: [{
        name: 'to',
        desc: '路由跳转目标',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'title',
        desc: '当前一级页面的标题',
        type: 'string',
        values: '-',
        defaults: '-'
    },

    ],
    examples: [{
        title: '基础用法',
        desc: "在 ui-breadcrumb 中使用 ui-breadcrumb-item 标签表示从首页开始的每一级。 该组件接受一个 String 类型的参数 separator来作为分隔符。 默认值为 '/'。",
        name: 'ex1'
    }, {
        title: '图标分隔符',
        desc: '通过设置 icon-separator 可使用相应的 icon 作为分隔符，注意如果设置了 separator ， icon-separator 将失效',
        name: 'ex2'
    }, {
        title: '插槽',
        desc: 'ui-breadcrumb-item中可以使用插槽自定义默认内容',
        name: 'ex3'
    },
    ]
}