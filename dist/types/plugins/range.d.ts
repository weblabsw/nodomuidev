import { BaseInput } from "./baseinput";
/**
 * radio group插件
 */
export declare class UIRange extends BaseInput {
    private __type;
    template(props: any): string;
    onBeforeFirstRender(): void;
    __initValue(): void;
}
