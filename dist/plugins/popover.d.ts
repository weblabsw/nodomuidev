import { Module } from "nodom3";
export declare class UIPopover extends Module {
    /**
     * 弹出位置
     */
    private placement;
    template(props?: object): string;
}
