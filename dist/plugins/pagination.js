import { Nodom, Module } from "nodom3";
import { NodomUI } from "./nodomui";
/**
 * $total       total值
 * 参数说明
 * ps-array：   页面大小数组，数组字符串，默认[10,20,30,50]
 * show-total:  显示总记录数
 * show-jump:   显示跳转
 * show-num:    显示页面号的数量，默认10
 * big-step:    一大步移动页数，默认5
 * onChange:    页号或页面大小改变时执行方法名
 */
/**
 * UIPagination 分页
 * @public
 */
export class UIPagination extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__watch();
        this.__onChange = props['onchange'];
        this.__showNum = props['show-num'] ? parseInt(props['show-num'] + '') : 10;
        this.__bigStep = props['big-step'] ? parseInt(props['big-step']) : 5;
        if (props['page-no'] && !this.model['pageNo']) {
            this.model['pageNo'] = parseInt(props['page-no'] + '');
        }
        if (props['page-size'] && !this.model['pageSize']) {
            this.model['pageSize'] = parseInt(props['page-size'] + '');
        }
        //页面大小选择列表
        let pageArrStr = '';
        if (props.hasOwnProperty('ps-array')) {
            if (!this.model['pageSizeArr']) {
                const sizeArr = [];
                const a = JSON.parse(props['ps-array']);
                for (const d of a) {
                    sizeArr.push({
                        size: d,
                        text: d + NodomUI.getText('pageSize')
                    });
                }
                this.model['pageSizeArr'] = sizeArr;
            }
            pageArrStr = `
                <ui-select style='width:120px' field='pageSize' $data={{pageSizeArr}} value-field='size' display-field='text'>
                    {{text}}
                </ui-select>
            `;
        }
        //共*条
        let totalStr = '';
        if (props.hasOwnProperty('show-total')) {
            totalStr = `<div class='ui-pagination-total-wrap'>${NodomUI.getText('total')}<span class="ui-pagination-total">{{total}}</span>${NodomUI.getText('totalUnit')}</div>`;
        }
        //跳转到
        let goStr = '';
        if (props.hasOwnProperty('show-jump')) {
            goStr = `<div class="ui-pagination-go">
                    ${NodomUI.getText('pagePre')}<ui-input type="number" field='pageNo' min='1' max={{pageCount}} />${NodomUI.getText('page')}
                </div>`;
        }
        //设置不渲染属性
        this.setExcludeProps(['onchange', 'show-num', 'show-go', 'ps-array', 'big-step']);
        return `
            <div class='ui-pagination'>
                ${totalStr}
                <if cond={{total>0}} class='ui-pagination-data-wrap' tag='div'>
                    ${pageArrStr}
                    <div class="ui-pagination-page-wrap">
                        <b class={{'ui-pagination-leftarrow1' + (pageNo===1?' ui-pagination-disable':'')}}  e-click='__reduceMore'/>
                        <b class={{'ui-pagination-leftarrow' + (pageNo===1?' ui-pagination-disable':'')}}  e-click='__reduceOne'/>
                        <span x-repeat={{pages}}
                            class={{'ui-pagination-page' + (this.model.pageNo===page?' ui-pagination-active':'')}}
                            e-click='__clickPage'>
                            {{page}}
                        </span>
                        <b class={{'ui-pagination-rightarrow' + (pageNo===pageCount?' ui-pagination-disable':'')}} e-click='__addOne'/>
                        <b class={{'ui-pagination-rightarrow1' + (pageNo===pageCount?' ui-pagination-disable':'')}}  e-click='__addMore'/>
                    </div>
                    ${goStr}
                </if>
                <else>
                    ${NodomUI.getText('noData')}
                </else>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            pageNo: 0,
            pageSize: 0,
            total: 0,
            pages: []
        };
    }
    /**
     * 页号修改钩子
     */
    __changePage() {
        if (!this.model['pageNo'] || !this.model['pageSize']) {
            return;
        }
        const pm = this.getParent();
        //事件回调
        if (this.__onChange && pm) {
            this.invokeOuterMethod(this.__onChange, this.model['pageNo'], this.model['pageSize']);
        }
    }
    __watch() {
        if (!this.__watched) {
            this.watch(this.model, ['pageNo', 'pageSize', 'total'], (model, key, ov, nv) => {
                if (key === 'pageNo') {
                    this.model['pageNo'] = parseInt(nv + '');
                    this.__changePage();
                }
                else if (key === 'pageSize') {
                    this.model['pageCount'] = Math.ceil(model['total'] / this.model['pageSize']);
                    this.__changePage();
                }
                else if (key === 'total') {
                    this.model['pageCount'] = Math.ceil(nv / this.model['pageSize']);
                }
                this.__cacPages();
            });
            this.__watched = true;
        }
    }
    /**
     * 设置页面
     * @param page - 页面号
     */
    __setPage(page) {
        if (!page) {
            page = this.model['pageNo'];
        }
        const count = this.model['pageCount'];
        if (page > count) {
            page = count;
        }
        else if (page < 1) {
            page = 1;
        }
        this.model['pageNo'] = page;
    }
    /**
     * 点击页
     * @param model - 当前节点model
     */
    __clickPage(model) {
        this.__setPage(model.page);
    }
    /**
     * 页号减1
     */
    __reduceOne() {
        if (this.model['pageNo'] === 1) {
            return;
        }
        this.__setPage(this.model['pageNo'] - 1);
    }
    /**
     * 页号加1
     */
    __addOne() {
        if (this.model['pageNo'] === this.model['pageCount']) {
            return;
        }
        this.__setPage(this.model['pageNo'] + 1);
    }
    /**
     * 页号减bigStep
     */
    __reduceMore() {
        if (this.model['pageNo'] === 1) {
            return;
        }
        this.__setPage(this.model['pageNo'] - this.__bigStep);
    }
    /**
     * 页号加bigStep
     */
    __addMore() {
        if (this.model['pageNo'] === this.model['pageCount']) {
            return;
        }
        this.__setPage(this.model['pageNo'] + this.__bigStep);
    }
    /**
     * 计算最大最小页号
     */
    __cacPages() {
        const step = this.__showNum / 2 | 0;
        let minPage;
        let maxPage;
        const count = this.model['pageCount'] || 0;
        if (count === 0) {
            minPage = 0;
            maxPage = 0;
            this.model['pages'] = [];
            return;
        }
        else if (count <= this.__showNum) {
            minPage = 1;
            maxPage = count;
        }
        else { //页面数大于显示数
            minPage = this.model['pageNo'] - step;
            maxPage = this.model['pageNo'] + step;
            if (maxPage - minPage === this.__showNum) {
                maxPage--;
            }
            //处理page范畴
            if (minPage > count) {
                minPage = count - this.__showNum + 1;
                maxPage = count;
            }
            else if (minPage < 1) {
                maxPage += 1 - minPage;
                minPage = 1;
            }
            if (maxPage < 1) {
                minPage = 1;
                maxPage = this.__showNum;
            }
            else if (maxPage > count) {
                minPage = count - this.__showNum + 1;
                maxPage = count;
            }
        }
        //重新计算pages
        let pages = this.model['pages'];
        if (pages.length === 0 || pages[0].page !== minPage || pages[pages.length - 1].page !== maxPage) {
            let finded = false;
            let pno = this.model['pageNo'];
            pages = [];
            for (let i = minPage; i <= maxPage; i++) {
                pages.push({ page: i });
                if (pno === i) {
                    finded = true;
                }
            }
            this.model['pages'] = pages;
            if (!finded) {
                //pageNo不在max min内，需要重新设置
                if (pno < minPage) {
                    pno = minPage;
                }
                else if (pno > maxPage) {
                    pno = maxPage;
                }
                this.model['pageNo'] = pno;
            }
        }
    }
}
//注册模块
Nodom.registModule(UIPagination, 'ui-pagination');
//# sourceMappingURL=pagination.js.map