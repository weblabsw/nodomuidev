import { Module, VirtualDom } from "nodom3";
/**
 * 表格组件
 * 配置参数
 *  $data           表格数据
 *  row-alt         行颜色交替标志，不用设置值
 *  grid-line       网格线类型，包括cols(列) rows(行) both(行列)，默认无
 *  fix-head        是否固定表头，默认false
 *  checkable       是否显示复选框，默认false
 *  single          支持单选，当配置checkable时有效，默认false
 *  onSelectChange  选中更改时触发事件，只针对单行选中有效，传入参数为当前行model，对于头部check框选中无效
 *  onRowClick      行单击事件
 *  onRowDblClick   行双击事件
 *  onRowExpand     行展开事件
 *  onRowCollapse   行从展开到闭合时事件
 */
/**
 * UIGrid 表格
 * @public
 */
export declare class UIGrid extends Module {
    /**
     * 行交替
     */
    private __rowAlt;
    /**
     * 网格线 rows cols both
     */
    private __gridLine;
    /**
     * 固定头部
     */
    private __fixHead;
    /**
     * 表格宽度
     */
    private __width;
    /**
     * 表头选中状态 0未选中 1选中 2部分选中
     */
    private __headCheck;
    /**
     * 展开dom
     */
    private __expandDom;
    /**
     * 选中的model，multiple为false时有效
     */
    private __selectedModel;
    /**
     * 是否是单选
     */
    private __singleSelect;
    /**
     * 选择修改事件名
     */
    private __onSelectChange;
    /**
     * 行单击事件
     */
    private __onRowClick;
    /**
     * 行双击事件
     */
    private __onRowDblClick;
    /**
     * 行展开事件
     */
    private __onRowExpand;
    /**
     * 行闭合事件
     */
    private __onRowCollapse;
    /**
     * 是否排序
     */
    private __sortable;
    /**
     * 列集合
     */
    private __columns;
    /**
     * 排序字段名
     */
    private __sortField;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 编译后事件，动态添加列到body
     */
    onCompile(): void;
    /**
     * 生产grid class
     * @returns     grid class
     */
    private __genGridCls;
    /**
     * 产生grid width style，用于body和head
     * @returns style样式
     */
    private __genWidthStyle;
    /**
     * 获取表格宽度
     * @returns
     */
    private __genGridWidth;
    /**
     * 产生body css
     * @returns css串
     */
    private __genBodyCls;
    /**
     * 生成checkbox class
     * @param  st - 状态 0未选中 1全选中 2部分选中
     * @returns     checkbox 的class
     */
    private __genCheckCls;
    /**
     * 点击expand
     * @param model - 对应模型
     */
    private __clickExpand;
    /**
     * 行点击
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __rowClick;
    /**
     * 行双击
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dblClick;
    /**
     * 点击头部checkbox
     */
    private __clickHeadCheck;
    /**
     * 点击行 checkbox
     * @param model - 点击项model
     */
    private __clickCheck;
    /**
     * 设置排序字段
     * @param field - 待排序字段
     * @param type -  1升序  -1降序
     */
    __setSortField(field: string, type: number): void;
    /**
     * 创建数组数据
     * @returns     新数据
     */
    private __genData;
    /**
     * 滚动表格body
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __scrollBody;
    /**
     * 添加记录
     * @param rows - 数据(数组)
     */
    __addRow(rows: any): void;
    /**
     * 删除记录
     * @param param -     对象参数，用于查找符合该参数条件的所有数据
     */
    __removeRow(param: any): void;
    /**
     * 获取选中记录
     * @returns     选中的记录集
     */
    __getSelectedRows(): any;
    /**
     * 添加列
     * @param col - UIGridCol
     */
    __addColumn(col: UIGridCol): void;
    /**
     * 设置展开节点
     * @param expand -    expand 组件实例
     */
    __setExpandDom(expand: UIGridExpand): void;
    /**
     * 清空列
     */
    __clearColumns(): void;
    /**
     * 为dom及其子节点设置新key
     * @param dom - 待设置key的dom节点
     */
    private __genNewKey;
}
/**
 * grid列项组件
 * 配置参数
 *  title           列标题
 *  width           宽度，不带单位，使用时直接按照px计算，如果不设置，则默认flex:1，如果自动铺满，最后一列不设置宽度
 *  sortable        是否支持排序，true/false
 *  field           如果sortable，则需要设置，以该字段排序，如果没设置列显示内容，则默认显示此项对应的值
 *  head-style     表头列样式
 *  body-style      表body列样式
 */
/**
 * UIGridCol 表格列项组件
 *  @public
 */
export declare class UIGridCol extends Module {
    /**
     * 列标题
     */
    title: string;
    /**
     * 列宽
     */
    width: number;
    /**
     * 绑定数据项名
     */
    field: string;
    /**
     * 是否排序
     */
    sortable: boolean;
    /**
     * 固定类型，left和right，默认left
     */
    fixed: string;
    /**
     * 表格头部列dom
     */
    headDom: VirtualDom;
    /**
     * 表格body列dom
     */
    bodyDom: VirtualDom;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 编译事件
     * @privateRemarks
     */
    onCompile(): void;
    /**
     * 升序
     */
    private raiseSort;
    /**
     * 降序
     */
    private downSort;
}
/**
 * UIGridExpand 行展开内容
 * @public
 */
export declare class UIGridExpand extends Module {
    node: VirtualDom;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): any;
}
