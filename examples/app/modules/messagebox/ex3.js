import {Module,nuiprompt} from '/dist/nodomui.min.js'

export class messageboxex3 extends Module{
    template(props) {
        return`
            <div>
                <ui-button title='默认prompt对话框' e-click='showPrompt'/>&nbsp;
                <ui-button title='带answer的prompt对话框' e-click='showPrompt1'/>
                <ui-messagebox />        
            </div>
        `;
    }
    showPrompt(){
        nuiprompt({
            text:'你觉得nodom好用吗？',
            icon:'prompt',
            cancelTitle:'关闭',
            buttons:[
                {
                    text:'确定',
                    theme:'active',
                    callback:(model)=>{
                        console.log('answer is:',model.answer);
                    }
                }
            ]
        });
    }

    showPrompt1(){
        nuiprompt({
            text:'你觉得nodom好用吗？',
            answer:'是的，好用',
            icon:'prompt',
            buttons:[
                {
                    text:'确定',
                    theme:'active',
                    callback:(model)=>{
                        console.log('answer is:',model.answer);
                    }
                }
            ]
        });
    }
}