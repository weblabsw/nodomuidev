import { Module, nuitip } from '/dist/nodomui.min.js'
export class tipsex4 extends Module {
    template(props) {
        return `
            <div>
                <ui-button e-click='showtip4' title="icon图标的显示"></ui-button>
                <ui-tip />
            </div>
        `;
    }
    data() {
        return {
            index: 1
        }
    }

    showtip4(model) {
        nuitip({
            text: 'hello top' + model.index++,
            closable: true,
            icon: 'close',
        });

    }
}