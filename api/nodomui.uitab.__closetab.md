<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UITab](./nodomui.uitab.md) &gt; [\_\_closeTab](./nodomui.uitab.__closetab.md)

## UITab.\_\_closeTab() method

关闭页签

**Signature:**

```typescript
__closeTab(data: TabItem | string | number): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  data | [TabItem](./nodomui.tabitem.md) \| string \| number | tab config对象 或 title 或index |

**Returns:**

void


