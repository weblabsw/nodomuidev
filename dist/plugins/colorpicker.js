import { ModuleFactory, Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
import Color from "./color";
/**
 * UIColorPicker 颜色采集器
 * @alpha
 */
export class UIColorPicker extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        return `
            <div class="ui-color-picker">
                 <div class="ui-color-picker-dropdown ui-color-picker-wrapper">
                    <ui-sv-panel field="color"/>
                    <ui-hue-slider field="color"/>
                </div>
                <div class="ui-color-picker-input">
                    <ui-input field="__value" readonly allow-clear/>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            //color类保存颜色相关属性，并计算
            color: new Color({ enableAlpha: this.__enableAlpha, format: '', value: this.__value }),
            value: ''
        };
    }
    /**
     * 第一次渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender() {
        super.onBeforeFirstRender();
        this.model['__value'] = this.model['color'].value;
    }
    /**
     * 当svPanel或 hueSlider改变时，调用此函数更新value
     */
    __valueUpdate() {
        this.model['__value'] = this.model['color'].value;
    }
}
/**
 * 颜色采集板
 */
class UISvPanel extends BaseInput {
    template(props) {
        super.template(props);
        const bgc = this.model['background'];
        return `
            <div class='ui-sv-panel' style={{'background-color:' + '${bgc}'}} e-mousedown='__dragStart' >
              <div class='ui-sv-panel-white' />
              <div class='ui-sv-panel-black' />
              <div class='ui-sv-panel-cursor' style={{__cursorStyle()}}></div>  
            </div>
        `;
    }
    data() {
        return {
            cursorTop: 0,
            cursorLeft: 0,
            background: 'hsl(0, 100%, 50%)',
        };
    }
    /**
     * 设置取色点位置
     */
    __cursorStyle() {
        return 'top:' + this.model['cursorTop'] + 'px;' + 'left:' + this.model['cursorLeft'] + 'px';
    }
    /**
     * 开始拖拽
     * @param model -     当前节点对应model
     * @param dom -       virtual dom节点
     * @param eobj -      NEvent对象
     * @param e -         event对象
     */
    __dragStart(model, dom, eobj, e) {
        e.preventDefault();
        let isDrag = true;
        document.addEventListener('mousemove', drag);
        document.addEventListener('mouseup', dragEnd);
        const that = this;
        /**
         * 拖拽
         * @param event - 事件对象
         */
        function drag(event) {
            if (isDrag) {
                handleEvent(event);
            }
        }
        /**
         * 拖拽结束或一次点击结束
         * @param event -
         */
        function dragEnd(event) {
            if (isDrag) {
                handleEvent(event);
                document.removeEventListener('mousemove', drag);
                document.removeEventListener('mouseup', dragEnd);
                isDrag = false;
            }
        }
        /**
         * 具体处理事件函数
         * @param event - 事件对象
         */
        function handleEvent(event) {
            //ui-sv-panel
            const el = that.getElement(that.domManager.vdomTree.key);
            const rect = el.getBoundingClientRect();
            const clientX = event.clientX;
            const clientY = event.clientY;
            //开始计算cursor的left和top
            let left = clientX - rect.left;
            let top = clientY - rect.top;
            left = Math.max(0, left);
            left = Math.min(left, rect.width);
            top = Math.max(0, top);
            top = Math.min(top, rect.height);
            //设置
            model.cursorTop = top;
            model.cursorLeft = left;
            //这里调用color类
            model.__value.set({
                saturation: (left / rect.width) * 100,
                value: 100 - (top / rect.height) * 100,
            });
            that.__update(false);
            //svPanel改变，更新colorPicker颜色值
            const cP = that.getParent();
            cP.invokeMethod('__valueUpdate');
        }
    }
    /**
     * 得到数值后更新
     * @param isSlider - 是否为slider调用
     */
    __update(isSlider) {
        //这里更新background-color等操作
        const color = this.model['__value'];
        if (!isSlider) {
            const saturation = color.get('saturation');
            const value = color.get('value');
            const el = this.getElement(this.domManager.vdomTree.key);
            const { clientWidth: width, clientHeight: height } = el;
            this.model['cursorLeft'] = (saturation * width) / 100;
            this.model['cursorTop'] = ((100 - value) * height) / 100;
        }
        this.model['background'] = `hsl(${color.get('hue')}, 100%, 50%)`;
    }
}
/**
 *  色相选择器
 */
class UIHueSlider extends BaseInput {
    template(props) {
        super.template(props);
        return `
            <div class="ui-hue-slider">
                <div class="ui-hue-slider-bar" e-mousedown='__clickBar'></div>
                <div class="ui-hue-slider-thumb" style={{__thumbStyle()}} e-mousedown='__dragStart'> </div>
            </div>
        `;
    }
    data() {
        return {
            thumbTop: 0,
        };
    }
    onMount() {
        this.__barEl = this.getElement(this.domManager.vdomTree.children[0].key);
        this.__El = this.getElement(this.domManager.vdomTree.children[1].key);
    }
    __thumbStyle() {
        return 'top:' + this.model['thumbTop'] + 'px';
    }
    /**
     * 点击Bar
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __clickBar(model, dom, evObj, e) {
        const target = e.target;
        if (target != this.__El) {
            this.__handleEvent(e);
        }
    }
    /**
     * 拖拽开始，获取鼠标位置及dot位置
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragStart(model, dom, evObj, e) {
        e.preventDefault();
        let isDrag = true;
        document.addEventListener('mousemove', drag);
        document.addEventListener('mouseup', dragEnd);
        const that = this;
        /**
         * 拖拽
         * @param event - 事件对象
         */
        function drag(event) {
            if (isDrag) {
                that.__handleEvent(event);
            }
        }
        /**
         * 拖拽结束或一次点击结束
         * @param event - 事件对象
         */
        function dragEnd(event) {
            if (isDrag) {
                that.__handleEvent(event);
                document.removeEventListener('mousemove', drag);
                document.removeEventListener('mouseup', dragEnd);
                isDrag = false;
            }
        }
    }
    /**
     * 处理事件
     * @param event - 事件对象
     */
    __handleEvent(event) {
        const el = this.getElement(this.domManager.vdomTree.key);
        const rect = el.getBoundingClientRect();
        const clientY = event.clientY;
        let top = clientY - rect.top;
        top = Math.min(top, rect.height - this.__El.offsetHeight / 2);
        top = Math.max(this.__El.offsetHeight / 2, top);
        const hue = Math.round(((top - this.__El.offsetHeight / 2) /
            (rect.height - this.__El.offsetHeight)) *
            360);
        const model = this.model;
        const color = model['__value'];
        color.set('hue', hue);
        model['thumbTop'] = top;
        //hue改变 设置svPanel背景
        const panel = ModuleFactory.get(this.getParent().children[0]);
        panel.invokeMethod('__update', true);
        //更新colorPicker颜色值
        const cP = this.getParent();
        cP.invokeMethod('__valueUpdate');
    }
    /**
     * 更新Thumb
     */
    getThumbTop() {
        const model = this.model;
        const color = model['__value'];
        const el = this.getElement(this.domManager.vdomTree.key);
        const hue = color.get('hue');
        model['thumbTop'] = Math.round((hue * (el.offsetHeight - this.__El.offsetHeight / 2)) / 360);
    }
}
Nodom.registModule(UIColorPicker, 'ui-color-picker');
Nodom.registModule(UISvPanel, 'ui-sv-panel');
Nodom.registModule(UIHueSlider, 'ui-hue-slider');
//# sourceMappingURL=colorpicker.js.map