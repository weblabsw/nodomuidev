import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import{EXCONFIG} from './config.js'
import{listtransferex1} from './ex1.js'
import{listtransferex2} from './ex2.js'


export class MListTransfer extends Module{
    modules=[ExModule,AttrGrid,EventGrid,MethodGrid,listtransferex1,listtransferex2];
    template(props){
        return `
            <div class='--component'>
                <h1>ListTransfer 穿梭框</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='listtransfer'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{listtransferAttrs}} />
                
            </div>
        `
    }
    data(){
        return EXCONFIG;
    }
}