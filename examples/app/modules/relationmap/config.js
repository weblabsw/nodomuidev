export const EXCONFIG = {
    name:'Relationmap',
    desc:'使用ui-relationmap组件进行设置。',
    relationMapAttrs:[{
        name:'$data',
        desc:'关系图数据，需包含rows和cols',
        type:'json array',
        values:'-',
        defaults:'-'
    },{
        name:'field',
        desc:'已选中数据数组',
        type:'array',
        values:'-',
        defaults:'-'
    },{
        name:'display-field',
        desc:'用于显示的字段',
        type:'string',
        values:'-',
        defaults:'-'
    },{
        name:'value-field',
        desc:'选定值字段',
        type:'string',
        values:'-',
        defaults:'-'
    }],
    examples:[{
        title:'简单应用',
        desc: '通过display-field，value-field进行设置显示，使用$data和field进行数据绑定。',
        name:'ex1'
    }]
}