import{Module} from '/dist/nodomui.min.js'
export class ratingex5 extends Module{
    template() {
        return `
            <div>
                <ui-rating field="score3" allow-half>{{score3}}</ui-rating>
            </div>
        `
    }
    data() {
        return {
            score3: 3.5
        }
    }
}