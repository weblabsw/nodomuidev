export const EXCONFIG = {
    name:'Input',
    desc:'使用ui-input组件进行设置。',
    inputAttrs:[{
        name:'field',
        desc:'绑定字段名',
        type:'string',
        values:'-',
        defaults:'-'
    },{
        name:'allow-clear',
        desc:'是否允许清空，无值属性',
        type:'无',
        values:'-',
        defaults:'-'
    },{
        name:'show-pwd',
        desc:' 是否允许密码显示和隐藏方式切换，无值属性，type为password时有效',
        type:'无',
        values:'-',
        defaults:'-'
    },{
        name:'name',
        desc:'原生属性，input名称，可参考input标签',
        type:'-',
        values:'-',
        defaults:'-'
    },{
        name:'type',
        desc:'原生属性，input类型，参考input标签，可设置为textarea',
        type:'-',
        values:'password,textarea...',
        defaults:'-'
    },{
        name:'placeholder',
        desc:'原生属性，当没有值设定时，出现在input上的文字，可参考input标签',
        type:'-',
        values:'-',
        defaults:'-'
    },{
        name:'min',
        desc:'原生属性，最小值，可参考input标签',
        type:'-',
        values:'-',
        defaults:'-'
    },{
        name:'max',
        desc:'原生属性，最大值，可参考input标签',
        type:'-',
        values:'-',
        defaults:'-'
    },{
        name:'minlength',
        desc:'原生属性，最小长度，可参考input标签',
        type:'-',
        values:'-',
        defaults:'-'
    },{
        name:'maxlength',
        desc:'原生属性，最大长度，可参考input标签',
        type:'-',
        values:'-',
        defaults:'-'
    },{
        name:'disabled',
        desc:'原生属性，是否禁用，可参考input标签',
        type:'无',
        values:'-',
        defaults:'-'
    },{
        name:'readonly',
        desc:'原生属性，值是否可编辑，可参考input标签',
        type:'无',
        values:'-',
        defaults:'-'
    },{
        name:'rows',
        desc:'原生属性，元素的输入文本的行数（显示的高度），可参考textarea标签',
        type:'-',
        values:'-',
        defaults:'-'
    },{
        name:'cols',
        desc:'原生属性，文本域的可视宽度，可参考textarea标签',
        type:'-',
        values:'-',
        defaults:'-'
    },{
        name:'resize',
        desc:'原生属性，设置元素是否可调整尺寸，可参考textarea标签',
        type:'string',
        values:'none,both...',
        defaults:'-'
    }],

    examples:[{
        title:'简单应用',
        desc:'通过field绑定输入值',
        name:'ex1'
    },{
        title:'禁用',
        desc:'通过disabled设置',
        name:'ex2'
    },{
        title:'清空输入框',
        desc:'通过allow-clear设置。',
        name:'ex3'
    },{
        title:'密码隐藏',
        desc:'通过show-pwd设置是否显示密码',
        imp:'type为password时有效',
        name:'ex4'
    },{
        title:'自定义前置和后置内容',
        desc:'通过插槽slot name属性设置。',
        name:'ex5'
    },{
        title:'允许调整尺寸',
        desc:'通过resize进行设置。',
        name:'ex6'
    },{
        title:'不允许编辑',
        desc:'通过readonly进行设置。',
        name:'ex7'
    }]
}