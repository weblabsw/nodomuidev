import{Module} from '/dist/nodomui.min.js'
export class dateex1 extends Module{
    template(props){
        return `
            <div>
                <div>日期：{{birth}}</div>
                <ui-date field="birth" format='yy/MM/dd'/>
            </div>
        `;
    }
    data(){
        return {
            birth:'2018-01-01'
        }
    }

}