import { Nodom, Util } from 'nodom3';
import { BaseInput } from './baseinput';
import { UITool } from './uibase';
import { NodomUI } from './nodomui';
/**
 * date 插件
 * 配置说明：
 * use-timestamp:   是否使用时间戳，如果使用，则对应value为数字，否则为日期串
 * format:          日期格式，参考ISO 日期字符串，默认为 yyyy-MM-dd
 */
/**
 * UIDate 日期选择器
 * @public
 */
export class UIDate extends BaseInput {
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__useTimestamp = props.hasOwnProperty('use-timestamp');
        this.__format = props['format'] || 'yyyy-MM-dd';
        return `
            <div class='ui-datetime' e-mouseleave='__closeBox' >
                <div class='ui-datetime-field'  e-click='__toggleBox'>
                    <input value={{__toDateStr()}} />
                    <b class='ui-datetime-date' />
                </div>
                <div x-animationbox class='ui-datetime-picker-wrap' 
                    style={{'left:' + __x + 'px;top:' + __y + 'px;width:' + __width + 'px;'}}>
                    <div class='ui-datetime-picker'>
                        <div>
                            <div class='ui-datetime-tbl'>
                                <div class='ui-datetime-datetbl'>
                                    <div class='ui-datetime-ymct'>
                                        <b class='ui-datetime-leftarrow1' e-click='__subYear' />
                                        <b class='ui-datetime-leftarrow' e-click='__subMonth' />
                                        <span class='ui-datetime-ym'>{{ year + '/' + month }}</span>
                                        <b class='ui-datetime-rightarrow' e-click='__addMonth' />
                                        <b class='ui-datetime-rightarrow1' e-click='__addYear' />
                                    </div>
                                    <div class='ui-datetime-weekdays'>
                                        <span>${NodomUI.getText('sunday')}</span>
                                        <span>${NodomUI.getText('monday')}</span>
                                        <span>${NodomUI.getText('tuesday')}</span>
                                        <span>${NodomUI.getText('wednesday')}</span>
                                        <span>${NodomUI.getText('thursday')}</span>
                                        <span>${NodomUI.getText('friday')}</span>
                                        <span>${NodomUI.getText('saturday')}</span>
                                    </div>
                                    <div class='ui-datetime-dates'>
                                        <span x-repeat={{__dates}} class={{(selected?'ui-datetime-selected':'') + (disable?'ui-datetime-disable':'')}}
                                            e-click='__clickDate'
                                        >{{date}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class='ui-datetime-btnct'>
                                <ui-button title='${NodomUI.getText('today')}' e-click='__today'/>
                                <ui-button theme='active' title='${NodomUI.getText('ok')}' e-click='__okClick'/>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __open: false,
            __dates: []
        };
    }
    /**
     * 交替 list box
     * @param model - 对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __toggleBox(model, dom, evObj, e) {
        if (this.model['__open']) {
            this.__closeBox();
        }
        else {
            this.__openBox(model, dom, evObj, e);
        }
    }
    /**
     * 打开list box
     * @param model - 对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __openBox(model, dom, evObj, e) {
        this.__setDate(this.model['__value']);
        this.__cacLoc(model, dom, evObj, e);
        this.model['__open'] = true;
        // (<UIAnimation>this.getModule('uianimation')).__open();
    }
    /**
     * 关闭list box
     */
    __closeBox() {
        this.model['__open'] = false;
        // (<UIAnimation>this.getModule('uianimation')).__close();
    }
    /**
     * 点击item
     * @param model - 对应model
     */
    __clickDate(model) {
        if (!this.__field || model.disable) {
            return;
        }
        this.model['date'] = model.date;
        this.__setDate(this.model['year'] + '-' + this.model['month'] + '-' + this.model['date']);
    }
    /**
     * 设置当前日期
     */
    __today() {
        this.__setDate();
    }
    /**
     * 确定按钮
     */
    __okClick() {
        const dStr = this.model['year'] + '-' + (this.model['month'] > 9 ? this.model['month'] : ('0' + this.model['month'])) + '-' + (this.model['date'] > 9 ? this.model['date'] : '0' + this.model['date']);
        if (this.__useTimestamp) {
            this.model['__value'] = new Date(dStr).getTime();
        }
        else {
            this.model['__value'] = dStr;
        }
        this.__closeBox();
    }
    /**
     * 转换为日期串
     */
    __toDateStr() {
        if (!this.model['__value']) {
            return;
        }
        const d = typeof this.model['__value'] === 'string' ? new Date(this.model['__value']).getTime() : this.model['__value'];
        return Util.formatDate(d, this.__format);
    }
    /**
     * 设置日期或时间
     * @param str - 待设置值(日期串或时间戳)
     */
    __setDate(str) {
        let date;
        let d; //日期串对应日期
        if (str && str !== '') {
            date = new Date(str);
            if (date.toString() !== 'Invalid Date') {
                d = date.getDate();
            }
        }
        if (!d) {
            date = new Date();
            d = date.getDate();
        }
        this.model['date'] = d;
        this.__genDates(date.getFullYear(), date.getMonth() + 1);
    }
    /**
     * 计算位置
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e -  html event
     */
    __cacLoc(model, dom, evObj, e) {
        const height = Math.ceil(this.model['__dates'].length / 7) * 40 + 164;
        const pos = UITool.cacPosition(e, 1, 312, height);
        model.__x = pos[0];
        model.__y = pos[1];
        model.__width = 312;
    }
    /**
     * 产生日期数组
     * @param year - 年
     * @param month - 月
     */
    __genDates(year, month) {
        this.model['year'] = year;
        this.model['month'] = month;
        //获取当日
        const cda = new Date();
        const cy = cda.getFullYear();
        const cm = cda.getMonth() + 1;
        const cd = cda.getDate();
        const days = this.__cacMonthDays(year, month);
        const dayArr = [];
        let date = new Date(year + '-' + month + '-1');
        //周几
        let wd = date.getDay();
        const lastMonthDays = this.__cacMonthDays(year, month, -1);
        //补充1号对应周前几天日期
        for (let d = lastMonthDays, i = 0; i < wd; i++, d--) {
            dayArr.unshift({
                disable: true,
                selected: false,
                date: d
            });
        }
        //当月日期
        for (let i = 1; i <= days; i++) {
            dayArr.push({
                date: i,
                selected: this.model['year'] === year && this.model['month'] === month && this.model['date'] === i,
                today: cy === year && cm === month && cd === i
            });
        }
        //下月日期
        date = new Date(year + '-' + month + '-' + days);
        //周几
        wd = date.getDay();
        for (let i = wd + 1; i <= 6; i++) {
            dayArr.push({
                disable: true,
                selected: false,
                date: i - wd
            });
        }
        this.model['__dates'] = dayArr;
        this.model['days'] = dayArr;
    }
    /**
     * 计算一个月的天数
     * @param year - 年
     * @param month - 月
     * @param disMonth - 相差月数
     */
    __cacMonthDays(year, month, disMonth) {
        if (disMonth) {
            month += disMonth;
        }
        if (month <= 0) {
            year--;
            month += 12;
        }
        else if (month > 12) {
            year++;
            month -= 12;
        }
        if ([1, 3, 5, 7, 8, 10, 12].includes(month)) {
            return 31;
        }
        else if (month !== 2) {
            return 30;
        }
        else if (year % 400 === 0 || year % 4 === 0 && year % 100 !== 0) {
            return 29;
        }
        else {
            return 28;
        }
    }
    /**
     * 修改月份
     * @param distance - 差异量
     */
    __changeMonth(distance) {
        let year = this.model['year'];
        let month = this.model['month'];
        month += distance;
        if (month <= 0) {
            year--;
            month += 12;
        }
        else if (month > 12) {
            year++;
            month -= 12;
        }
        if (month <= 0) {
            year--;
            month += 12;
        }
        else if (month > 12) {
            year++;
            month -= 12;
        }
        this.__genDates(year, month);
    }
    /**
     * 年份-1
     */
    __subYear() {
        this.__changeMonth(-12);
    }
    /**
     * 年份+1
     */
    __addYear() {
        this.__changeMonth(12);
    }
    /**
     * 月份-1
     */
    __subMonth() {
        this.__changeMonth(-1);
    }
    /**
     * 月份+1
     */
    __addMonth() {
        this.__changeMonth(1);
    }
}
Nodom.registModule(UIDate, 'ui-date');
//# sourceMappingURL=date.js.map