import { Module } from "nodom3";
/**
 * 配置参数
 *  vertical    是否为纵向分布，默认false（横向分布）
 */
/**
 * UILayout 布局
 *  @public
 */
export declare class UILayout extends Module {
    template(props?: object): string;
}
/**
 * UILayoutTop 顶部布局
 * @public
 */
export declare class UILayoutTop extends Module {
    template(): string;
}
/**
 * UILayoutLeft 左侧布局
 * 必须设置宽度
 * @public
 */
export declare class UILayoutLeft extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 拖动开始事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragStart;
}
/**
 * UILayoutCenter 中心布局
 * @public
 */
export declare class UILayoutCenter extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): string;
}
/**
 * UILayoutRight 右部布局
 * @public
 */
export declare class UILayoutRight extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 拖动开始事件
     * @param model -     模型
     * @param dom -       dom节点
     * @param evObj -     nodom event对象
     * @param event -     html event对象
     */
    private __dragStart;
}
/**
 * UILayoutBottom 底部布局
 * @public
 */
export declare class UILayoutBottom extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): string;
}
