import{Module} from '/dist/nodomui.min.js'
export class badgeex3 extends Module{
    template(props){
        return `
            <div>
                <ui-badge dot>
                    <ui-button size="small"  title='评论' />
                </ui-badge>
                <ui-badge dot>
                    <b class='ui-icon-user' style="font-size:24px"></b>
                </ui-badge>

                <style>
                    .ui-badge{
                        margin-right:40px;
                        margin-bottom:10px;
                    }
                </style>
            </div>
        `;
    }
}