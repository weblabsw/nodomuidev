import { Module } from "nodom3";
/**
 * TabItem 配置接口类型
 * @public
 */
export declare type TabItem = {
    /**
     * tab标题
     */
    title: string;
    /**
     * 是否可关闭
     */
    closable?: boolean;
    /**
     * 是否激活
     */
    active?: boolean;
    /**
     * tab item 插件
     */
    tab: UITabItem;
};
/**
 * bgcolor:         默认背景色
 * color:           默认前景色
 * active-bgcolor:  激活背景色
 * active-color:    激活前景色
 * onTabChange:     tab页切换钩子函数，页面切换时调用
 */
/**
 * UITab 标签页
 * @public
 */
export declare class UITab extends Module {
    /**
     * 当前tab配置项
     */
    private __currentTab;
    /**
     * 拖动参数
     */
    private __dragParam;
    /**
     * tab 关闭事件
     */
    private __onTabClose;
    /**
     * tab 切换事件
     */
    private __onTabChange;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        tabs: any[];
    };
    /**
     * 添加tab
     * @param cfg - tab配置项
     */
    __addTab(cfg: TabItem): void;
    /**
     * 点击tab
     * @param model - 对应节点model
     */
    private __clickTab;
    /**
     * 切换到目标tab
     * @param tab - 目标tab
     * @returns
     */
    private __changeTo;
    /**
     * 获取tab
     * @param data - tab config对象 或 title 或index
     * @returns  tab
     */
    private __getTab;
    /**
     * 激活新tab
     * @param data - tab config对象 或 title 或index
     */
    __activeTab(data: TabItem | string | number): void;
    /**
     * 关闭页签
     * @param data - tab config对象 或 title 或index
     * @returns
     */
    __closeTab(data: TabItem | string | number): void;
    /**
     * 关闭所有tab
     */
    __closeAll(): void;
    /**
     * 头部拖动开始
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragStart;
    /**
     * 拖动
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     * @returns
     */
    private __drag;
    /**
     * 拖动停止
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragEnd;
    /**
     * 移动
     * @param e - 事件对象
     * @returns
     */
    private __move;
    private __setDefaultActive;
}
/**
 * 配置说明
 * title    tab标题
 * closable 是否可关闭
 * active   是否处于打开状态
 */
/**
 * UTabItem 标签页个体
 * @public
 */
export declare class UITabItem extends Module {
    /**
     * 标题
     */
    private __title;
    /**
     * 是否可关闭
     */
    private __closable;
    /**
     * 是否处于激活状态
     */
    private __active;
    /**
     * 已添加标志，如果已添加，则不再添加
     */
    private __added;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        show: boolean;
    };
    /**
     * 渲染前
     */
    onBeforeFirstRender(): void;
    /**
     * 隐藏
     */
    __hide(): void;
    /**
     * 显示
     */
    __show(): void;
}
