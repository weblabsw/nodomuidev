import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { selectex1 } from './ex1.js'
import { selectex2 } from './ex2.js'
import { selectex3 } from './ex3.js'
import { selectex4 } from './ex4.js'
import { selectex5 } from './ex5.js'
import { selectex6 } from './ex6.js'
export class MSelect extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, selectex1, selectex2, selectex3, selectex4, selectex5, selectex6];

    template(props) {
        return `
            <div class='--component'>
                <h1>Select 下拉框</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='select'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{selectAttrs}} />

            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}