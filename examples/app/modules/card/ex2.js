import { Module } from '/dist/nodomui.min.js'

export class cardex2 extends Module {
    template(props) {
        return `
                <div>
                    <ui-card  width='400px' height='220px'>
                        <div>
                            <p x-repeat={{rows}} index="idx">
                                index:{{idx}},name:{{name}},age:{{age}}<br>
                            </p>
                        </div>
                    </ui-card>
                </div>

            `;
    }

    data() {
        return {
            rows: [
                { name: "Nodom", age: 6 },
                { name: "Noomi", age: 4 },
                { name: "Relaen", age: 3 }
            ]
        }
    }
}