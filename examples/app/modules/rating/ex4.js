import { Module } from '/dist/nodomui.min.js'
export class ratingex4 extends Module {
    template() {
        return `
            <div>
                <ui-rating field="score2" max="7">{{score2}}</ui-rating>
            </div>
        `
    }
    data() {
        return {
            score2: 5
        }
    }
}