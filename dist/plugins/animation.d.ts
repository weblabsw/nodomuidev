import { Module } from "nodom3";
/**
 * 动画插件
 * 属性配置
 * type:类型  toright(从左到右展开),toleft(从右到左展开)，totop(从下到上展开)，tobottom(从上到下展开)，fade(通过fade方式展开)，默认tobottom
 * onBeforeOpen:open前事件方法名，如果返回true，则不打开
 * onOpen: open后事件方法名
 * onBeforeClose:close前事件方法名，如果返回true，则不关闭
 * onClose:close后事件方法名
 */
/**
 * UIAnimation 动画效果
 * @public
 */
export declare class UIAnimation extends Module {
    /**
     * 动画类型：slide/fade,默认slide
     */
    private __type;
    /**
     * 打开前事件名，如果该方法返回值为true，则不打开
     */
    private __onBeforeOpen;
    /**
     * 打开后事件名
     */
    private __onOpen;
    /**
     * 关闭前事件名，如果该方法返回值为true，则不打开
     */
    private __onBeforeClose;
    /**
     * 关闭后事件名
     */
    private __onClose;
    /**
模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 组件模型
     * @privateRemarks
     */
    data(): {
        __open: boolean;
        __show: boolean;
    };
    /**
     * 挂载事件
     * @privateRemarks
     */
    onMount(): void;
    /**
     * 更新事件
     * @privateRemarks
     */
    onUpdate(): void;
    /**
     * 设置大小
     */
    private __setRootSize;
    /**
     * 打开
     */
    __open(): void;
    /**
     * 关闭
     */
    __close(): void;
    /**
     * animation执行后进行状态检测
     */
    private __checkState;
}
