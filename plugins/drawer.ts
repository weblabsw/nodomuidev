import { Module,Nodom } from "nodom3"
import { UIPanelBody } from "./panel";
import { nuiconfirm } from "./messagebox"
import { UITool } from "./uibase";

/**
 * drawer插件
 * 参数说明
 * title:           标题
 * closable:        是否显示close按钮
 * modal:           控制是否显示遮罩层
 * showconfirm:     控制是否显示确认关闭消息框
 * size:            控制抽屉的大小
 * direction:       控制抽屉弹出的方向
 * onInner          点击抽屉时事件
 * onOpen:          drawer打开后事件方法名
 * onClose:         drawer关闭后事件方法名
 * onBeforeOpen:    drawer打开前事件名，如果对应方法返回true，则不打开
 * onBeforeClose:   drawer关闭前事件名，如果对应方法返回true，则不关闭
 */

/**
 * UIDrawer 抽屉
 * @beta
 */
export class UIDrawer extends Module{
    /**
     * 点击抽屉事件
     */
    private __onInner:string;

    /**
     * 打开时事件
     */
    private __onOpen:string;

    /**
     * 关闭时事件
     */
    private __onClose:string;

    /**
     * 打开前事件方法名
     */
    private __onBeforeOpen:string;

    /**
     * 关闭前事件方法名
     */
    private __onBeforeClose:string;
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props?:object):string{
        this.__onInner = props['oninner'];
        this.__onOpen = props['onopen'];
        this.__onClose = props['onclose'];
        this.__onBeforeClose = props['onbeforeclose'];
        this.__onBeforeOpen = props['onbeforeopen'];
        let closeStr = ''
        if(props.hasOwnProperty('closable')){
            closeStr = `<div class='ui-pannel-header-bar'>
                            <b class='ui-drawer-close' e-click='__close'></b>
                        </div>`
        }
        const boxType = props['direction'] === 'left' || props['direction'] === 'right' ? 'horizontal' : 'vertical';
        const boxPosition = props['direction'] === 'left' || props['direction'] === 'right' ? `height:100%;${props['direction']}:0;` : `width:100%;${props['direction']}:0;`;
        const style = boxType === 'horizontal' ? `width:${props['size']};height:100%` : `height:${props['size']};width:100%`;
        const modal = props.hasOwnProperty('modal') ? 'display:none':'';
        const showconfirm = props.hasOwnProperty('showconfirm') ? '__showConfirm':'__close'
        this.setExcludeProps(['title','onopen','onclose','closable','direction','size','modal','showconfirm'])
        return `
            <div class={{'ui-drawer' + (__show?'':' ui-dialog-hide')}} e-click='${showconfirm}'>
                <div class={{'ui-mask' + (__show?'':' ui-mask-hide')}} style='${modal}'/>
                <div x-animationbox='${boxType}' e-transitionend='__checkClose' style='position:absolute;${boxPosition}' > 
                    <div class="ui-drawer-content" style='padding:0px;${style}'>
                        <div style='${style}'>
                            <div class='ui-panel' style='${style}' e-click="__inner:nopopo">
                                <div class='ui-panel-header'>
                                    <span class='ui-panel-title'>${props['title']}</span>
                                    ${closeStr}
                                </div>
                                <div class='ui-panel-bodyct'>
                                    <slot />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    public data() {
        return {
            __show:false,
            __open:false,
        }
    }
    /**
     * 点击drawer外部时，弹出消息框询问是否关闭
     */
    private __showConfirm(){
        nuiconfirm({
            text:'确定关闭？',
            icon:'prompt',
            buttons:[
                {
                    text:'确定',
                    theme:'active',
                    callback:()=>{
                        if(this.__onBeforeClose && UITool.invokePropMethod(this,this.__onBeforeClose,this.model)){
                            return;
                        }
                        this.model['__open'] = false;
                    }
                }
            ]
        });
    }

    /**
     * 点击drawer时触发
     */
    public __inner(){
        if(this.__onInner){
            UITool.invokePropMethod(this,this.__onInner,this.model);
        }
    }

    /**
     * 关闭drawer
     */
    public __close(){
        if(this.__onBeforeClose && UITool.invokePropMethod(this,this.__onBeforeClose,this.model)){
            return;
        }
        this.model['__open'] = false;
    }

    /**
     * 打开drawer
     */
    public __open(){
        if(this.__onBeforeOpen && UITool.invokePropMethod(this,this.__onBeforeOpen,this.model)){
            return;
        }
        this.model['__show'] = true;
        this.model['__open'] = true;
        if(this.__onOpen){
            UITool.invokePropMethod(this,this.__onOpen,this.model);
        }
    }

    /**
     * 检查关闭
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    public __checkClose(model,dom,evObj,e){
        if(e.target !== this.domManager.renderedTree.children[1].node){
            return;
        }
        if(!this.model['__open']){
            this.model['__show'] = false;
            if(this.__onClose){
                UITool.invokePropMethod(this,this.__onClose,this.model);
            }
        }

    }
}

class UIDrawerBody extends UIPanelBody{}
//注册模块
Nodom.registModule(UIDrawer,'ui-drawer')
Nodom.registModule(UIDrawerBody,'ui-drawer-body')