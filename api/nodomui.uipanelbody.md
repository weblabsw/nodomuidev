<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UIPanelBody](./nodomui.uipanelbody.md)

## UIPanelBody class

UIPanelBody 面板主体

**Signature:**

```typescript
export declare class UIPanelBody extends Module 
```
**Extends:** Module

## Methods

|  Method | Modifiers | Description |
|  --- | --- | --- |
|  [template()](./nodomui.uipanelbody.template.md) |  | 模板函数 |

