import {Module} from "/dist/nodomui.min.js";
export class inputex2 extends Module{
    template(props) {
        return`
             <ui-input field="f2" disabled />
        `;
    }
    data(){
        return{
            f2:"noomi"
        }
    }
}