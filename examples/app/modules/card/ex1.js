import { Module } from '/dist/nodomui.min.js'

export class cardex1 extends Module {
    template(props) {
        return `
                <div>
                    <ui-card width='400px' height='220px'>
                        <div x-slot='header'>
                            这是header命名插槽
                        </div>

                        <div>
                            <p x-repeat={{rows}} index="idx">
                                index:{{idx}},name:{{name}},age:{{age}}<br>
                            </p>
                        </div>
                        
                        <div x-slot='footer'>
                            这是footer命名插槽
                        </div>
                    </ui-card>
                </div>

            `;
    }

    data() {
        return {
            rows: [
                { name: "Nodom", age: 6 },
                { name: "Noomi", age: 4 },
                { name: "Relaen", age: 3 }
            ]
        }
    }
}