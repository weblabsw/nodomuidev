
import { Module } from '/dist/nodomui.min.js'
export class selectex1 extends Module {
    template() {
        return `
            <div>
                <label>性别:</label>
                <ui-select
                    $data={{sexes}}
                    field='sexy'
                    value-field='value'
                    display-field='text'
                    style='width:100px'
                    class='cls1'>
                    {{text}}
                </ui-select>
            </div>
        `
    }

    data() {
        return {
            sexy: 'F',
            sexes: [{ text: '男', value: 'M' }, { text: '女', value: 'F' }]
        }
    }

    onBeforeFirstRender(model) {
        setTimeout(() => {
            this.model.sexy = 'M'
        }, 200);
    }

}