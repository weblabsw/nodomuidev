<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [ITipCfg](./nodomui.itipcfg.md) &gt; [width](./nodomui.itipcfg.width.md)

## ITipCfg.width property

宽度，单位为px，默认300

**Signature:**

```typescript
width?: number;
```
