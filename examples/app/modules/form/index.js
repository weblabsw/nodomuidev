import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { formex1 } from './ex1.js'
export class MForm extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, formex1];
    template(props) {
        return `
            <div class='--component'>
                <h1>Form 表单</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='form'/>
                </div>
                <!--<h2>属性</h2>
                <h3>UIForm/ui-form</h3>
                <AttrGrid $rows={{gridAttrs}} />

                <h3>UIFormItem/ui-form-item</h3>
                <AttrGrid $rows={{gridColAttrs}} />

                <h2>方法</h2>
                <h3>UIForm/ui-form</h3>
                <MethodGrid $rows={{gridMethods}} />-->
            </div>
        `;
    }
    data() {
        return EXCONFIG;
    }
}