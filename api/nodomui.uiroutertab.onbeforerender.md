<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UIRouterTab](./nodomui.uiroutertab.md) &gt; [onBeforeRender](./nodomui.uiroutertab.onbeforerender.md)

## UIRouterTab.onBeforeRender() method

渲染前事件

**Signature:**

```typescript
onBeforeRender(): void;
```
**Returns:**

void

