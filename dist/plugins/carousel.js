import { Module, Nodom } from "nodom3";
/**
 * 参数说明
 * autoplay:        是否自动切换，无值属性
 * interval:        切换时间，单位为ms，默认500
 * ani-type:        动画效果  'scroll' | 'fade'
 * init-index:      默认展示的索引
 * arrow-type:      箭头的显示方式  'always' | 'hover'
 * show-indicator:  是否展示指示器
 * onChange:        切换时的回调函数 `(oldIdx, newIdx) => void`
 */
/**
 * UICarousel 轮播图
 * @public
 */
export class UICarousel extends Module {
    constructor() {
        super(...arguments);
        /**
         * 当前索引号
         */
        this.__currentIndex = -1;
        /**
         * items 集合
         */
        this.__items = [];
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __count: 0,
            __indicators: [] //索引圆圈
        };
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__interval = props['interval'] ? parseInt(props['interval'] + '') : 500;
        this.__autoplay = props.hasOwnProperty('autoplay');
        this.__animation = props['ani-type'] || 'scroll';
        this.__initIndex = props['init-index'] ? parseInt(props['init-index'] + '') : 0;
        this.__onChange = props['onchange'];
        const showIndicator = props.hasOwnProperty('show-indicator');
        const arrowType = props['arrow-type'] || 'alaways';
        let __indicatorstr = '';
        /**
         * 是否显示指示器
         */
        if (showIndicator) {
            __indicatorstr = `<ul class='ui-carousel-indicator-wrap'>
            <li x-repeat={{__indicators}} index='idx' class={{'ui-carousel-indicator' + (__isCurr ? ' ui-carousel-indicator-active' : '')}} 
              style='transition-duration:${this.__interval}ms;' 
              e-click='__goto' />
          </ul>`;
        }
        this.setExcludeProps(['autoplay', 'ani-type', 'init-index', 'arrow-type', 'show-indicator', 'onchange']);
        return `
      <div class='ui-carousel'
          e-mouseenter='__handleMouseEnter'
          e-mouseleave='__handleMouseLeave'>
          <div class='ui-carousel-wrap'>
            <slot />
          </div>
          ${__indicatorstr}
          <div x-show={{arrowType !== 'never' && __count}} ${arrowType === 'hover' ? ' class="ui-carousel-arrow-hover"' : ''}>
            <div class='ui-carousel-arrow-left' e-click='__prev'>
              <b class='ui-icon-arrow-left'></b>
            </div>
            <div class='ui-carousel-arrow-right' e-click='__next'>
              <b class='ui-icon-arrow-right'></b>
            </div>
          </div>
      </div>
    `;
    }
    /**
     * 播放下一张图片
     * @param next - 下一数据
     */
    __go(next) {
        if (this.__items.length === 0 || this.__currentIndex === next || this.__animating) {
            return;
        }
        this.__moved = true;
        if (this.__onChange) {
            this.invokeOuterMethod(this.__onChange, this.__currentIndex, next);
        }
        //设置初始currentindex和sliding标志
        if (this.__currentIndex === -1) {
            this.__currentIndex = 0;
            this.__animating = false;
        }
        else {
            this.__animating = true;
        }
        if (this.__animation === 'scroll') {
            this.__doScroll(next);
        }
        else {
            this.__doFade(next);
        }
        const c = next % this.__items.length;
        //设置indicator选中状态
        for (let i = 0; i < this.__items.length; i++) {
            this.model['__indicators'][i].__isCurr = i === c;
        }
        //设置当前索引  
        this.__currentIndex = (next + this.__items.length) % this.__items.length;
    }
    /**
     * 执行scroll
     * @param next - 下一数据
     */
    __doScroll(next) {
        //计算位置
        if (next > this.__currentIndex) { //右滑动
            for (let i = this.__currentIndex; i <= next; i++) {
                const loc1 = 'transform:translateX(' + -(this.__currentIndex - i) * 100 + '%)';
                const loc2 = 'transform:translateX(' + -((next - i) * 100) + '%)';
                this.__items[i % this.__items.length].__setLoc(i === next, loc1, loc2);
            }
        }
        else { //左滑动
            for (let i = this.__currentIndex; i >= next; i--) {
                const loc1 = 'transform:translateX(' + -(this.__currentIndex - i) * 100 + '%)';
                const loc2 = 'transform:translateX(' + -((next - i) * 100) + '%)';
                this.__items[(i + this.__items.length) % this.__items.length].__setLoc(i === next, loc1, loc2);
            }
        }
    }
    /**
     * 执行fade
     * @param next - 下一数据
     */
    __doFade(next) {
        const index1 = (this.__currentIndex + this.__items.length) % this.__items.length;
        const index2 = (next + this.__items.length) % this.__items.length;
        //避免最开始的状态 opacity设置为0
        if (index1 !== index2) {
            this.__items[index1].__setLoc(false, 'opacity:1', 'opacity:0');
        }
        this.__items[index2].__setLoc(true, 'opacity:0', 'opacity:1');
    }
    /**
     * 到指定页
     * @param model -模型
     */
    __goto(model) {
        this.__go(model.idx);
    }
    /**
     * 右按钮事件
     */
    __next() {
        this.__go(this.__currentIndex + 1);
    }
    /**
     * 左按钮事件
     */
    __prev() {
        this.__go(this.__currentIndex - 1);
    }
    /**
     * 结束自动播放
     */
    __stop() {
        if (this.__timer) {
            clearInterval(this.__timer);
            delete this.__timer;
        }
    }
    /**
     * 自动播放
     */
    __play() {
        this.__timer = setInterval(() => {
            this.__next();
        }, this.__interval);
    }
    /**
     * 鼠标进入，autoplay时有效
     */
    __handleMouseEnter() {
        if ((this.__autoplay)) {
            this.__stop();
        }
    }
    /**
     * 鼠标离开，autoplay时有效
     */
    __handleMouseLeave() {
        if (this.__autoplay) {
            this.__play();
        }
    }
    /**
     * 添加item
     * @param item - 需被添加的item
     */
    __addItem(item) {
        if (!this.__items.includes(item)) {
            this.__items.push(item);
            this.model['__indicators'].push({ isCurr: false });
        }
        this.model['__count'] = this.__items.length;
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender() {
        if (!this.__moved && this.__items.length > this.__initIndex) {
            this.__go(this.__initIndex);
        }
    }
    /**
     * 挂载后事件
     * @privateRemarks
     */
    onMount() {
        if (this.__autoplay) {
            this.__play();
        }
    }
}
/**
 * UICarouselItem 轮播图个体
 * @public
 */
export class UICarouselItem extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template() {
        const pmodule = this.getParent();
        pmodule.__addItem(this);
        return `
      <div class={{'ui-carousel-item' + (__isCurr?' ui-carousel-item-active':'') + (__isAnimation?' ui-carousel-item-animation':'')}}
        style={{__loc + ';transition-duration:${pmodule.__interval}ms'}} 
        e-transitionend='__animationEnd'>
        <slot/>
      </div>`;
    }
    /**
     * 设置位置
     * @param flag -  是否为当前节点
     * @param loc1 -  初始位置
     * @param loc2 -  结束位置
     */
    __setLoc(flag, loc1, loc2) {
        this.model['__isCurr'] = flag;
        this.model['__isAnimation'] = true;
        this.model['__loc'] = loc1;
        this.__toLoc = loc2;
    }
    /**
     * 动画结束
     * @param model -  模型
     */
    __animationEnd(model) {
        //关闭动画
        model.__isAnimation = false;
        //设置父模块的滑动标识
        this.getParent().__animating = false;
    }
    /**
     * 更新事件
     * @privateRemarks
     */
    onUpdate() {
        // 需要延迟，因为此时尚在渲染流程中，无法添加到渲染队列进行下次渲染
        setTimeout(() => {
            if (this.__toLoc) {
                this.model['__loc'] = this.__toLoc;
                delete this.__toLoc;
            }
        }, 0);
    }
    /**
     * 挂载事件
     * @privateRemarks
     */
    onMount() {
        // 需要延迟，因为此时尚在渲染流程中，无法添加到渲染队列进行下次渲染
        setTimeout(() => {
            if (this.__toLoc) {
                this.model['__loc'] = this.__toLoc;
                delete this.__toLoc;
            }
        }, 0);
    }
}
Nodom.registModule(UICarousel, 'ui-carousel');
Nodom.registModule(UICarouselItem, 'ui-carousel-item');
//# sourceMappingURL=carousel.js.map