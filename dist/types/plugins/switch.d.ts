import { BaseInput } from "./baseinput";
/**
 * 配置项
 * active-color:    激活状态颜色（可选）
 * inactive-color:  非激活状态颜色（可选）
 */
/**
 * UISwitch 开关
 * @public
 */
export declare class UISwitch extends BaseInput {
    /**
     * 激活色
     */
    private __activeColor;
    /**
     * 非激活色
     */
    private __inactiveColor;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 打开/关闭状态切换
     */
    private __genClass;
    /**
     * 自定义颜色
     */
    private __genStyle;
    /**
     * 点击
     */
    private __click;
}
