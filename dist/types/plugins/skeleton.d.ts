import { Module } from "nodom3";
/**
 * 参数说明
 * animated:       是否开启动画
 * loading:        是否处于加载状态，false 时展示子组件
 * avater:         是否显示头像
 * title:          是否显示标题
 * text:           是否显示段落
 * row:            段落数量
 * class
 * style
 */
/**
 * UISkeleton 骨架屏
 * @alpha
 */
export declare class UISkeleton extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
}
/**
 * 参数说明
 * type:  'title' | 'circle' | 'rect'  | 'text' | 'img';
 * class
 * style
 */
/**
 * UISkeletonItem 骨架屏子组件
 * @alpha
 */
export declare class UISkeletonItem extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
}
