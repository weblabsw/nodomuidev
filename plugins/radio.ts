import {Module, ModuleFactory, Nodom} from "nodom3";
import { BaseInput } from "./baseinput";

/**
 * UIRadioGroup 单选框集
 * @public
 */
export class UIRadioGroup extends BaseInput{
    /**
     * 当前model
     */
    private __current;
    /**
     * 子radio数组
     */
    private __radios = [];
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props){
        super.template(props);
        return `
            <div class='ui-radiogroup'>
                <slot />
            </div>
        `;
    }

    /**
     * 设置当前子model
     * @param radio - 子radio
     */
    public __setCurrent(radio){
        if(this.__current){
            this.__current.model.checked = false;
        }
        radio.model.checked = true;
        this.__current = radio;
        this.model['__value'] = radio.value;
    }

    /**
     * 添加子model
     * @param radio -  子radio
     * @returns 
     */
    public __add(radio){
        if(this.__radios.includes(radio)){
            return;
        }
        this.__radios.push(radio);
        if(radio.value == this.model['__value']){
            this.__setCurrent(radio);
        }else{
            radio.model.checked = false;
        }
    }
    /**
     * 初始化数据
     */
    protected __initValue(){
        for(const r of this.__radios){
            if(r.value == this.model['__value'] && r !== this.__current){
                this.__setCurrent(r);
            }
        }
    }
}

/**
 * 配置说明
 * field:           绑定父模块的字段
 */

/**
 * UIRadio 单选框
 * @public
 */
export class UIRadio extends Module{
    /**
     * radio选择时的值
     */
    public value;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props){
        this.value = props.value;
        return `
            <span class={{'ui-radio ' + (checked?'ui-radio-active':'ui-radio-unactive')}} e-click='__click'>
                <b/>
                <span>${props.title}</span>
            </span>    
        `;
    }
    /**
     * 第一次渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender(){
        (<UIRadioGroup>ModuleFactory.get(this.srcDom.slotModuleId)).__add(this);
    }
    /**
     * 点击事件
     * @param model - 模型
     */
    __click(){
        (<UIRadioGroup>ModuleFactory.get(this.srcDom.slotModuleId)).__setCurrent(this);
    }
}
Nodom.registModule(UIRadioGroup,'ui-radiogroup');
Nodom.registModule(UIRadio,'ui-radio');