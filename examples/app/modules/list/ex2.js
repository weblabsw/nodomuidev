import { Module } from '/dist/nodomui.min.js'
export class listex2 extends Module {
    template(props) {
        return `
            <div>
				<div>值：{{hobby1}}</div>
				<ui-list
					$data={{hobbies1}}
					field="hobby1"
					value={{hobby1}}
					value-field="hid"
					type="col"
					multiple
					class='list'
					>
					{{htitle}}
				</ui-list>

                <style>
					.list{
						width:400px;
						height:200px;
					}
				</style>
            </div>
        `;
    }
    data() {
        return {
            hobby1: [3, 4],
            hobbies1: this.genData(),
        }
    }

    genData() {
        return [
            { hid: 1, htitle: "体育", icon: "message", desc: "体育运动很不错" },
            { hid: 2, htitle: "阅读", icon: "search", desc: "阅读让人睿智" },
            { hid: 3, htitle: "健身", icon: "home", disable: true, desc: "健身让人上瘾" },
            { hid: 4, htitle: "电脑游戏", icon: "user", desc: "好玩可别贪玩" },
            { hid: 5, htitle: "户外运动", icon: "password", desc: "感受大自然，轻呼吸" },
            { hid: 6, htitle: "旅游", icon: "prompt", disable: true, desc: "读万卷书，行万里路" },
        ]
    }
}