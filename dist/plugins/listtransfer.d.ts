import { BaseInput } from "./baseinput";
/**
 * $data：          列表数据数组
 * field:           绑定父模块的字段
 * value-field：    值字段名
 * disable-field：  禁用字段名
 */
/**
 * UIListTransfer 穿梭框
 * @public
 */
export declare class UIListTransfer extends BaseInput {
    /**
     * 值field
     */
    private __valueField;
    /**
     * 禁用字段名
     */
    private __disableField;
    /**
     * 值
     */
    private __value;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        __selectedRows: any[];
    };
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender(): void;
    /**
     * 获取左边列表数据
     * @returns
     */
    private __getLeftRows;
    /**
     * 点击item
     * @param model - 点击dom的model
     */
    private __clickItem;
    /**
     * 设置值
     * @param value - 需设置的值
     */
    private __setValue;
    /**
     * 把选中节点传递到右边
     */
    private __rightClick;
    /**
     * 把所有节点传递到右边
     */
    private __rightClick1;
    /**
     * 把选中节点传递到左边
     */
    private __leftClick;
    /**
     * 把所有节点传递到左边
     */
    private __leftClick1;
    /**
     * 添加选中
     * @param m - 选中的model
     */
    private __addSelect;
    /**
     * 移除选中
     * @param m - 选中的model
     */
    private __removeSelect;
    /**
     * 更新值
     * @returns
     */
    private __updateValue;
    /**
     * 获取值
     * @returns 值数组
     */
    private __getSelectedValue;
}
