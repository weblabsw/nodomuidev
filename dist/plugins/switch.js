import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
/**
 * 配置项
 * active-color:    激活状态颜色（可选）
 * inactive-color:  非激活状态颜色（可选）
 */
/**
 * UISwitch 开关
 * @public
 */
export class UISwitch extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        if (props.hasOwnProperty('active-color')) {
            this.__activeColor = props['active-color'];
        }
        if (props.hasOwnProperty('inactive-color')) {
            this.__inactiveColor = props['inactive-color'];
        }
        this.setExcludeProps(['active-color', 'inactive-color']);
        return `
            <div class="ui-switch" e-click='__click'>
                <div class={{__genClass()}} style={{__genStyle()}}>
                    <span class='ui-switch-circle'></span>
                </div>
            </div>
        `;
    }
    /**
     * 打开/关闭状态切换
     */
    __genClass() {
        return 'ui-switch-slider' + (this.model['__value'] ? ' ui-switch-checked' : ' ui-switch-unchecked');
    }
    /**
     * 自定义颜色
     */
    __genStyle() {
        if (this.__activeColor && this.__inactiveColor) {
            return 'background-color:' + (this.model['__value'] ? this.__activeColor : this.__inactiveColor);
        }
    }
    /**
     * 点击
     */
    __click() {
        this.model['__value'] = !this.model['__value'];
        this.__genClass();
    }
}
//注册模块
Nodom.registModule(UISwitch, 'ui-switch');
//# sourceMappingURL=switch.js.map