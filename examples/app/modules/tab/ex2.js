import{Module} from '/dist/nodomui.min.js'
export class tabex2 extends Module{
    template(props){
        return `
            <ui-tab onTabClose='closeTab'>
                <ui-tab-item x-repeat={{tabs}} title={{title}}>
                    <p>{{content}}</p>
                </ui-tab-item>
            </ui-tab>
				`;
    }

    data(){
        return {
            index:5,
            tabs:[
                {title:'tab1',content:'这是tab1'},
                {title:'tab2',content:'这是tab2'},
                {title:'tab3',content:'这是tab3'},
                {title:'tab4',content:'这是tab4'}
            ]
        }
    }
}