export const EXCONFIG = {
    name:'Progress',
    desc: '使用ui-progress(进度条)进行设置。',
    progressAttrs: [{
        name: 'size',
        desc: '进度条尺寸',
        type: 'string',
        values: 'small|normal|large',
        defaults: 'small'
    }, {
        name: 'percent',
        desc: '进度',
        type: 'number',
        values: '-',
        defaults: '0'
    }, {
        name: 'hide-text',
        desc: '文本是否显示',
        type: '无',
        values: '-',
        defaults: '-'
    }, {
        name: 'type',
        desc: '设置进度条类型',
        type: 'string',
        values: 'line|circle',
        defaults: 'line'
    }, {
        name: 'stroke-color',
        desc: '已完成进度条色',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'trail-color',
        desc: '未完成进度条色',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'format',
        desc: '内容模板函数',
        type: 'function',
        values: '-',
        defaults: '((text)=>"{{percent}}%")'
    }],
    examples: [{
        title:'简单应用',
        name: 'ex1'
    }, {
        title:'更改进度条大小',
        desc: '通过size进行设置',
        name: 'ex2'
    }, {
        title: '设置类型',
        desc: '通过type进行设置',
        name: 'ex3'
    }, {
        title: '设置已完成进度和未完成颜色',
        desc: '通过stroke-color、trail-color进行设置',
        name: 'ex4'
    }, {
        title: '自定义进度文本',
        desc: '通过format进行设置',
        name: 'ex5'
    }, {
        title: '隐藏进度文本',
        desc: '通过hide-text进行设置',
        name: 'ex6'
    }]
}