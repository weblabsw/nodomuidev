import { Module } from '/dist/nodomui.min.js'

export class breadcrumbex3 extends Module {
    template(props) {
        return `
                <div>
                    <div style="display: flex;flex-direction: column; height: 80px;justify-content: space-around;">
                        <ui-breadcrumb icon-separator="arrow-right">
                            <ui-breadcrumb-item title="首页" to="/" />
                            <ui-breadcrumb-item>
                                <a href="/path1">路径1</a>
                            </ui-breadcrumb-item>  
                            <ui-breadcrumb-item>
                                <a href="/path2">路径2</a>
                            </ui-breadcrumb-item>
                            <ui-breadcrumb-item>
                                <a href="/path3">路径3</a>
                            </ui-breadcrumb-item>
                        </ui-breadcrumb>
                    </div>
                </div>

            `;
    }
}