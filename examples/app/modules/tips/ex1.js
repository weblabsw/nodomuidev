import { Module, nuitip } from '/dist/nodomui.min.js'
export class tipsex1 extends Module {
    template(props) {
        return `
            <div>
                <ui-button e-click='showtip1' title="上边延迟关闭"></ui-button>
                <ui-tip />
            </div>
        `;
    }
    data() {
        return {
            index: 1
        }
    }

    showtip1(model) {
        nuitip({ text: 'hello top' + model.index++ });
    }
}