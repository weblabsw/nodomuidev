import { Module, Nodom } from "nodom3";

export class UIPopover extends Module {
    /**
     * 弹出位置
     */
    private placement: string;
    template(props?: object): string {
        //弹出位置默认为底部
        this.placement = props["placement"] || "bottom";

        const placementClazz = `data-placement="${this.placement}"`;
        //设置不渲染属性
        this.setExcludeProps(["placement", placementClazz]);

        return `
            <div class="ui-popover">
                <div class="ui-popover-trigger">
                    <slot name="trigger" />
                </div>
                <div class="ui-popover-content" ${placementClazz}>
                    <slot/>
                </div>
            </div>
        `;
    }
}

Nodom.registModule(UIPopover, "ui-popover");
