import { Module } from '/dist/nodomui.min.js'
export class timelineex3 extends Module {
    template(props) {
        return `
        <ui-timeline 
                    $data={{timelist3}} 
                    place-ment
                    >
         </ui-timeline>
        `;
    }
    data() {
        return {
            timelist3: this.genData3()
        }
    }
    genData3() {
        return [
            { ttitle: "更新Github模板", tdate: "2023-06-11 ", ttime: "20:46", tdesc: "张三提交于" },
            { ttitle: "更新Github模板", tdate: "2023-06-13 ", ttime: "20:46", tdesc: "张三提交于" },
            { ttitle: "更新Github模板", tdate: "2023-06-15 ", ttime: "20:46", tdesc: "张三提交于" },

        ]
    }
}