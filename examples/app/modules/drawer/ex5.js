import { Module } from '/dist/nodomui.min.js'
export class drawerex5 extends Module {
    template(props) {
        return `
            <div>
                <ui-button e-click='openLeft' title="从左往右开"></ui-button>
                <ui-drawer title='drawer-left' closable direction='left' size="400px" showconfirm>
                    <ui-drawer-body>
                        left to right
                    </ui-drawer-body>
                    <ui-button e-click='closeLeft' theme='error' title='关闭'/>
                </ui-drawer>
            </div>
        `
    }
    openLeft() {
        this.getModule('UIDrawer').__open();
    }
    closeLeft() {
        this.getModule('UIDrawer').__close();
    }
}