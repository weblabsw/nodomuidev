<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UIRouterTab](./nodomui.uiroutertab.md) &gt; [\_\_activeTab](./nodomui.uiroutertab.__activetab.md)

## UIRouterTab.\_\_activeTab() method

手动active

**Signature:**

```typescript
__activeTab(data: string | number | RouterTabItem): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  data | string \| number \| [RouterTabItem](./nodomui.routertabitem.md) | model或title或索引号 |

**Returns:**

void

