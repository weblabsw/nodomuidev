import{Module} from '/dist/nodomui.min.js'
export class radioex1 extends Module{
    template(){
        return `
            <div>
                <ui-button e-click='change' title="change"></ui-button>
                <div>值：{{edu}}</div>
                <ui-form>
                    <ui-form-item>
                        <ui-radiogroup field='edu'>
                            <ui-radio value='1' title='高中' />
                            <ui-radio value='2' title='本科' />
                            <ui-radio value='3' title='硕士' />
                        </ui-radiogroup>
                    </ui-form-item>
                </ui-form>
            </div>
        `
    }

    data(){
        return {
            edu:1,
        }
    }

    change(){
        this.model.edu=2;

    }
}