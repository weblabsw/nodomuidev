import { Module, Nodom } from "nodom3";

export class UIAvatar extends Module {
    private __shape: "circle" | "square";

    private __src: string;

    private __alt: string;

    private __size: string;

    private __icon: string;

    private __method: "image" | "icon" | "slot";

    template(props?: object): string {
        this.__size = props["size"] || "normal";

        this.__shape = props["shape"] || "circle";
        // 图标字符串
        let btnStr = "";

        if (props["src"]) {
            this.__method = "image";
            this.__src = props["src"];
            this.__alt = props["alt"];
            btnStr = `<img src="${this.__src}" alt="${this.__alt}">`;
        } else if (props["icon"]) {
            this.__method = "icon";
            this.__icon = props["icon"] || "user";
            btnStr = `<b class='ui-icon-${this.__icon} ui-icon-size${this.__size}'/>`;
        } else {
            this.__method = "slot";
        }

        const clazz = `class = "ui-avatar ui-avatar-${this.__size} ui-avatar-${this.__shape}"`;

        //设置不渲染属性
        this.setExcludeProps(["__shape", "__src", "__alt", "__size", "__icon", "__method"]);

        return `
            <span ${clazz}>
                <div x-if={{this.__method == 'image'}}>
                    ${btnStr}
                </div>
                <div x-elseif={{this.__method == 'icon'}}>
                    ${btnStr}
                </div>
                <div v-elseif={{this.__method == 'slot'}}>
                  <slot />
                </div>
            </span>
        `;
    }
}

//注册模块
Nodom.registModule(UIAvatar, "ui-avatar");
