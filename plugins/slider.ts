import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
import { UITool } from './uibase';

/**
 *  配置说明：
 *  field 绑定滑块百分比值，默认为0
 *  max 最大值，默认为100
 *  min 最小值，默认为0
 *  vertical 是否竖向
 *  height 滑动条高度，竖向模式必填
 *  step 滑动步数
 *  show-step 是否显示间断点，必须在设置了step的前提下才生效
 */

/**
 * 拖拽事件
 * @param startPos - 鼠标点击起始位置（clientX,clientY）
 * @param currentPos - 拖拽后鼠标位置（clientX,clientY）
 * @param newPosition - 拖拽后滑块应到达的位置
 * @param startPosition - 滑块起始位置
 * @param highWayLength - 轨道长度
 */
export type initData = {
    /**
     * 鼠标点击起始位置（clientX,clientY）
     */
    startPos: {
        x: number,
        y: number
    }
    /**
     * 拖拽后鼠标位置（clientX,clientY）
     */
    currentPos: {
        x: number,
        y: number
    }
    /**
     * 拖拽后滑块应到达的位置
     */
    newPosition: number;
    /**
     * 滑块起始位置
     */
    startPosition: number;
    /**
     * 轨道长度
     */
    highWayLength: number;
}
/**
 *  UISlider 滑动条
 *  @public
 */
export class UISlider extends BaseInput {
    /**
     * 滑块计算移动参数对象
     */
    private __initData: initData;
    /**
     * 滑动条精确值最大值，可设定，默认100
     */
    private __max: number;

    /**
     * 滑动条精确值最小值，可设定，默认0
     */

    private __min: number;
    /**
     * 是否竖向模式
     */

    private __vertical: boolean;

    /**
     * 如果是竖向模式，则需要设定设定，默认'200px'
     */
    private __height: number;
    /**
     * 是否禁用滑动
     */
    private __disabled: boolean;
    /**
     * 滑动条每次移动间隔
     */
    private __step: number;
    /**
     * 是否有tick,只有设置step值为true
     */
    private __hasTick: boolean;
    /**
     * 是否显示步点
     */
    private __showStep: boolean;

    /**
     *  轨道的element
     */
    private __pel: HTMLElement;

    /**
     * dot的element
     */
    private __el: HTMLElement;

    /**
     *  bar的element
     */
    private __bel: HTMLElement;
    /**
     * tick们的element
     */
    private __ticks: HTMLElement;
    /**
     * 是否拖拽
     */
    private __dragging: boolean;
    /**
    * 是否点击，防止拖拽后点击轨道造成的偏移
    */
    private __isClick: boolean;

    /**
     * 
     * @param props - 传递进来的参数，具体见顶部
     * @returns 模板字符串，根据此进行渲染
     */
    template(props?: object): string {
        super.template(props);
        this.__max = props['max'] || 100;
        this.__min = props['min'] || 0;
        this.__vertical = props.hasOwnProperty('vertical');
        this.__disabled = props.hasOwnProperty('disabled');
        this.__step = props['step'] || 1;
        // 是否显示tick
        this.__showStep = props['show-step'] || false;
        // 根据是否设置了step来判断是否生成tick
        this.__hasTick = props.hasOwnProperty('step') || false;
        // 竖直情况下，获得滑动条高度
        if (this.__vertical) {
            this.__height = props['height'];
        }
        this.model['showStep'] = this.__showStep;
        //设置class
        let className = 'ui-slider-normol ui-slider-' + (this.__vertical ? 'vertical' : 'normal');
        className += ' ' + (this.__disabled ? 'ui-slider-disabled' : '');
        let ticks = '';
        if (this.__hasTick) {
            ticks += `<div class='ui-slider-ticks'>`
            const length = this.__max - this.__min;
            let stepValue = 0;
            for (let i = this.__step; i < length; i += this.__step) {
                stepValue = i / length * 100;
                if (this.__vertical) {
                    ticks += `<div class='ui-slider-tick' style="bottom: ${stepValue}%;"/>`;
                } else {
                    ticks += `<div class='ui-slider-tick'  style="left: ${stepValue}%;"/>`;
                }
            }
            ticks += `</div>`
        }
        this.setExcludeProps(['height', 'max', 'min', 'vertical', 'disabled', 'step', 'show-step'])
        return `
                <div class='${className}' style='${this.__vertical ? 'height:' + this.__height + 'px;' : ''}' e-mousedown='__onSliderDown'>
                    <div class='ui-slider-bar' style={{(this.__vertical ? 'height:' : 'width:') +this.__currentPosition() + "%;" ;}}/>
                    ${ticks}
                    <div class='ui-slider-dot' e-mousedown='__dragStart' style={{(this.__vertical ? 'bottom:' : 'left:')  + this.__currentPosition() + "%;"}}/>
                </div>
        `;
    }
    /**
     * 初始化参数对象
     */
    onInit() {
        this.__initData = {
            startPos: {
                x: 0,
                y: 0
            },
            currentPos: {
                x: 0,
                y: 0
            },
            newPosition: 0,
            startPosition: 0,
            highWayLength: 0
        }

    }
    /**
     * 挂载后钩子函数，只有挂载后才能获取对应ELement以及相关属性，此处完成slider的初始化
     */
    onMount() {
        //获取元素及初始化slider
        this.__pel = <HTMLElement>this.domManager.renderedTree.node;
        this.__bel = <HTMLElement>this.domManager.renderedTree.children[0].node;
        // 是否显示ticks关乎到获取element的次序，也许querySelector是更好的选择。
        if (this.__hasTick) {
            this.__ticks = <HTMLElement>this.domManager.renderedTree.children[1].node;
            this.__el = <HTMLElement>this.domManager.renderedTree.children[2].node;
        } else {
            this.__el = <HTMLElement>this.domManager.renderedTree.children[1].node;
        }
        this.__initSlider();
    }
    /**
     * 渲染前钩子函数，此处设置对showStep属性的监听
     */
    onBeforeRender(): void {
        //监听showStep
        this.watch('showStep', () => {
            this.__updateTicks();
        });
    }
    /**
     * model数据
     * @returns showStep - 设置showStep以便设置监听函数
     */
    data() {
        return {
            showStep: false,
        }
    }
    /**
     * 初始化绑定的field，若不存在则为0
     */
    protected __initValue() {
        if (isNaN(this.model['__value']))
            this.model['__value'] = 0;
    }

    /**
     * 获取初始化slider所需参数
     */
    private __initSlider() {
        // 实际value
        this.__initData.newPosition = this.__currentPosition();
        // 获取轨道长度
        this.__initData.highWayLength = this.__pel[`client${this.__vertical ? 'Height' : 'Width'}`];
        this.__setPosition(this.__initData.newPosition)
    }
    /**
     * 计算目标位置
     * @returns 目标位置百分比
     */
    private __currentPosition() {
        return ((this.model['__value'] - this.__min) / (this.__max - this.__min)) * 100;
    }
    /**
     * 拖拽事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragStart(model, dom, evObj, e) {
        e.preventDefault();
        //获取鼠标点击位置
        this.__initData.startPos = UITool.returnClientXY(e);
        // 获取滑块初始位置
        this.__initData.startPosition = this.__currentPosition();
        this.__initData.newPosition = this.__initData.startPosition;
        this.__dragging = true;
        this.__isClick = true;
        const that = this;

        const onDragging = (event: MouseEvent) => {
            if (this.__disabled) return;
            if (that.__dragging) {
                that.__isClick = false;
                const initData = that.__initData;
                // 拖动过程中鼠标的位置
                initData.currentPos = UITool.returnClientXY(event);
                const { currentPos, startPos } = initData;
                // 计算被拖动的距离
                let diff: number;
                if (this.__vertical) {
                    diff = startPos.y - currentPos.y;
                } else {
                    diff = currentPos.x - startPos.x;
                }

                that.__initData.newPosition = initData.startPosition + ((diff / initData.highWayLength) * 100);
                // 计算出[min,max]范围的value，并移动滑块和滑块条
                that.__setPosition(that.__initData.newPosition);
            }
        }
        // 拖动结束，注销事件
        const onDragEnd = () => {
            if (that.__dragging) {
                that.__dragging = false;
                // 移除事件
                window.removeEventListener('mousemove', onDragging);
                window.removeEventListener('mousedown', onDragEnd);
            }
        }
        // 创建拖拽流程事件
        window.addEventListener('mousemove', onDragging);
        window.addEventListener('mouseup', onDragEnd);
    }
    /**
     * 点击轨道事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __onSliderDown(model, dom, evObj, e) {
        if (this.__disabled) return;
        // 获取点击后位置
        let newL = UITool.getDisplacement(e, this.__pel, this.__vertical);
        newL = (newL / this.__initData.highWayLength) * 100;
        // 点击轨道超出范围则取消
        if (newL < 0 || newL > 100) return;
        this.__setPosition(newL);
    }

    /**
     * 根据范围修改value，并决定每步步长(如设置step)
     * @param newL - 滑块拖拽后位置
     */
    private __setPosition(newL: number) {
        if (newL === null || isNaN(newL)) return;
        // 超出范围修正
        if (newL < 0) newL = 0;
        else if (newL > 100) newL = 100;
        // 实际步长
        const lengthPerStep = 100 / ((this.__max - this.__min) / this.__step);
        // 步数
        const steps = Math.round(newL / lengthPerStep);
        // 实际值
        const value = steps * lengthPerStep * (this.__max - this.__min) * 0.01 + this.__min;
        // 更新value
        this.model['__value'] = parseFloat(value.toPrecision(3));
        //更新ticks
        this.__updateTicks();
    }
    /**
     * 更新tick状态，如果要求显示，则会根据滑块所处位置，更新其之前的tick颜色
     */
    private __updateTicks() {
        if (!this.__hasTick) return;
        const value = this.__currentPosition() / 10;
        const ticks = this.__ticks.childNodes;
        if (!this.__showStep) {
            for (let i = 0; i < ticks.length; i++) {
                (<HTMLElement>ticks[i]).className = 'ui-slider-tick';
            }
        }
        else {
            for (let i = 0; i < ticks.length; i++) {
                if (i < value) {
                    (<HTMLElement>ticks[i]).className = 'ui-slider-tick ui-slider-tick-active';
                } else {
                    (<HTMLElement>ticks[i]).className = 'ui-slider-tick';
                }
            }
        }
    }


}
//注册模块
Nodom.registModule(UISlider, 'ui-slider');
