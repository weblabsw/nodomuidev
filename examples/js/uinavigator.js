import { Module, Router } from '/dist/nodomui.min.js'
export class MUINavgator extends Module {
    template() {
        return `
            <div class='--nav'>
                <for cond={{children}} >
                    <div class='--nav-title1'>{{title}}</div>
                    <a x-repeat={{children}} x-route={{url}} class='--nav-title2'>{{title}}</a>
                </for>
            </div>
        `
    }

    data() {
        return {
            children: [{
                title: '开发指南', children: [
                    { title: '安装', url: '/install' },
                    { title: '国际化', url: '/international' },
                    { title: '主题', url: '/theme' },
                ]
            }, {
                title: '组件', children: [
                    { title: 'Accordion 折叠面板', url: '/accordion' },
                    { title: 'Avatar 头像展示', url: '/avatar' },
                    { title: 'Badge 标记', url: '/badge' },
                    { title: 'Breadcrumb 面包屑', url: '/breadcrumb' },
                    { title: 'Button 按钮', url: '/button' },
                    { title: 'Card 卡片', url: '/card' },
                    { title: 'Carousel 轮播图', url: '/carousel' },
                    { title: 'Checkbox 复选框', url: '/checkbox' },
                    { title: 'Date 日期选择器', url: '/date' },
                    { title: 'Dialog 对话框', url: '/dialog' },
                    { title: 'Drawer 抽屉', url: '/drawer' },
                    { title: 'File 文件上传', url: '/file' },
                    { title: 'Form 表单', url: '/form' },
                    { title: 'Grid 表格', url: '/grid' },
                    { title: 'Icon 图标', url: '/icon' },
                    { title: 'Input 输入框', url: '/input' },
                    { title: 'Layout 布局', url: '/layout' },
                    { title: 'List 列表', url: '/list' },
                    { title: 'ListTransfer 穿梭框', url: '/listtransfer' },
                    { title: 'Loading 加载', url: '/loading' },
                    { title: 'Menu 菜单', url: '/menu' },
                    { title: 'Messagebox 消息框', url: '/messagebox' },
                    { title: 'Pagination 分页', url: '/pagination' },
                    { title: 'Panel 面板', url: '/panel' },
                    { title: 'Popover 气泡卡片', url: '/popover' },
                    { title: 'Progress 进度条', url: '/progress' },
                    { title: 'Radio 单选框', url: '/radio' },
                    { title: 'Rating 评分', url: '/rating' },
                    { title: 'Relationmap 关系图', url: '/relationmap' },
                    { title: 'Select 下拉框', url: '/select' },
                    { title: 'Slider 滑动条', url: '/slider' },
                    { title: 'Steps 步骤条', url: '/steps' },
                    { title: 'Switch 开关', url: '/switch' },
                    { title: 'Tab 标签页', url: '/tab' },
                    { title: 'Timeline 时间轴', url: '/timeline' },
                    { title: 'Tip 信息提示', url: '/tips' },
                    { title: 'Tree 树形控件', url: '/tree' },
                    // {title:'Skeleton 骨架屏',url:'/skeleton'},

                ]

            }

            ]

        }
    }
}