import{Module,nuiloading} from '/dist/nodomui.min.js'
export class loadingex2 extends Module{
    template(){
        return `
            <div>
                <ui-button e-click='show1' title='叠加打开2次(共5s)后自动关闭' />
                <ui-loading color='red'/>
            </div>
        `
    }
    
    show1(){
        //开启1次，3s后自动关闭
        nuiloading().__show(3000);
        //延迟2s开启第2次，3s后自动关闭
        setTimeout(()=>{
            nuiloading().__show(3000);
        },2000);
    }
}