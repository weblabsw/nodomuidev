
import{Module} from '/dist/nodomui.min.js'
export class gridex12 extends Module{
    template(){
        return `
            <div>
                <ui-grid $data={{rows}} style='height:300px;' grid-line='rows' fix-head>
                    <ui-grid-col width='80' title='编号'>{{id}}</ui-grid-col>
                    <ui-grid-col width='200' title='框架'>{{name}}</ui-grid-col>
                    <ui-grid-col width='300' title='发布时间'>{{pubTime}}</ui-grid-col>
                    <ui-grid-col title='描述' >{{desc}}</ui-grid-col>
                </ui-grid>
                <ui-pagination 
                    total={{total}}
                    page-size={{30}}
                    page-no={{1}}
                    ps-array={{[10,30,50,100]}}
                    show-total
                    show-jump
                    show-num={{10}}
                    big-step={{10}}
                    onChange="changePage"
                />
            </div>
        `;
    }
    data(){
        return{
            total:500
        }
    }

    /**
     * pagination 改变页面号和页面大小时执行事件
     * @param pageNo    页面号
     * @param pageSize  页面大小
     */
    changePage(pageNo,pageSize){
        const rows = [];
        //此处可用ajax代替
        for(let i=0;i<pageSize;i++){
            const id = (pageNo-1)*pageSize + i;
            if(id===this.model.total){
                break;
            }
            rows.push({
                id:id,
                name:'nodom' + id,
                pubTime:new Date().toUTCString(),
                desc:'nodom desc' + id
            })
        }
        this.model.rows = rows;
    }
}