import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
import { UIFormItem } from "./form";
/**
 * radio group插件
 */
export class UIRange extends BaseInput {
    template(props) {
        super.template(props);
        this.__type = props.type || 'number';
        let type = this.__type;
        if (type === 'datetime') {
            type = 'datetime-local';
        }
        return `
            <div class='ui-range'>
                <input type='${type}' x-field='data1'/>
                <span> - </span>
                <input type='${type}' x-field='data2'/>
            </div>
        `;
    }
    onBeforeFirstRender() {
        super.onBeforeFirstRender();
        const formItem = this.getParent();
        if (formItem instanceof UIFormItem) {
            formItem.__addValidator({
                validator: 'range'
            });
        }
        this.watch(this.model, ['data1', 'data2'], (m, k, ov, nv) => {
            this.model['__value'] = (k === 'data1' ? [nv, this.model['data2']] : [this.model['data1'], nv]);
        });
    }
    __initValue() {
        const value = this.model['__value'];
        if (value && value.length === 2) {
            this.model['data1'] = value[0];
            this.model['data2'] = value[1];
        }
    }
}
Nodom.registModule(UIRange, 'ui-range');
//# sourceMappingURL=range.js.map