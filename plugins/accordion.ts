import { Module, Nodom, ModuleFactory, Model, RenderedDom } from 'nodom3';
/**
 * 属性配置
 * single: true/false 是否同时只展开一个
 */


/**
 * UIAccordion 折叠面板
 * @public
 */
export class UIAccordion extends Module {
	/**
	 * 单选标志
	 */
	public __single: boolean;

	/**
	 * 子item数组
	 */
	public __items:UIAccordionItem[] = [];
	/**
	 * 模板函数html
	 * @privateRemarks
	 */
	template(props?: object): string {
		this.__single = props.hasOwnProperty('single');
		if(this.__single){
			this.setExcludeProps(['single']);
		}
		/**
		 * 子菜单 type=0表示在当前菜单项下侧，否则表示右侧
		 */
		return `
            <div class='ui-accordion'>
                <slot />
            </div>
        `;
	}

	/**
	 * 添加item
	 * @param item	待加入的accordion item 
	 */
	__addItem(item:UIAccordionItem){
		if(!this.__items.includes(item)){
			this.__items.push(item);
		}
	}
}

/**
 * accordion item
 * 属性配置
 * title:	标题栏内容
 * opened: 	是否展开
 */
/**
 * UIAccordionItem 折叠子组件
 * @public
 */
export class UIAccordionItem extends Module {
	/**
	 * 父accordion
	 */
	private __accordion;
	/**
	 * 模板函数
	 * @privateRemarks
	 */
	template(props?: object): string {
		if(props.hasOwnProperty('opened')){
			this.model['__open'] = true;	
		}
		//设置父accordion
		this.__accordion = ModuleFactory.get(this.srcDom.slotModuleId);
		//添加到父item集合
		this.__accordion.__addItem(this);
		delete props['opened'];
		this.setExcludeProps(['title','opened']);
		return `
            <div class='ui-accordion-item'>
                <div class='ui-accordion-title' e-click='__clickItem'>
                    ${props['title']}
                    <span class={{__open?'ui-expand-icon ui-expand-icon-open':'ui-expand-icon'}}></span>
                </div>
				<div x-animationbox>
					<div class='ui-accordion-content'>
						<slot />
					</div>
				</div>
            </div>
        `;
	}
	/**
	 * 点击事件	展开/折叠
	 * @param model - 对应model
	 */
	private __clickItem(model: Model) {
		if (this.__accordion.__single) {
			for (const m of this.__accordion.__items) {
				if (m !== this) {
					if(m.model['__open']){
						m.model['__open'] = false;
					}
				} else {
					m.model['__open'] = true;
				}
			}
		} else {
			model['__open'] = !model['__open'];
		}
	}
}

Nodom.registModule(UIAccordion, 'ui-accordion');
Nodom.registModule(UIAccordionItem, 'ui-accordion-item');