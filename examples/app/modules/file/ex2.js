import { Module } from '/dist/nodomui.min.js'
export class fileex2 extends Module {
    template(props) {
        return `
        <ui-file width='100px' height='100px'
			field='files'
			value-field="id"
			display-field="url"
			upload-name="file"
			file-type='image'
			url-field='url'
			delete-url="http://localhost:3000/delete"
			upload-url="http://localhost:3000/upload"
			multiple='true'
			max-count='3' />
        `;
    }
    data() {
        return {
            files: [
                { id: "2", fileName: "file1", url: "http://localhost:3000/upload/tmp/b18eadf0-b41a-11ec-9c94-4169a31adf6d.jpg" },
                { id: "3", fileName: "file2", url: "http://localhost:3000/upload/tmp/b18eadf0-b41a-11ec-9c94-4169a31adf6d.jpg" }
            ]
        }
    }
}