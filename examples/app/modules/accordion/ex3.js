import { Module } from '/dist/nodomui.min.js'
export class accordionex3 extends Module {
    template(props) {
        return `
            <ui-accordion >
                <for cond={{levels}}>
                    <ui-accordion-item title={{title}} >
                        <ol style='margin-block-start:0px'>
                            <li x-repeat={{rows}}>
                                <b class={{'ui-icon-' + icon}}></b>
                                {{name}}
                            </li>
                        </ol>
                    </ui-accordion-item>
                </for>
            </ui-accordion>
        `;
    }
    data() {
        return {
            levels: [
                {
                    title: '商品管理',
                    icon: 'insurance',
                    rows: [
                        { name: '商品上架', icon: 'prompt' },
                        { name: '商品下架', icon: 'prompt' },
                        { name: '商品编辑', icon: 'prompt' },
                        { name: '商品统计', icon: 'prompt' },
                    ],
                },
                {
                    title: '商家管理',
                    icon: 'edit',
                    rows: [
                        { name: '商家审核', icon: 'prompt' },
                        { name: '商家关店', icon: 'prompt' },
                        { name: '临时关闭', icon: 'prompt' },
                        { name: '商家统计', icon: 'prompt' },
                    ],
                },
                {
                    title: '订单管理',
                    icon: 'shopping-cart',
                    rows: [
                        { name: '发货', icon: 'prompt' },
                        { name: '取消申请', icon: 'prompt' },
                        { name: '删除订单', icon: 'prompt' },
                    ],
                },
                {
                    title: '安全管理',
                    icon: 'user',
                    rows: [
                        { name: '角色管理', icon: 'prompt' },
                        { name: '用户管理', icon: 'user' },
                        { name: '资源管理', icon: 'prompt' },
                        { name: '权限管理', icon: 'prompt' },
                    ],
                },
            ],
        };
    }
}