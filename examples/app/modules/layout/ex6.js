import{Module,nuiloading} from '/dist/nodomui.min.js'
export class layoutex6 extends Module{
    template(){
        return `
            <div>
                <ui-layout vertical class='layout'>
                    <ui-layout-top class='header'>顶部</ui-layout-top>
                    <ui-layout>
                        <ui-layout-left class='left' sizable>左侧</ui-layout-left>
                        <ui-layout-center class='center'>中心</ui-layout-center>
                        <ui-layout-right class='right' sizable>右侧</ui-layout-right>
                    </ui-layout>
                    <ui-layout-bottom class='footer'>底部</ui-layout-bottom>
                </ui-layout>
            </div>
        `;
    }
}