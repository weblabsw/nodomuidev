import { Module } from "nodom3";
/**
 * 标记插件
 * 配置参数
 * value:       标记内容
 * max:         展示max+字样
 * dot:         显示小红点
 * active:      右上角标记带跳动效果
 * type:        标记背景颜色
 * icon         自定义标记图标，值参考图教程
 * color        自定义标记背景颜色
 */
/**
 * UIBadge 标记
 * @beta
 */
export declare class UIBadge extends Module {
    /**
     * 标记图标
     */
    private __icon;
    /**
     * 标记内容
     */
    private __value;
    /**
模板函数
     * @privateRemarks
     */
    template(props?: object): string;
}
