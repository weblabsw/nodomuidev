import { Module } from '/dist/nodomui.min.js'
export class drawerex2 extends Module {
    template(props) {
        return `
            <div>
                <ui-button e-click='openLeft' title="从左往右开"></ui-button>&nbsp;
                <ui-button e-click='openRight' title="从右往左开"></ui-button>&nbsp;
                <ui-button e-click='openTop' title="从上往下开"></ui-button>&nbsp;
                <ui-button e-click='openBottom' title="从下往上开"></ui-button>
                <ui-drawer title='drawer-left' sizable closable direction='left' size="400px">
                    <ui-drawer-body>
                        left to right
                    </ui-drawer-body>
                    <ui-button e-click='closeLeft' theme='error' title='关闭'/>
                </ui-drawer>
                <ui-drawer title='drawer-right' closable direction='right' size="400px">
                    <ui-drawer-body>
                        right to left
                    </ui-drawer-body>
                    <ui-button e-click='closeRight' theme='error' title='关闭'/>
                </ui-drawer>
                <ui-drawer title='drawer-top' closable direction='top' size="400px">
                    <ui-drawer-body>
                        top to bottom
                    </ui-drawer-body>
                    <ui-button e-click='closeTop' theme='error' title='关闭'/>
                </ui-drawer>
                <ui-drawer title='drawer-bottom' closable direction='bottom' size="400px">
                    <ui-drawer-body>
                        bottom to top 
                    </ui-drawer-body>
                    <ui-button e-click='closeBottom' theme='error' title='关闭'/>
                </ui-drawer>
            </div>
        `
    }
    openLeft() {
        this.getModule('UIDrawer', false, { direction: 'left' }).__open();
    }
    openRight() {
        this.getModule('UIDrawer', false, { direction: 'right' }).__open();
    }
    openTop() {
        this.model.mydirection = "top";
        this.getModule('UIDrawer', false, { direction: 'top' }).__open();
    }
    openBottom() {
        this.model.mydirection = "bottom";
        this.getModule('UIDrawer', false, { direction: 'bottom' }).__open();
    }

    closeLeft() {
        this.getModule('UIDrawer', false, { direction: 'left' }).__close();
    }

    closeRight() {
        this.getModule('UIDrawer', false, { direction: 'right' }).__close();
    }

    closeTop() {
        this.getModule('UIDrawer', false, { direction: 'top' }).__close();
    }

    closeBottom() {
        this.getModule('UIDrawer', false, { direction: 'bottom' }).__close();
    }
}