import { Module } from "nodom3";
/**
 * 参数说明
 * total       total值
 * ps-array：   页面大小数组，数组字符串，默认[10,20,30,50]
 * show-total:  显示总记录数
 * show-jump:   显示跳转
 * show-num:    显示页面号的数量，默认10
 * big-step:    一大步移动页数，默认5
 * onChange:    页号或页面大小改变时执行方法名
 */
/**
 * UIPagination 分页
 * @public
 */
export declare class UIPagination extends Module {
    /**
     * 页号修改后的钩子方法名，来源于父模块
     */
    private __onChange;
    /**
     * 一大步的步数
     */
    private __bigStep;
    /**
     * 显示页面号数
     * 默认10
     */
    private __showNum;
    /**
     * 监听标志
     */
    private __watched;
    /**
     * 页面大小选择列表数据
     */
    private __pageSizeArray;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        pageNo: number;
        pageSize: number;
        total: number;
        pages: any[];
    };
    /**
     * 页号修改钩子
     */
    private __changePage;
    private __watch;
    /**
     * 设置页面
     * @param page - 页面号
     */
    private __setPage;
    /**
     * 点击页
     * @param model - 当前节点model
     */
    private __clickPage;
    /**
     * 页号减1
     */
    private __reduceOne;
    /**
     * 页号加1
     */
    private __addOne;
    /**
     * 页号减bigStep
     */
    private __reduceMore;
    /**
     * 页号加bigStep
     */
    private __addMore;
    /**
     * 计算最大最小页号
     */
    private __cacPages;
}
