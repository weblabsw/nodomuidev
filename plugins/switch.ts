
import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";

/**
 * 配置项
 * active-color:    激活状态颜色（可选）
 * inactive-color:  非激活状态颜色（可选）
 * values:          值数组，第一个元素为关闭时的值，第二个元素为打开时的值，默认为[false,true]
 */


/**
 * UISwitch 开关
 * @public
 */
export class UISwitch extends BaseInput {
    /**
     * 激活色
     */
    private __activeColor: string;
    /**
     * 非激活色
     */
    private __inactiveColor: string;

    /**
     * 值数组，默认为[false,true]，设置时，需设置为2个元素的数组
     */
    private __values:any[];
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        if (props.hasOwnProperty('active-color')) {
            this.__activeColor = props['active-color'];
        }

        if (props.hasOwnProperty('inactive-color')) {
            this.__inactiveColor = props['inactive-color'];
        }
        if(props.values && Array.isArray(props.values) && props.values.length === 2){
            this.__values = props.values;
        }else{
            this.__values = [false,true];
        }
        this.setExcludeProps(['active-color', 'inactive-color']);
        return `
            <div class="ui-switch" e-click='__click'>
                <div class={{__genClass()}} style={{__genStyle()}}>
                    <span class='ui-switch-circle'></span>
                </div>
            </div>
        `;
    }

    /**
     * 打开/关闭状态切换
     */
    private __genClass() {
        return 'ui-switch-slider' + (this.model['__value']===this.__values[1] ? ' ui-switch-checked' : ' ui-switch-unchecked');
    }

    /**
     * 自定义颜色
     */
    private __genStyle() {
        if (this.__activeColor && this.__inactiveColor) {
            return 'background-color:' + (this.model['__value']===this.__values[1] ? this.__activeColor : this.__inactiveColor);
        }
    }

    /**
     * 点击
     */
    private __click() {
        this.model['__value'] = this.model['__value']===this.__values[1]?this.__values[0]:this.__values[1];
        this.__genClass();
    }
}
//注册模块
Nodom.registModule(UISwitch, 'ui-switch');