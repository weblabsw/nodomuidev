import { Expression, Module } from "nodom3";
import { UITool } from "./uibase";

/**
 * BaseInput 绑定数据型父类组件
 * @public
 */
export class BaseInput extends Module {
    /**
     * 对应字段
     */
    protected __field: string;
    
    /**
     * change事件方法名
     */
    private __onChange: string;
    
    /**
     * 启用watch标志
     */
    private __watched:boolean;

    /**
     * 重构
     */
    constructor() {
        super();
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string {
        if (props['field']) {
            this.__field = props['field'];
            this.__onChange = props['onchange'];
            if(!this.__watched){
                this.__watched = true;
                // 在父dom添加__value属性，父修改时，可驱动子模块渲染
                this.srcDom.vdom.setProp('__value', new Expression(this.__field));
                //添加监听
                this.watch('__value', (m, key, ov, nv) => {
                    this.__change(ov, nv);
                });
            }else{  //第二次则直接赋值
                this.model['__value'] = this.props['__value']
            }
        }
        this.setExcludeProps(['__value']);
        return null;
    }

    onBeforeFirstRender(){
        //第一渲染前获取srcDom的属性值，后续则直接从“__value”获取
        if(this.__field){
            this.model['__value'] = this.get(this.srcDom.model,this.__field);
        }
    }
    /**
     * 更改值
     * 需要对父模块对应数据项进行更改
     * @param oldValue - 旧值
     * @param newValue - 新值
     */
    public __change(oldValue: unknown, newValue: unknown) {
        if (!this.__field) {
            return;
        }
        //修改props值，避免二次渲染
        this.props['__value'] = newValue;
        //调用change事件
        if (this.__onChange) {
            UITool.invokePropMethod(this,this.__onChange, oldValue, newValue);
        }
        //更改父模块对应数据项
        this.set(this.srcDom.model, this.__field, newValue);
        //初始化显示
        this.__initValue();
    }

    /**
     * 初始化value，当模块依赖值进行初始化时有用
     */
    protected __initValue() {}

    /**
     * 获取值
     * @returns - 模块值 
     */
    public __getValue() {
        return this.model['__value'];
    }
}