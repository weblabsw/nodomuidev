import { Module, ModuleFactory, Nodom } from "nodom3";
/**
 * 配置项：
 *  radius  圈的半径
 *  color   圆点颜色
 */
/**
 * UILoading 加载
 *  @public
 */
class UILoading extends Module {
    constructor() {
        super(...arguments);
        /**
         * 显示数，每open一次，+1，每close一次，-1。close时检查是否为0，为0则关闭
         */
        this.__openCount = 0;
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__color = props.color;
        //圈大小，默认半径40
        this.__radius = props.radius ? parseInt(props.radius) : 40;
        const width = this.__radius * 2;
        this.setExcludeProps(['radius', 'color']);
        return `
            <div class={{"ui-loading" + (__open?'':' ui-loading-hide')}}>
                <div class={{'ui-mask' + (__open?'':' ui-mask-hide')}} />
                <if cond={{__url}}>
                    <img src={{__url}} />
                </if>
                <else>
                    <canvas role='ui-loading-canvas' width='${width}' height='${width}'
                    style='width:${width}px;height:${width}px' />
                </else>
            </div>
        `;
    }
    /**
     * 打开loading
     * @param limit - 停留时间/动画url地址，如果为0或不填，则表示不自动关闭，如果为字符串，则为动画url地址
     * @param imgUrl - 动画url地址
     */
    __show(limit, imgUrl) {
        const me = this;
        const rootEl = this.getElement(1);
        if (!rootEl) {
            return;
        }
        const canvas = rootEl.querySelector('canvas');
        this.model['__open'] = true;
        let url;
        //设置了自动关闭，则到时关闭
        if (limit) {
            const tp = typeof limit;
            if (tp === 'number') {
                setTimeout(() => {
                    this.__close();
                }, limit);
            }
            else if (tp === 'string') {
                url = limit;
            }
        }
        if (imgUrl) {
            url = imgUrl;
        }
        //已经开启了loading，则不再加载
        if (this.__openCount++ !== 0) {
            return;
        }
        //存在url，则使用自定义动画，不绘制canvas
        if (url) {
            this.model['__url'] = url;
            return;
        }
        const width = canvas.width;
        const circleCount = 6;
        const ctx = canvas.getContext('2d');
        const centerx = me.__radius;
        const centery = me.__radius;
        loop();
        /**
         * 循环绘制
         */
        function loop() {
            if (!me.model['__open']) {
                return;
            }
            const radius1 = 6;
            const radius = me.__radius - radius1;
            let angle = Math.PI / 2;
            const circleArr = [];
            loop1();
            setTimeout(loop, 1500);
            /**
             * 单次绘制
             */
            function loop1() {
                if (!me.model['__open']) {
                    return;
                }
                ctx.clearRect(0, 0, width, width);
                ctx.fillStyle = me.__color;
                if (circleArr.length < circleCount) {
                    circleArr.push(true);
                }
                let overNum = 0;
                for (let i = 0; i < circleArr.length; i++) {
                    let a = angle - i * Math.PI / circleCount;
                    if (a > Math.PI * 5 / 2) {
                        overNum++;
                        a = Math.PI * 5 / 2;
                    }
                    //绘制路径                
                    ctx.beginPath();
                    ctx.arc(centerx - radius * Math.cos(a), centery - radius * Math.sin(a), radius1 - i, 0, 360);
                    ctx.closePath();
                    ctx.fill();
                }
                angle += Math.PI / circleCount;
                if (overNum < circleCount) {
                    setTimeout(loop1, 60);
                }
            }
        }
    }
    /**
     * 关闭loading
     * @param force - 是否强制完全关闭，如果为true，则不计算openCount，直接关闭
     */
    __close(force) {
        if (force) { //强制关闭
            this.model['__open'] = false;
            this.__openCount = 0;
            delete this.model['__url'];
        }
        else if (--this.__openCount === 0) {
            this.model['__open'] = false;
            delete this.model['__url'];
        }
    }
}
//注册插件
Nodom.registModule(UILoading, 'ui-loading');
/**
 * 获取加载框类
 * @public
 */
export function nuiloading() {
    return ModuleFactory.getMain().getModule('uiloading');
}
//# sourceMappingURL=loading.js.map