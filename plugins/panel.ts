import { Nodom,Module, ModuleFactory } from 'nodom3';
import { UIButton } from './button';


/**
 * 参数说明
 * title:   标题
 * buttons: 按钮，以','分割，按钮事件以'|'分割,如：minus|clickMinus,close|clickClose
 */

/**
 * UIPanel 面板
 * @public
 */
export class UIPanel extends Module{
    /**
     * @example
     * 事件对象
     * key:按钮名
     * value:事件名
     */
    private eventMap:object = {};

    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?:object):string{
        let iconStr = '';
        let style = '';
        if(props['bgcolor']){
            style += 'background-color:' + props['bgcolor'] + ';';
        }
        if(props['color']){
            style += 'color:' + props['color'];
        }
        if(props['buttons']){
            iconStr = "<div class='ui-panel-header-bar'>";
            const arr = props['buttons'].split(',');
            for(const icon of arr){
                const a = icon.split('|');
                if(a.length === 1){
                    iconStr += `<ui-button icon='${a[0]}' nobg/>`;
                }else if(a.length===2){
                    iconStr += `<ui-button icon='${a[0]}' nobg e-click='clickButton'/>`;
                    this.eventMap[a[0]] = a[1];
                }
            }
            iconStr += "</div>";
        }
        this.setExcludeProps(['title','buttons','bgcolor','color']);
        return  `
            <div class='ui-panel'>
                <div class='ui-panel-header' style='${style}'>
                    <span class='ui-panel-title'>${props['title']}</span>
                    ${iconStr}
                </div>
                <div class='ui-panel-bodyct'>
                    <slot />
                </div>
            </div>
        `;
    }
    /**
     * 点击按钮
     * @param model - 对应model
     * @param dom - Virtual dom
     */
    clickButton(model,dom){
        for(const p in this.eventMap){
            if(dom.props['class'].indexOf('ui-icon-' + p) !== -1){
                const pm = ModuleFactory.get(this.srcDom.slotModuleId);
                pm.invokeMethod(this.eventMap[p],pm.model);
                break;
            }
        }
    }
}

/**
 * UIPanelBody 面板主体
 * @public
 */
export class UIPanelBody extends Module{
    /**
     * 模板函数
     * @privateRemarks
     */
    template(){
        return `
            <div class='ui-panel-body'>
                <slot />
            </div>
        `;
    }
}

/**
 * UIToolbar 面板工具栏
 * @public
 */
export class UIToolbar extends Module{
    /**
     * 子按钮
     */
    modules=[UIButton];
    /**
     * 模板函数
     * @privateRemarks
     */
    template(){
        return `
            <div class='ui-toolbar'>
                <slot />
            </div>
        `;
    }
}

/**
 * UIButtonGroup 面板按钮集
 * @public
 */
export class UIButtonGroup extends Module{
    /**
     * 子按钮
     */
    modules=[UIButton];
    /**
     * 模板函数
     * @privateRemarks
     */
    template(){
        return `
            <div class='ui-button-group'>
                <slot/>
            </div>
        `;
    }
}
//注册模块
Nodom.registModule(UIPanel,'ui-panel');
Nodom.registModule(UIPanelBody,'ui-panel-body');
Nodom.registModule(UIToolbar,'ui-toolbar');
Nodom.registModule(UIButtonGroup,'ui-button-group');
