<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UIBadge](./nodomui.uibadge.md)

## UIBadge class

> This API is provided as a preview for developers and may change based on feedback that we receive. Do not use this API in a production environment.
> 

UIBadge 标记

**Signature:**

```typescript
export declare class UIBadge extends Module 
```
**Extends:** Module

## Methods

|  Method | Modifiers | Description |
|  --- | --- | --- |
|  [template(props)](./nodomui.uibadge.template.md) |  | **_(BETA)_** 模板函数 |

