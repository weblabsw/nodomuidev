import{Module} from '/dist/nodomui.min.js'
export class tabex7 extends Module{
    template(props){
        return `
            <ui-tab onTabClose='closeTab'>
                <ui-tab-item x-repeat={{tabs}} title={{title}} active={{active}} closable={{closable}}>
                    <p>{{content}}</p>
                </ui-tab-item>
            </ui-tab>
				`
    }

    data(){
        return {
            index:5,
            tabs:[
                {title:'tab1',content:'这是tab1'},
                {title:'tab2',content:'这是tab2',closable:true},
                {title:'tab3',content:'这是tab3',closable:true},
                {title:'tab4',content:'这是tab4',closable:true}
            ]
        }
    }
    closeTab(model){
        console.log(model);
        alert("确认关闭？")
    }
}