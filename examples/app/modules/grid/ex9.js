
import{Module} from '/dist/nodomui.min.js'
export class gridex9 extends Module{
    template(){
        return `
            <div>
                <ui-grid $data={{rows}} grid-line='rows' fix-head onRowExpand='rowExpand'>
                    <ui-grid-col width='80' title='编号'>{{id}}</ui-grid-col>
                    <ui-grid-col width='200' title='框架'>{{name}}</ui-grid-col>
                    <ui-grid-col width='150' title='发布时间'>{{pubTime}}</ui-grid-col>
                    <ui-grid-col title='描述' >{{desc}}</ui-grid-col>
                    <ui-grid-expand>
                        <div><label>编号:</label><span>{{id}}</span></div>
                        <div><label>名称:</label><span>{{name}}</span></div>
                        <div><label>发布时间:</label><span>{{pubTime}}</span></div>
                        <div><label>描述:</label><span>{{desc}}</span></div>
                        <div><label>作者:</label><span>{{detail.authors}}</span></div>
                        <div><label>机构:</label><span>{{detail.company}}</span></div>
                    </ui-grid-expand>
                </ui-grid>
                <style>
                    label{
                        width:100px;
                        display:inline-block;
                    }
                </style>
            </div>
        `;
    }
    data(){
        return{
            rows:[
                {id:1,name:'nodom',pubTime:'2017年11月',desc:'Web技术实验室发布的web前端框架'},
                {id:2,name:'noomi',pubTime:'2019年10月',desc:'Web技术实验室发布的node.js服务端框架'},
                {id:3,name:'relaen',pubTime:'2020年10月',desc:'Web技术实验室发布的node.js ORM框架'},
                {id:4,name:'react',pubTime:'2013年5月',desc:'Facebook开源发布的web前端框架'},
                {id:5,name:'vue',pubTime:'2013年2月',desc:'国内优秀web前端框架'},
                {id:6,name:'angular',pubTime:'2010年2月',desc:'Goole发布的web前端框架'}
            ]
        }
    }
    rowExpand(model){
        //请求数据
        if(!model.detail){
            //模拟异步请求
            setTimeout(()=>{
                model.detail = {
                    authors:'nodom groups',
                    company:'web技术实验室'
                }
            },300)
        }
    }
    
}