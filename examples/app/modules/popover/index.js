import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'

import { EXCONFIG } from './config.js'
import { popoverex1 } from './ex1.js'
import { popoverex2 } from './ex2.js'

export class MPopover extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, popoverex1, popoverex2];

    template() {
        return `
            <div class='--component'>
                <h1>Popover 气泡卡片</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='popover'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{popoverAttrs}} />
            </div>`
    }

    data() {
        return EXCONFIG;
    }
}