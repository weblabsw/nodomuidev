export const EXCONFIG = {
    desc: '通过使用ui-tip组件，然后设置nuitip方法进行使用。',
    tipMethods: [{
        name:'nuitip',
        desc:'设定tips',
        params: 'position(弹出位置),text(内容),closable(是否显示关闭按钮),iocn(图标),theme(主题),exclusive(是否排它),width(宽度)'
    },{
        name: '__close',
        desc: '关闭消息提示',
        params: 'model(改变状态的model)',
        returns: '-'
    }, {
        name: '__removeHide',
        desc: '移除hide item',
        params: '-',
        returns: '-'
    }],
    examples: [{
        title: '简单应用',
        imp:'ui-tip组件需设置在根Module',
        name: 'ex1'
    }, {
        title: '在不同位置显示',
        desc: '使用position属性，使消息提示在不同位置显示',
        name: 'ex2'
    }, {
        title: '关闭按钮的显示',
        imp:'当不设置为延时关闭',
        desc: '通过closable进行设定',
        name: 'ex3'
    }, {
        title: '不同icon的显示',
        desc: '通过icon进行设置(参考ui-icon)',
        name: 'ex4'
    }, {
        title: '不同theme的显示',
        desc: '通过theme进行设置',
        name: 'ex5'
    }, {
        title: '设置排他的效果',
        desc: '通过exclusive属性进行设置',
        name: 'ex6'
    }]
}