import{Module} from '/dist/nodomui.min.js'
export class badgeex1 extends Module{
    template(props){
        return `
            <div>
                <p>主题色</p>
                <ui-badge  value="3" type="error" >
                    <ui-button size="small"  title='评论' />
                </ui-badge>
                <ui-badge  value="11" type="success">
                    <ui-button size="small"  title='捐赠' />
                </ui-badge>
                <ui-badge  value="14" type="active">
                    <ui-button size="small"  title='回复' />
                </ui-badge>
                <ui-badge  value="11"  type="warn">
                    <ui-button size="small"  title='点赞' />
                </ui-badge>
                <p>自定义颜色</p>
                <ui-badge  value="11" color="#000000">
                    <ui-button size="small"  title='点赞' />
                </ui-badge>
                <ui-badge  value="11" color="#972761">
                    <ui-button size="small"  title='点赞' />
                </ui-badge>
                <p>自定义内容</p>
                <ui-badge  value="new">
                    <ui-button size="small"  title='特卖' />
                </ui-badge>
                <ui-badge  active value="hot">
                    <ui-button size="small"  title='捡漏' />
                </ui-badge>

                <style>
                    .ui-badge{
                        margin-right:40px;
                        margin-bottom:10px;
                    }
                </style>
            </div>
        `;
    }
}