import{Module} from '/dist/nodomui.min.js'

export class menuex2 extends Module{
    template(props) {
        return`
         <ui-menu $data={{rows}} vertical style='width:200px;padding:5px'>
            <span class={{'ui-icon-' + icon}}>{{title}}</span>
        </ui-menu>
        `;
    }
    data(){
        return{
            rows: this.genMenuData()
        }
    }
    genMenuData(){
        return [
            {
                title: "商家管理",
                icon: "user",
                children: [
                    {
                        title: "商家审核",
                        icon: "search",
                    },
                    {
                        title: "商家关店",
                        icon: "search",
                    },
                    {
                        title: "临时关闭",
                        icon: "search",
                    },
                    {
                        title: "商家统计",
                        icon:"cart",
                        children: [
                            {
                                title: "按名称",
                                icon: "add",
                            },
                            {
                                title: "按区域",
                                icon: "close",
                            },
                            {
                                title: "按服务类型",
                                icon: "ashbin",
                            },
                        ],
                    }
                ]
            },
            {
                title: "订单管理",
                icon: "cart",
                children: [
                    {
                        title: "发货",
                        icon: "add",
                    },
                    {
                        title: "取消申请",
                        icon: "close",
                        __active:true
                    },
                    {
                        title: "删除订单",
                        icon: "ashbin",
                    },
                ],
            },
            {
                title: "商品管理",
                icon: "cart",
                children: [
                    {
                        title: "商品上架",
                        icon: "add",
                    },
                    {
                        title: "商品下架",
                        icon: "minus",
                    },
                    {
                        title: "商品审核",
                    },
                    {
                        title: "商品统计",
                        children: [
                            {
                                title: "上架统计",
                            },
                            {
                                title: "销售统计",
                                children: [
                                    {
                                        title: "日统计",
                                        children: [
                                            {
                                                title: "时统计",
                                            },
                                            {
                                                title: "分统计",
                                            },
                                            {
                                                title: "秒统计",
                                            },
                                        ],
                                    },
                                    {
                                        title: "周统计",
                                    },
                                    {
                                        title: "月统计",
                                    },
                                    {
                                        title: "年统计",
                                    },
                                ],
                            },
                            {
                                title: "下架统计",
                            }
                        ],
                    },
                ],
            },
            {
                title: "安全管理",
                icon: "insurance",
                children: [
                    {
                        title: "角色管理",
                        icon: "insurance",
                    },
                    {
                        title: "用户管理",
                        icon: "user",
                    },
                    {
                        title: "资源管理",
                        icon: "insurance",
                    },
                    {
                        title: "权限管理",
                        icon: "security",
                    },
                ],
            },{
                title:'个人中心',
                icon:'user'
            }]
    }
}