import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { radioex1 } from './ex1.js'
import { radioex2 } from './ex2.js'


export class MRadio extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, radioex1, radioex2];
    template(props) {
        return `
            <div class='--component'>
                <h1>Radio 单选框</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='radio'/>
                </div>
        
                <h2>属性</h2>
                <h3>UIRadioGroup/ui-radiogroup</h3>
                <AttrGrid $rows={{radioGroupAttrs}} />

                <h3>UIRadio/ui-radio</h3>
                <AttrGrid $rows={{radioAttrs}} />

            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}