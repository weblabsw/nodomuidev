
import { Module } from '/dist/nodomui.min.js'
export class sliderex3 extends Module {

    template(props) {
        return `
            <div>
                <span>拖动范围:[40,80]：{{outValue}}</span>
                <ui-slider  field='outValue'  style="width:200px;left:100px" min={{min}} max={{max}} />
                
                <span>拖动范围：[-0.5,0.5]：{{outValue1}}</span>
                <ui-slider  field='outValue1'  style="width:200px;left:100px" min={{min1}} max={{max1}} step={{0.1}} show-step={{true}}/>
            </div>
        `;
    }
    data() {
        return {
            outValue: 50,
            min: 40,
            max: 80,
            outValue1: 0,
            min1: -0.5,
            max1: 0.5,
        }
    }

}