<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UICarousel](./nodomui.uicarousel.md) &gt; [\_\_currentIndex](./nodomui.uicarousel.__currentindex.md)

## UICarousel.\_\_currentIndex property

当前索引号

**Signature:**

```typescript
__currentIndex: number;
```
