<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UIDrawer](./nodomui.uidrawer.md) &gt; [\_\_open](./nodomui.uidrawer.__open.md)

## UIDrawer.\_\_open() method

> This API is provided as a preview for developers and may change based on feedback that we receive. Do not use this API in a production environment.
> 

打开drawer

**Signature:**

```typescript
__open(): void;
```
**Returns:**

void

