<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UICarousel](./nodomui.uicarousel.md) &gt; [template](./nodomui.uicarousel.template.md)

## UICarousel.template() method

模板函数

**Signature:**

```typescript
template(props?: object): string;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  props | object | _(Optional)_ |

**Returns:**

string

