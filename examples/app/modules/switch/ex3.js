import { Module } from '/dist/nodomui.min.js'
export class switchex3 extends Module {
    template(props) {
        return `
        <div>开关值：{{status}}</div>
        <ui-switch field="status" values={{[0,1]}}/>
        `;
    }
    data() {
        return {
            status: 0
        }
    }
}