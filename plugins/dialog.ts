import { Nodom,Module} from 'nodom3';
import { UIPanelBody } from './panel';
import { UITool } from './uibase';

/**
 * dialog插件
 * 参数说明
 * title:           标题
 * closable:        是否显示close按钮
 * height:          高度
 * width:           宽度
 * onOpen:          dialog打开后事件方法名
 * onClose:         dialog关闭后事件方法名
 * onBeforeOpen:    dialog打开前事件名，如果对应方法返回true，则不打开
 * onBeforeClose:   dialog关闭前事件名，如果对应方法返回true，则不关闭
 */

/**
 * Dialog 对话框
 * @public
 */
export class UIDialog extends Module{
    /**
     * 打开后事件方法名
     */
    private __onOpen:string;

    /**
     * 关闭后事件方法名
     */
    private __onClose:string;

    /**
     * 打开前事件方法名
     */
    private __onBeforeOpen:string;

    /**
     * 关闭前事件方法名
     */
    private __onBeforeClose:string;
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props?:object):string{
        this.__onOpen = props['onopen'];
        this.__onClose = props['onclose'];
        this.__onBeforeClose = props['onbeforeclose'];
        this.__onBeforeOpen = props['onbeforeopen'];
        let closeStr = '';
        if(props.hasOwnProperty('closable')){
            closeStr = `<div class='ui-panel-header-bar'>
                            <b class='ui-dialog-close' e-click='__close'/>
                        </div>`;
        }
        this.setExcludeProps(['title','onopen','onclose','onbeforeopen','onbeforeclose','height','width','closable']);
        return `
            <div class={{'ui-dialog' + (__show?'':' ui-dialog-hide')}} >
                <div class={{'ui-mask' + (__show?'':' ui-mask-hide')}} />
                <div x-animationbox e-transitionend='__checkClose'>
                    <div class='ui-dialog-body'>
                        <div>
                            <div class='ui-panel' style='width:${props['width']};height:${props['height']}' >
                                <div class='ui-panel-header'>
                                    <span class='ui-panel-title'>${props['title']}</span>
                                    ${closeStr}
                                </div>
                                <div class='ui-panel-bodyct'>
                                    <slot />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    public data() {
        return{
            __show:false,
            __open:false
        }
    }

    /**
     * 关闭dialog
     */
    __close(){
        if(this.__onBeforeClose && UITool.invokePropMethod(this,this.__onBeforeClose,this.model)){
            return;
        }
        this.model['__open'] = false;
    }

    /**
     * 打开dialog
     */
    __open(){
        if(this.__onBeforeOpen && UITool.invokePropMethod(this,this.__onBeforeOpen,this.model)){
            return;
        }
        this.model['__show'] = true;
        this.model['__open'] = true;
        if(this.__onOpen){
            UITool.invokePropMethod(this,this.__onOpen,this.model);
        }
    }

    /**
     * 检查关闭
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __checkClose(model,dom,evObj,e){
        if(e.target !== this.domManager.renderedTree.children[1].node){
            return;
        }
        if(!this.model['__open']){
            this.model['__show'] = false;
            if(this.__onClose){
                UITool.invokePropMethod(this,this.__onClose,this.model);
            }
        }
    }
}

class UIDialogBody extends UIPanelBody{}
//注册模块
Nodom.registModule(UIDialog,'ui-dialog');
Nodom.registModule(UIDialogBody,'ui-dialog-body');
