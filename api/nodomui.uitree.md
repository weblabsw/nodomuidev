<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UITree](./nodomui.uitree.md)

## UITree class

树形控件

**Signature:**

```typescript
export declare class UITree extends BaseInput 
```
**Extends:** BaseInput

## Methods

|  Method | Modifiers | Description |
|  --- | --- | --- |
|  [\_\_initValue()](./nodomui.uitree.__initvalue.md) | <code>protected</code> | 首次渲染事件 |
|  [template(props)](./nodomui.uitree.template.md) |  | 模版函数 |

