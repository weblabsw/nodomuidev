import { Module, Nodom } from "nodom3";
/**
 * 参数说明
 * percent:       当前进度
 * format:        内容模板函数 `(percent) => text | HtmlElementString`
 * hide-text:     是否隐藏文本
 * type:          类型 line | circle
 * size:          尺寸 small | normal | large
 * stroke-color:  已完成进度条色
 * trail-color:   未完成进度条色
 */
/**
 * UIProgress 进度条
 * @public
 */
export class UIProgress extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__strokeColor = props['stroke-color'];
        this.__trailColor = props['trail-color'];
        let percent = props['percent'] || 0;
        const size = props['size'] || 'small';
        const showInfo = !props.hasOwnProperty('hide-text');
        const format = props['format'] || (() => "{{percent}}%");
        this.__type = props['type'] || 'line';
        //percent必须保持在0-100之间
        if (percent < 0) {
            percent = 0;
        }
        else if (percent > 100) {
            percent = 100;
        }
        const progress = this.__type === 'line' ?
            `<div class='ui-progress-bar'>
        <div class='ui-progress-outer'
          style={{'${this.__trailColor ? 'background-color: ' + this.__trailColor + ';' : ''}'}}>
          <div class='ui-progress-inner'
            style={{'width:' +  percent + '%;${this.__strokeColor ? 'background-color: ' + this.__strokeColor + ';' : ''}'}}></div>
        </div>
      </div>` : `<div class='ui-progress-round'>
        <div class='ui-progress-track'>
          <div class='ui-progress-wrapper'>
            <div class='ui-progress-left' style={{__getCircleStyle('left')}}/>
          </div>
          <div class='ui-progress-wrapper'>
            <div class='ui-progress-right' style={{__getCircleStyle('right')}}/>
          </div>
        </div>
      </div>`;
        //显示文本
        const progressInfo = showInfo ? `<div class='ui-progress-text'>${format()}</div>` : '';
        this.model['percent'] = percent;
        this.setExcludeProps(['type', 'percent', 'format', 'hide-text', 'size', 'stroke-color', 'trail-color']);
        return `<div
          class='ui-progress ui-progress-${size} ui-progress-${this.__type}'
          role='progressbar'
          aria-valuenow='{{percent}}'
          aria-valuemin='0'
          aria-valuemax='100'
        >
          ${progress}
          ${progressInfo}
      </div>`;
    }
    /**
     * 获取圆圈样式
     * @param part -  左半或右办
     * @returns style 样式内容
     */
    __getCircleStyle(part) {
        const perc = this.model['percent'];
        let style = '';
        if (part === 'right' && perc <= 50) {
            style = `transform: rotate(${perc * 3.6 + 45}deg);`;
        }
        else if (part === 'left' && perc <= 50) {
            style = 'transform: rotate(-45deg);';
        }
        else if (part === 'right' && perc > 50) {
            style = 'transform: rotate(225deg);';
        }
        else {
            style = `transform: rotate(${(perc - 50) * 3.6 - 45}deg);`;
        }
        if (this.__strokeColor) {
            style += 'border-bottom-color: ' + this.__strokeColor + ';' + 'border-' + (part === 'left' ? 'right' : 'left') + '-color: ' + this.__strokeColor + ';';
        }
        if (this.__trailColor) {
            style += 'border-top-color: ' + this.__trailColor + ';' + 'border-' + part + '-color: ' + this.__trailColor + ';';
        }
        return style;
    }
}
//注册模块
Nodom.registModule(UIProgress, 'ui-progress');
//# sourceMappingURL=progress.js.map