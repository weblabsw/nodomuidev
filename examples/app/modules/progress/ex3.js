import { Module } from "/dist/nodomui.min.js";
export class progressex3 extends Module {
    template() {
        return `
          <div style='padding:15px'>
            <div style='width:300px'>
                <ui-progress type='circle' size='small' percent={{percent}} style='margin: 5px'/>
                <ui-progress type='circle' size='normal' percent={{percent}} style='margin: 5px'/>
                <ui-progress type='circle' size='large' percent={{percent}} show-info style='margin: 5px'/>
            </div>
            <ui-button e-click="subPercent" title='-' />
              <ui-button e-click="addPercent" title='+' />
          </div>
        `
    }
    data() {
        return {
            percent: 10,
        }
    }

    addPercent(model, dom) {
        console.log(this.model.percent);
        this.model.percent += 3;
    }

    subPercent(model, dom) {
        console.log(this.model.percent);
        this.model.percent -= 3;
    }
}
