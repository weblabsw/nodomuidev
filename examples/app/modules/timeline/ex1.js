import { Module } from '/dist/nodomui.min.js'
export class timelineex1 extends Module {
    template(props) {
        return `
            
        <ui-timeline 
        $data={{timelist1}}
        reverse>  
        </ui-timeline>
        `;
    }
    data() {
        return {
            timelist1: this.genData1(),
        }
    }
    genData1() {
        return [
            { ttitle: "创建成功", tdate: "2023-06-11", },
            { ttitle: "通过审核", tdate: "2023-06-13", },
            { ttitle: "活动按期开始", tdate: "2023-06-15" }
        ]
    }
}