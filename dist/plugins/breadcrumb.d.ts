import { Module } from "nodom3";
export declare class UIBreadcrumb extends Module {
    /**
     * 待显示的item
     */
    __items: Array<UIBreadcrumbItem>;
    /**
     *  分隔符
     */
    __separator: string;
    /**
     * 图标分隔符
     */
    __iconSeparator: string;
    template(props?: object): string;
    /**
     * 添加item
     * @param item - 待加入的Breadcrumb Item
     */
    __addItem(item: UIBreadcrumbItem): void;
}
export declare class UIBreadcrumbItem extends Module {
    /**
     * 父组件
     */
    private __parent;
    /**
     * 标题
     */
    private __title;
    /**
     * 跳转地址
     */
    private __toPath;
    template(props?: object): string;
    /**
     * 点击事件	跳转路由
     */
    private __clickItem;
}
