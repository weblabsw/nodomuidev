import { Module,Nodom } from "nodom3";

/**
 * 参数说明
 * title:           标题
 * icon:            图标
 * vertical-icon    图标位置 left top right bottom,默认left
 * theme:           主题 default active error success warn，默认default
 * size:            按钮size tiny normal large，默认normal
 * nobg:            不需要背景 true/false，默认false
 * circle:          圆形
 * disable:         禁用，true/false,默认false
 */

/**
 * UIButton 按钮
 * @public
 */
export class UIButton extends Module{
    /**
     *模板函数html
     * @privateRemarks
     */
    template(props?: object): string {
        const arr = ['ui-btn'];
        if(props.hasOwnProperty('vertical-icon')){
            arr.push('ui-btn-vert');
        }
        if(props.hasOwnProperty('disabled')){
            arr.push('ui-btn-disable');
        }
        //字体
        arr.push('ui-btn-' + (props['size']||'normal'));
        let btnStr = '';
        //图标
        if(props['icon']){
            btnStr = `<b class='ui-btn-icon ui-icon-${props['icon']}'/>`;
        }
        //无背景
        if(props.hasOwnProperty('nobg')){
            arr.push('ui-btn-nobg');
        }else if(!props.hasOwnProperty('disabled')){
            arr.push('ui-btn-' + (props['theme']||'default'));
        }
        //圆形
        if(props.hasOwnProperty('circle')){
            arr.push('ui-btn-circle');
        }
        if(!props['title']){
            arr.push('ui-btn-notext');
        }
        const disable = props.hasOwnProperty('disabled')?'disabled':'';
        this.setExcludeProps(['theme','size','nobg','circle','icon','verticle-icon','title','disabled']);
        return `
            <button class='${arr.join(" ")}' ${disable}>
                ${btnStr} ${props['title']||''}
            </button>
        `;
    }
}
Nodom.registModule(UIButton,'ui-button');