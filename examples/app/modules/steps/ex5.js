import { Module } from '/dist/nodomui.min.js'
export class stepsex5 extends Module {
    template(props) {
        return `
            <div >
                <ui-steps 
                    style="height:300px"
                    current={{current}} 
                    $data={{steps}}
                    direction="vertical"
                    >
                </ui-steps>    
            </div>
        `;
    }
    data() {
        return {
            current: 1,
            steps: [
                {
                    title: "Step 1",
                    desc: "This is a description."
                }, {
                    title: "Step 2",
                    desc: "This is a description."
                }, {
                    title: "Step 3",
                    desc: "This is a description."
                },{
                    title: "Step 4",
                    desc: "This is a description."
                }
            ],
        }
    }
    next() {
        this.model['current'] += 1;
    }
}