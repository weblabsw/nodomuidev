/**
 * ITipCfg uiTip配置项
 * @public
 */
export interface ITipCfg{
    /**
     * id，由管理器自动生成
     */
    id?:number;
    /**
     * 位置，可选值：top,left,right,bottom
     * 默认top
     */
    position?:string;

    /**
     * 显示信息
     */
    text:string;

    /**
     * 是否显示关闭按钮，默认false
     */
    closable?:boolean;

    /**
     * 消息框图标
     */
    icon?:string;

    /**
     * 主题active,success,error,warn,default
     * 默认default
     */
    theme?:string;

    /**
     * 是否排他true:关闭该区域其它提示框，默认false
     */
    exclusive?:boolean;

    /**
     * 显示时长(ms)，如果设置closable，则无效，默认2000
     */
    timeout?:number;

    /**
     * 宽度，单位为px，默认300
     */
    width?:number;
    
    /**
     * 是否显示icon，由icon参数自动设置
     */
    showIcon?:boolean;

    /**
     * 关闭时间，有timeout计算产生
     */
    closeTime?:number;
}