import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { timelineex1 } from './ex1.js'
import { timelineex2 } from './ex2.js'
import { timelineex3 } from './ex3.js'
export class MTimeline extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, timelineex1, timelineex2, timelineex3];
    template(props) {
        return `
            <div class='--component'>
                <h1>Timeline 时间轴</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='timeline'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{gridAttrs}} />

            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}