import { Module, Nodom, ModuleFactory } from "nodom3";

export class UIBreadcrumb extends Module {
    /**
     * 待显示的item
     */
    public __items: Array<UIBreadcrumbItem> = [];
    /**
     *  分隔符
     */
    public __separator: string;
    /**
     * 图标分隔符
     */
    public __iconSeparator: string;

    template(props?: object): string {
        // 分隔符
        this.__separator = props["separator"];
        // 图标分隔符
        this.__iconSeparator = props["icon-separator"];

        // 分隔符和图标分隔符都没有的话，优先为分隔符”/“
        if (!this.__separator && !this.__iconSeparator) {
            this.__separator = "/";
        }

        this.setExcludeProps(["separator"]);
        return `
            <div class='ui-breadcrumb'>
                <slot />
            </div>
        `;
    }

    /**
     * 添加item
     * @param item - 待加入的Breadcrumb Item
     */
    __addItem(item: UIBreadcrumbItem) {
        if (!this.__items.includes(item)) {
            const itemLength = this.__items.length;
            if (itemLength > 0) {
                this.__items[itemLength - 1].model["__needSeparator"] = true;
            }
            this.__items.push(item);
        }
    }
}

export class UIBreadcrumbItem extends Module {
    /**
     * 父组件
     */
    private __parent: UIBreadcrumb;
    /**
     * 标题
     */
    private __title: string;
    /**
     * 跳转地址
     */
    private __toPath: string;

    template(props?: object): string {
        //设置父accordion
        this.__parent = ModuleFactory.get(this.srcDom.slotModuleId) as UIBreadcrumb;

        //添加到父item集合
        this.__parent.__addItem(this);
        let separator = "";

        //分隔符和图标分隔符都有的话，优先为分隔符 其次是图标分隔符
        if (this.__parent.__separator) {
            separator = this.__parent.__separator;
        } else {
            separator = `<b class='ui-icon-${this.__parent.__iconSeparator} ui-icon-sizesmall'/>`;
        }

        if (props["title"]) {
            this.__title = props["title"];
        }

        this.__toPath = props["to"];

        this.setExcludeProps(["title", "to"]);

        let clazz = "class = '";
        if (this.__toPath || this.slots.size > 0) {
            clazz += "ui-breadcrumb-item-link'";
        } else {
            clazz += "ui-breadcrumb-item-unlink'";
        }

        return `
            <div class='ui-breadcrumb-item'>
                    <div ${clazz} e-click='__clickItem' x-if={{this.__title}}>
                        ${this.__title}
                    </div>
                    <div ${clazz} x-else>
                        <slot />
                    </div>
                    <div class="ui-breadcrumb-separator" x-if={{this.model.__needSeparator == true}}>
                        ${separator}
                    </div>
            </div>
        `;
    }

    /**
     * 点击事件	跳转路由
     */
    private __clickItem() {
        if (this.__toPath) {
            Nodom["$Router"].go(this.__toPath);
        }
    }
}

Nodom.registModule(UIBreadcrumb, "ui-breadcrumb");
Nodom.registModule(UIBreadcrumbItem, "ui-breadcrumb-item");
