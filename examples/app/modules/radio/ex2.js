import{Module} from '/dist/nodomui.min.js'
export class radioex2 extends Module{
    template(){
        return `
            <div>
                <div>值：{{sexy}}</div>
                <label>性别：</label>
                <ui-radiogroup field='sexy'>
                    <ui-radio x-repeat={{sexies}} value={{value}} title={{title}} e-click='show'/>
                </ui-radiogroup>
            </div>
        `
    }

    data(){
        return {
            sexy:1,
            sexies:[{value:0,title:'男'},{value:1,title:'女'},{value:2,title:'中性'}]
        }
    }

    show(m){
        console.log(m);
    }
}