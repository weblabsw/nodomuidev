import { Module } from "nodom3";
/**
 * bgcolor          默认t背景色
 * color            默认前景色
 * active-bgcolor    激活背景色
 * active-color      激活前景色
 * tabs             页签对象数组
 *  包括：
 *      title       页签标题
 *      active      是否激活
 *      closable    是否可关闭
 *      path        路由路径（需先定义路由）
 * onChange         tab切换时事件，this指向RouterTab插件
 *      参数1：      切换前tab，可能为undefined
 *      参数2:       切换后tab
 * onTabClose       tab关闭事件，this指向RouterTab插件
 *      参数1：      被关闭tab
 */
/**
 * UIRouterTab 路由标签页
 * @public
 */
export declare class UIRouterTab extends Module {
    /**
     * 当前tab配置项
     */
    private __currentTab;
    /**
     * 拖动参数
     */
    private __dragParam;
    /**
     * tab 关闭事件
     */
    private __onTabClose;
    /**
     * tab 切换事件
     */
    private __onTabChange;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        tabs: any[];
    };
    /**
     * 头部拖动开始
     * @param model - 对应model
     * @param dom - Virtual dom
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragStart;
    /**
     * 拖动
     * @param model - 对应model
     * @param dom - Virtual dom
     * @param evObj - NEvent对象
     * @param e - event对象
     * @returns
     */
    private __drag;
    /**
     * 拖动停止
     * @param model - 对应model
     * @param dom - Virtual dom
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragEnd;
    /**
     * 移动
     * @param e - event对象
     * @returns
     */
    private __move;
    /**
     * 清除多余active
     */
    private __cleanActive;
    /**
     * 切换到目标tab
     * @param tab - 目标tab
     * @returns
     */
    private __changeTo;
    /**
     * 手动active
     * @param data - model或title或索引号
     */
    __activeTab(data: string | number | RouterTabItem): void;
    /**
     * 关闭页签
     * @param model - TabItem
     * @returns
     */
    __closeTab(model: RouterTabItem): void;
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender(): void;
}
/**
 * RouterTabItem 配置接口类型
 * @public
 */
export declare type RouterTabItem = {
    /**
     * 索引号
     */
    idx: number;
    /**
     * tab标题
     */
    title: string;
    /**
     * 是否可关闭
     */
    closable?: boolean;
    /**
     * 是否激活
     */
    active?: boolean;
    /**
     * 模块路径
     */
    path: string;
};
