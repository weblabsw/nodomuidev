import { Expression, Module } from "nodom3";
/**
 * BaseInput 绑定数据型父类组件
 * @public
 */
export class BaseInput extends Module {
    /**
     * 重构
     */
    constructor() {
        super();
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        console.log(props);
        if (props['field']) {
            this.__field = props['field'];
            this.__onChange = props['onchange'];
            this.model['__value'] = this.get(this.srcDom.model, this.__field);
        }
        // this.setExcludeProps(['__value']);
        return null;
    }
    /**
     * 首次渲染前事件
     */
    onBeforeFirstRender() {
        if (this.__field) {
            // 在父dom添加__value属性，父修改时，可驱动子模块渲染
            this.srcDom.vdom.setProp('__value', new Expression(this.__field));
            //添加监听
            this.watch('__value', (m, key, ov, nv) => {
                this.__change(ov, nv);
            });
        }
    }
    /**
     * 更改值
     * 需要对父模块对应数据项进行更改
     * @param oldValue - 旧值
     * @param newValue - 新值
     */
    __change(oldValue, newValue) {
        if (!this.__field) {
            return;
        }
        //修改props值，避免二次渲染
        this.props['__value'] = newValue;
        //调用change事件
        if (this.__onChange) {
            this.invokeOuterMethod(this.__onChange, oldValue, newValue);
        }
        //更改父模块对应数据项
        this.set(this.srcDom.model, this.__field, newValue);
        //初始化显示
        this.__initValue();
    }
    /**
     * 初始化value，当模块依赖值进行初始化时有用
     */
    __initValue() { }
    /**
     * 获取值
     * @returns - 模块值
     */
    __getValue() {
        return this.model['__value'];
    }
}
//# sourceMappingURL=baseinput.js.map