import{Module} from '/dist/nodomui.min.js'
export class badgeex4 extends Module{
    template(props){
        return `
            <div>
                <ui-badge icon="ui-icon-time" >
                    <ui-button size="small"  title='日期' />
                </ui-badge>

                <style>
                    .ui-badge{
                        margin-right:40px;
                        margin-bottom:10px;
                    }
                </style>
            </div>
        `;
    }
}