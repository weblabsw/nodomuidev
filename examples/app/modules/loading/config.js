export const EXCONFIG = {
    name:'Loading',
    desc:'使用ui-loading(加载)组件进行设置。',
    loadingAttrs:[{
        name:'color',
        desc:'颜色',
        type:'string',
        values:'-',
        defaults:'-'
    },{
        name:'radius',
        desc:'半径',
        type:'number',
        values:'-',
        defaults:'40'
    }],
    loadingMethods:[{
        name:'__show',
        desc:'打开loading',
        params:'limit(停留时间),imgUrl(动画url地址)',
        returns:'-'
    },{
        name:'__close',
        desc:'关闭loading',
        params:'force(是否强制完全关闭，如果为true，则不计算openCount，直接关闭)',
        returns:'-'
    }],
    examples:[{
        title:'简单应用',
        desc: '通过nuiloading调用__show()方法进行设置。',
        imp:'注意：ui-loading插件必须放置在根模块。',
        name:'ex1'
    },{
        title:'叠加打开2次(共5s)后自动关闭',
        desc: '通过定时器调用__show()方法设置。',
        name:'ex2'
    },{
        title:'打开1次，3s后手动关闭',
        desc: '通过定时器调用__close()方法设置。',
        name:'ex3'
    },{
        title:'打开2次，4s后强制关闭',
        desc: '通过定时器同时调用__show()和_close()方法设置。',
        name:'ex4'
    },{
        title:'自定义gif动画',
        desc: '通过传入__show()自定义gif图片参数设置。',
        name:'ex5'
    },{
        title:'自定义gif动画 3s关闭',
        desc: '通过__show()的第一个参数设置关闭自定义gif动画时间。',
        name:'ex6'
    },{
        title:'自定义半径',
        desc: '通过radius设置',
        name:'ex7'
    }]
}