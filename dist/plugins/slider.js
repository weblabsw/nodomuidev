import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
import { UITool } from './uibase';
/**
 *  配置说明：
 *  field 绑定滑块百分比值，默认为0
 *  max 最大值，默认为100
 *  min 最小值，默认为0
 *  vertical 是否竖向
 *  height 滑动条高度，竖向模式必填
 *  step 滑动步数
 *  show-step 显示间断点
 */
/**
 *  UISlider 滑动条
 *  @public
 */
export class UISlider extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__max = props['max'] || 100;
        this.__min = props['min'] || 0;
        this.__vertical = props.hasOwnProperty('vertical');
        this.__disabled = props.hasOwnProperty('disabled');
        this.__step = props['step'] || 1;
        this.__showStep = props.hasOwnProperty('show-step');
        // 竖直情况下，获得滑动条高度
        if (this.__vertical) {
            this.__height = props['height'];
        }
        this.setExcludeProps(['height', 'max', 'min', 'vertical', 'disabled', 'step', 'show-step']);
        let className;
        let style;
        if (this.__vertical) {
            className = 'ui-slider-vertical';
            style = 'height:' + this.__height + 'px;';
        }
        else {
            className = 'ui-slider-normal';
        }
        //设置disabled时的class
        const disabledClass = this.__disabled ? '-disabled' : '';
        let stopGroup = '';
        if (this.__showStep) {
            const length = this.__max - this.__min;
            if (this.__vertical) {
                for (let i = this.__step; i < length; i += this.__step) {
                    stopGroup += `<div class='ui-slider-vertical-stop' style="bottom: ${i / length * 100}%;"/>`;
                }
            }
            else {
                for (let i = this.__step; i < length; i += this.__step) {
                    stopGroup += `<div class='ui-slider-normal-stop' style="left: ${i / length * 100}%;"/>`;
                }
            }
        }
        return `
                <div class='${className}' style='${style}' e-mousedown='__onSliderClick'>
                    <div class={{'ui-slider-bar${disabledClass}'}} />
                    <div class={{'ui-slider-dot${disabledClass}'}} e-mousedown='__dragStart' />
                    ${stopGroup}
                </div>
        `;
    }
    /**
     * 渲染前
     */
    onBeforeRender() {
        //初始化field值
        this.__initValue();
    }
    /**
     * 挂载后
     */
    onMount() {
        //获取元素及初始化slider
        this.__pel = this.getElement(this.domManager.vdomTree.key);
        this.__el = this.getElement(this.domManager.vdomTree.children[1].key);
        this.__bel = this.getElement(this.domManager.vdomTree.children[0].key);
        this.__initSlider();
    }
    /**
     * 初始化绑定的field，若不存在则为0
     */
    __initValue() {
        if (!this.model['__value'])
            this.model['__value'] = 0;
    }
    /**
     * 获取初始化slider所需参数
     */
    __initSlider() {
        // 若是自定义范围，则将value值精确化
        let value = this.model['__value'];
        value = (value - this.__min) / (this.__max - this.__min) * 100;
        // 根据是否竖直模式初始生成slider
        if (this.__vertical) {
            this.__currentLen = this.__pel.offsetHeight - this.__el.offsetHeight;
        }
        else {
            this.__currentLen = this.__pel.offsetWidth - this.__el.offsetWidth;
        }
        this.__setPosition(value);
    }
    /**
     * 拖拽开始，获取鼠标位置及dot位置
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragStart(model, dom, evObj, e) {
        if (this.__disabled)
            return;
        e.preventDefault();
        this.__isDrag = true;
        window.addEventListener('mousemove', drag);
        window.addEventListener('mouseup', dragEnd);
        const that = this;
        /**
         * 拖拽计算
         * @param event - Event对象
         */
        function drag(event) {
            if (that.__isDrag) {
                //处理鼠标拖动事件
                that.__handleEvent(event);
            }
        }
        /**
         *  结束拖拽，初始化相关参数
         */
        function dragEnd() {
            if (that.__isDrag) {
                window.removeEventListener('mousemove', drag);
                window.removeEventListener('mouseup', dragEnd);
                that.__isDrag = false;
            }
        }
    }
    /**
     * 计算位置
     * @param value - 百分比值
     * @returns 实际位置值
     */
    __calPosition(value) {
        return (value - this.__min) / (this.__max - this.__min) * 100;
    }
    /**
     * 设置拖动后效果
     * @param value - 滑块应处位置的百分比值
     */
    __setDisplace(value) {
        const newValue = this.__calPosition(value);
        if (this.__vertical) {
            this.__el.style.bottom = newValue + '%';
            this.__bel.style.height = newValue + '%';
        }
        else {
            this.__el.style.left = newValue + '%';
            this.__bel.style.width = newValue + '%';
        }
    }
    /**
     * 设置滑动条移动效果
     * @param scale - 移动后，滑块所处的百分比值
     */
    __setPosition(scale) {
        if (scale === null || isNaN(scale))
            return;
        // 现位置与总长度比例
        let value;
        // dot拖拽超过总长度
        if (scale < 0)
            scale = 0;
        if (scale > 100)
            scale = 100;
        // 实际步长
        const lengthPerStep = 100 / ((this.__max - this.__min) / this.__step);
        // 步数
        const steps = Math.round(scale / lengthPerStep);
        // 实际值
        value = steps * lengthPerStep * (this.__max - this.__min) * 0.01 + this.__min;
        //设置效果
        this.__setDisplace(value);
        // 更新value
        // 当value位于（-1，1）时，保留两位小数
        if (value >= 1 || value === 0 || value <= -1) {
            value = Math.trunc(value);
        }
        else {
            value = value.toFixed(3).slice(0, -1);
        }
        this.model['__value'] = value;
    }
    /**
     * 鼠标点击slider（没有点击dot的前提）
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __onSliderClick(model, dom, evObj, e) {
        if (this.__disabled || this.__isDrag)
            return;
        this.__handleEvent(e);
    }
    /**
     * 处理拖动事件，计算位置相关
     * @param event - 鼠标事件
     */
    __handleEvent(event) {
        let newL = UITool.getDisplacement(event, this.__pel, this.__vertical);
        newL = newL / this.__currentLen * 100;
        this.__setPosition(newL);
    }
}
//注册模块
Nodom.registModule(UISlider, 'ui-slider');
//# sourceMappingURL=slider.js.map