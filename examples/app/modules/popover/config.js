export const EXCONFIG = {
    name: 'Popover',
    lowerCaseName: 'popover',
    desc: '使用ui-popover组件进行设置。',
    popoverAttrs: [{
        name: 'placement',
        desc: '弹出框的弹出位置',
        type: 'string',
        values: 'bottom | top | left | right',
        defaults: 'bottom'
    }
    ],
    examples: [{
        title: '基础用法',
        desc: '触发弹出框的元素内容使用命名插槽trigger，弹出部分使用默认插槽。',
        name: 'ex1'
    }, {
        title: '弹出位置',
        desc: '弹出位置通过placement属性设置，值可有bottom,top,left,right，默认为bottom。',
        name: 'ex2'
    },
    ]
}