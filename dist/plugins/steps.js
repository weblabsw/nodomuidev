import { Nodom, Module } from 'nodom3';
/**
 * 数据项
 * $data            步骤数组数据
 * 配置参数
 * current          当前步骤位置
 * direction        步骤条方向 vertical/horizontal 默认horizontal
 * finish-status    完成时状态 active/success/warn/error
 * process-status   进行时状态
 * success-select   完成状态图标
 */
/**
 * UISteps 步骤条
 * @beta
 */
export class UISteps extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__current = parseInt(props['current']);
        this.__finishColor = props['finish-status'] || '';
        this.__processColor = props['process-status'] || '';
        this.__direction = props.hasOwnProperty("direction") ? `${props['direction']}` : "horizontal";
        this.__useSelect = props.hasOwnProperty('success-select');
        /**
         * 设置步骤条方向
         */
        const stepsStyle = (this.__direction === "horizontal") ? 'ui-steps-horizontal' : 'ui-steps-vertical';
        this.setExcludeProps(['current', 'direction']);
        return `
            <div class='ui-steps ${stepsStyle}'>
                <for cond={{data}}  class={{__genStepClass(__status)}} index='idx'>
                    <!-- 头部 -->
                    <div class="ui-step-head">
                        <!-- 节点 -->
                        <div class="ui-step-text" x-if={{!icon}}>
                            <div class={{'ui-step-inner ' + __checked}}>
                                {{__getIndex(__status,idx)}}
                            </div>
                        </div>
                        <div class="ui-step-icon" x-else>
                            <div class={{'ui-step-inner ' + icon}}>
                            </div>
                        </div>
                        <!-- 线条 -->
                        <div class="ui-step-line">
                            <b class="ui-step-line-inner" />
                        </div>
                    </div>
                    <!-- 内容区域 -->
                    <div class="ui-step-main">
                        <div class="ui-step-title" x-show={{title}}>
                            {{title}}
                        </div>
                        <div class="ui-step-description" x-show={{desc}}>
                            {{desc}}
                        </div>
                    </div>
                </for>
            </div>
        `;
    }
    /**
     * 渲染前
     */
    onBeforeRender() {
        this.__setCurrent(this.__current);
    }
    /**
     * 初始化步骤节点状态
     * @param cur - 当前节点
     */
    __setCurrent(cur) {
        const array = this.model['data'];
        if (!Array.isArray(array)) {
            return;
        }
        for (let i = 0; i < array.length; i++) {
            const status = i < cur ? 1 : (i === cur ? 2 : 3);
            //为每个节点设置初始状态样式
            this.__setStatus(status, array[i]);
        }
    }
    /**
     * 设置每个步骤节点状态
     * @param status - 状态类型
     * @param arr - 节点对象
     */
    __setStatus(status, arr) {
        arr.__status = status;
        arr.__checked = '';
        //如果成功状态并且用户设置了成功的样式,则显示为"√"，否则默认显示数字
        if (status === 1 && this.__useSelect) {
            arr.__checked = 'ui-icon-select';
        }
        //如果当前节点设置了样式为error，则显示为“x”
        else if (status === 2 && this.__processColor === 'error') {
            arr.__checked = 'ui-icon-cross';
        }
    }
    /**
     * 设置每个step节点样式
     * @param status - 状态值
     * @returns - class值
     */
    __genStepClass(status) {
        const clsArr = ['ui-step'];
        switch (status) {
            // 完成时状态
            case 1:
                clsArr.push(`ui-step-success ${this.__finishColor}`);
                break;
            //当前状态
            case 2:
                clsArr.push(`ui-step-process ${this.__processColor}`);
                break;
            //等待状态
            case 3:
                clsArr.push(`ui-step-wait`);
        }
        return clsArr.join(' ');
    }
    /**
     * 设置节点显示图标还是数字
     * @param status - 状态类型
     * @param idx - 索引
     * @returns 节点显示值
     */
    __getIndex(status, idx) {
        if ((status === 1 && this.__useSelect) || (status === 2 && this.__processColor === 'error')) {
            return '';
        }
        return idx + 1;
    }
}
//注册模块
Nodom.registModule(UISteps, 'ui-steps');
//# sourceMappingURL=steps.js.map