import { Model, Module, ModuleFactory, NEvent, RenderedDom } from "nodom3";

//关闭右键菜单
document.oncontextmenu = function (e) {
    e.preventDefault();
};

/**
 * 工具类
 * @public
 */
export class UITool {
    /**
     * 去掉字符串的空格
     * @param src - 需处理的字符串
     */
    static clearSpace(src: string): string {
        if (src && typeof src === 'string') {
            return src.replace(/\s+/g, '');
        }
        return src;
    }

    /**
     * 计算实际位置
     * @param el - 待计算的element
     * @returns  [x坐标,y坐标]
     */
    static getRelPos(el: HTMLElement): number[] {
        let x = el.offsetLeft - el.scrollLeft;
        let y = el.offsetTop - el.scrollTop;
        const offsetEl: HTMLElement = <HTMLElement>el.offsetParent;
        while (el) {
            el = <HTMLElement>el.parentElement;
            x -= el.scrollLeft;
            y -= el.scrollTop;
            if (offsetEl === el || el === document.body) {
                break;
            }
        }
        return [x, y];
    }

    /**
     * 获取实际位置
     * @param el - 待计算的element
     * @returns  [x坐标,y坐标]
     */
    static getRealPos(el: HTMLElement) {
        let x = el.offsetLeft - el.scrollLeft;
        let y = el.offsetTop - el.scrollTop;
        let offsetEl: HTMLElement = <HTMLElement>el.offsetParent;
        while (el) {
            el = <HTMLElement>el.parentElement;
            x -= el.scrollLeft;
            y -= el.scrollTop;
            if (offsetEl === el) {
                x += el.offsetLeft;
                y += el.offsetTop;
                offsetEl = <HTMLElement>offsetEl.offsetParent;
            }
            if (el === document.body) {
                break;
            }
        }
        return [x, y];
    }

    /**
     * 计算位置
     * @param event - 鼠标事件
     * @param relPos - 相对位置：1下 2右
     * @param width - dom宽
     * @param height - dom高
     * @returns  [x,y,width,height]
     */
    static cacPosition(event: MouseEvent, relPos: number, width: number, height: number): number[] {
        const relEl = <HTMLElement>event.currentTarget;
        const rect = relEl.getBoundingClientRect();

        //box 阴影面padding为5
        const padding = 5;
        //页面高度和宽度
        const gwidth = window.innerWidth;
        const gheight = window.innerHeight;
        //相对位置
        let x;
        let y;
        if (relPos === 1) {//纵向
            x = rect.x - padding;
            y = rect.y + rect.height;
            if (y + height > gheight + padding) {  //下方不够放
                if (y > gheight / 2 - padding) {  //y过半，移到上方
                    y = rect.y - height;
                    if (y < 0) {
                        height += y - padding;
                        y = padding;
                    }
                } else {  //在下边
                    if (y + height > gheight - padding) {  //高度超出边界
                        height = gheight - y - padding * 2;
                    }
                }
            }
        } else { //横向
            x = rect.x + rect.width + padding;
            y = rect.y;
            if (x + width > gwidth - padding) {  //右方不够放，放左放
                x = rect.x - width - padding;
            }
        }
        return [x | 0, y | 0, width | 0, height | 0];
    }

    /**
     * 将对象转换为属性字符串
     * @param obj - 要转换的 object
     * @returns     转换完成的 string
     */
    static toAttrString(obj: object): string {
        let res = '';
        for (const [key, val] of Object.entries(obj)) {
            res += `${key}='${val}' `;
        }

        return res.trim();
    }

    /**
     * 鼠标拖动事件，获取拖动后位置（相对父元素）
     * @param event - 鼠标事件
     * @param el - 拖动事件的父Element
     * @param vertical - 是否竖直
     * @returns 拖动后位置
     * @beta
     */
    static getDisplacement(event: MouseEvent, el: HTMLElement, vertical: boolean): number {
        const rect = el.getBoundingClientRect();
        let newL: number;
        if (vertical) {
            newL = rect.bottom - event.clientY;
        } else {
            newL = event.clientX - rect.left;
        }
        return newL;
    }

        /**
     * 鼠标拖动事件，获取拖动后位置（相对父元素）
     * @param event - 鼠标事件
     * @returns x,y - 鼠标指针相对于浏览器窗口左上角的水平距离和垂直距
     * @beta
     */
    static returnClientXY(event: MouseEvent) {
        const x = event.clientX;
        const y = event.clientY;
        return { x, y }
    }

    /**
     * 调用通过props传递的方法
     * @param module -      当前模块       
     * @param methodName -  方法名 
     * @param params -      参数数组
     * @returns             方法实际返回值
     */
    static invokePropMethod(module:Module,methodName:string,...params){
        const pm = ModuleFactory.get(module.srcDom.moduleId);
        if(!pm || !pm[methodName]){
            return;
        }
        return pm[methodName](...params);
    }
}
