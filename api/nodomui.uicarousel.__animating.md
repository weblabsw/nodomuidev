<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UICarousel](./nodomui.uicarousel.md) &gt; [\_\_animating](./nodomui.uicarousel.__animating.md)

## UICarousel.\_\_animating property

是否处于动画中

**Signature:**

```typescript
__animating: boolean;
```
