import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { stepsex1 } from './ex1.js'
import { stepsex2 } from './ex2.js'
import { stepsex3 } from './ex3.js'
import { stepsex4 } from './ex4.js'
import { stepsex5 } from './ex5.js'


export class MSteps extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, stepsex1, stepsex2, stepsex3, stepsex4, stepsex5];
    template(props) {
        return `
            <div class='--component'>
                <h1>Steps 步骤</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='steps'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{stepsAttrs}} />

            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}