import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
import { UITool } from "./uibase";
import { NodomUI } from "./nodomui";
/**
 * 配置说明
 * $data：              列表数据数组
 * field:               绑定父模块的字段
 * value-field：        值字段
 * disable-field：      禁用字段（表示记录项不可点击）
 * list-width:          下拉框宽度，默认为select宽度，单位px
 * list-height:         下拉框最大高度，单位px
 * allow-search          是否需要输入搜索
 */
/**
 * UISelect 下拉框
 * @public
 */
export class UISelect extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__multiple = props.hasOwnProperty('multiple');
        this.__valueField = props['value-field'];
        this.__displayField = props['display-field'];
        this.__disableField = props['disable-field'];
        this.__listWidth = props['list-width'] ? parseInt(props['list-width'] + '') : undefined;
        this.__allowEmpty = props.hasOwnProperty('allow-empty');
        this.__allowSearch = props.hasOwnProperty('allow-search');
        let heightStyle = '';
        if (props['list-height']) {
            this.__listHeight = parseInt(props['list-height'] + '');
            heightStyle = "max-height:" + this.__listHeight + "px;";
        }
        let disableCtx = '';
        if (props['disable-field']) {
            disableCtx = "+ (" + props['disable-field'] + "?' ui-select-item-disable':'')";
        }
        let styleStr = '';
        let selectStr = '';
        if (this.__allowSearch) {
            styleStr = ` <div class='ui-select-input-wrap' e-click='__toggleBox:capture' e-keyup='__openBox'>
            <div class='ui-select-show '>
                 <input type="text" x-field="__content"    e-keyup='__filterItem' e-click='__changeInput'>
                </input>
            </div>`;
            selectStr = `
            <div class={{'ui-select-item' + (__selected?' ui-select-item-active':'')  ${disableCtx} }}
            x-repeat={{data}} e-click='__clickItem' x-show={{!__hidden}}>
            `;
        }
        else {
            styleStr = `
                <div class="ui-select-input-wrap" e-click='__toggleBox:captrue' >
                 <div class="ui-select-show" >{{showText}}</div>`;
            selectStr = `
                <div class={{'ui-select-item' + (__selected?' ui-select-item-active':'')  ${disableCtx} }}
                 x-repeat={{data}} e-click='__clickItem'>
             `;
        }
        this.setExcludeProps(['allow-empty', 'list-width', 'list-height', 'disable-field', 'value-field', 'display-field', 'multiple', 'allow-search']);
        return `
            <div class="ui-select" e-mouseleave='__closeBox'>
                ${styleStr}
                    <b class='ui-expand-icon ui-expand-icon-open'/>
            </div>

            <div class='ui-select-list-wrap' x-animationbox style={{'left:' + __x + 'px;top:' + __y + 'px;width:' + __width + 'px;'}}>
                <div class='ui-select-list'   style={{'${heightStyle}height:' + __height + 'px'}}>
                    <div>
                        ${selectStr}
                            <div class="ui-select-itemcontent" >
                                <slot innerRender/>
                            </div>
                            <b class="ui-select-icon" />
                        </div>
                    </div>
                </div>
            </div>
        </div>  
            `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __open: false,
            rows: [],
            showText: '',
            __x: 0,
            __y: 0,
            __width: 0,
            __height: 0,
            __content: ''
        };
    }
    /**
    * 选中后控制输入框
    * @param model - 对应model
    */
    __changeInput(model) {
        this.model['__content'] = '';
        for (const m of model['data']) {
            m['__selected'] = false;
        }
        for (const m of model['data']) {
            m['__hidden'] = false;
        }
    }
    /**
     * 输入内容进行筛选
     * @param model -     模型
     * @param dom -       当前节点
     * @param ev -        event object
     * @param e -         html event
     */
    __filterItem(model, dom, ev, e) {
        const rows = this.model['data'];
        if (!rows) {
            return;
        }
        this.__hidden = false;
        if (this.__disableField && model[this.__disableField]) {
            return;
        }
        const serachInput = e.target;
        const seachValue = serachInput.value;
        for (const m of rows) {
            if (m !== model) {
                if (m['htitle'].includes(seachValue) && !m[this.__disableField]) {
                    m['__hidden'] = false;
                }
                else {
                    m['__hidden'] = true;
                }
            }
        }
    }
    /**
     * 交替 list box
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __toggleBox(model, dom, evObj, e) {
        if (this.model['__open']) {
            this.__closeBox();
        }
        else {
            this.__openBox(model, dom, evObj, e);
        }
    }
    /**
     * 打开list box
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __openBox(model, dom, evObj, e) {
        model['__width'] = this.__listWidth ? this.__listWidth : e.currentTarget.offsetWidth + 10;
        this.__cacLoc(model, dom, evObj, e);
        this.model['__open'] = true;
    }
    /**
     * 关闭list box
     */
    __closeBox() {
        this.model['__open'] = false;
    }
    /**
     * 点击item
     * @param model - 点击的item对应model
     * @returns
     */
    __clickItem(model) {
        if (this.__disableField && model[this.__disableField]) {
            return;
        }
        let value;
        if (this.__multiple) {
            model.__selected = !model.__selected;
            //如果allowEmpty，则选中实际元素时，第一个元素取消选中，否则第一个元素选中
            value = [];
            const index = this.__allowEmpty ? 1 : 0;
            for (let i = index; i < this.model['data'].length; i++) {
                const m = this.model['data'][i];
                if (m.__selected) {
                    value.push(m[this.__valueField]);
                }
            }
            if (this.__allowEmpty) {
                //未选中实际节点，则空选择项被选中，否则空选择项取消选中
                if (value.length === 0) {
                    this.model['data'][0].__selected = true;
                }
                else {
                    this.model['data'][0].__selected = false;
                }
            }
        }
        else {
            if (model['__selected']) {
                return;
            }
            model['__selected'] = true;
            for (const m of this.model['data']) {
                if (m !== model) {
                    m['__selected'] = false;
                }
            }
            this.__closeBox();
            value = model[this.__valueField];
            this.model["__content"] = model['htitle'];
        }
        this.model['__value'] = value;
        this.model['__isEdit'] = false;
    }
    /**
     * 设置值
     * @returns
     */
    __initValue() {
        const emptyText = NodomUI.getText('pleaseSelect');
        super.__initValue();
        if (!Array.isArray(this.model['data'])) {
            return;
        }
        let value = this.model['__value'];
        //如果允许空，则需要在数据最前面加上"请选择"项
        if (this.__allowEmpty) {
            const rows = this.model['data'];
            //数组为空或者第一项不为空，都需要添加“请选择”项
            if (rows.length === 0 || rows[0][this.__displayField] !== emptyText) {
                const o = {};
                o[this.__valueField] = undefined;
                o[this.__displayField] = emptyText;
                this.model['data'].unshift(o);
            }
            if (!value || value.length === 0) {
                rows[0]['__selected'] = true;
            }
            else {
                rows[0]['__selected'] = false;
            }
        }
        const rows = this.model['data'];
        if (!this.__field || !Array.isArray(rows)) {
            return;
        }
        let showArr = [];
        if (this.__multiple) {
            if (!Array.isArray(value)) {
                value = [value];
            }
            for (const m of rows) {
                if (value.indexOf(m[this.__valueField]) !== -1) {
                    m.__selected = true;
                    showArr.push(m[this.__displayField]);
                }
            }
        }
        else {
            for (const m of rows) {
                if (value === m[this.__valueField]) {
                    m.__selected = true;
                    showArr.push(m[this.__displayField]);
                }
                else {
                    m.__selected = false;
                }
            }
        }
        if (showArr.length === 0 && this.__allowEmpty) {
            showArr = [NodomUI.getText('pleaseSelect')];
        }
        this.model['showText'] = showArr.join(',');
    }
    /**
     * 计算位置
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __cacLoc(model, dom, evObj, e) {
        if (!model.data) {
            return;
        }
        //let h = model.data.length * 30;
        let h = model.data.filter((item) => !item.__hidden).length * 30 + 14;
        if (h > this.__listHeight) {
            h = this.__listHeight;
        }
        const target = e.currentTarget;
        const pos = UITool.cacPosition(e, 1, target.clientWidth, h);
        model.__x = pos[0];
        model.__y = pos[1];
        model.__height = pos[3] + 2;
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender() {
        super.onBeforeFirstRender();
        const rows = this.model['data'];
        if (!rows) {
            return;
        }
        for (const m of rows) {
            m.__hidden = false;
        }
        //增加"请选择项"
        if (this.__field && this.__allowEmpty) {
            if (!this.model['__value']) {
                this.__initValue();
            }
        }
    }
}
//注册模块
Nodom.registModule(UISelect, 'ui-select');
//# sourceMappingURL=select.js.map