
import { Module } from '/dist/nodomui.min.js'
export class selectex3 extends Module {
    template() {

        return `
            <div>
                <p>选择的爱好是：{{hobby2}}</p>
                <label>爱好:</label>
                <ui-select
                    $data={{hobbies2}}
                    field='hobby2'
                    value-field='hid'
                    display-field='htitle'
                    allow-empty
                    multiple>
                    {{htitle}}
                </ui-select>
            </div>
        `
    }

    data() {
        return {
            hobby2: 2,
            hobbies2: this.genData()
        }
    }

    genData() {
        return [
            { hid: 1, htitle: "体育", icon: "message", desc: "体育运动很不错" },
            { hid: 2, htitle: "阅读", icon: "search", desc: "阅读让人睿智" },
            { hid: 3, htitle: "健身", icon: "home", disable: true, desc: "健身让人上瘾", disable: true },
            { hid: 4, htitle: "电脑游戏", icon: "user", desc: "好玩可别贪玩" },
            { hid: 5, htitle: "户外运动", icon: "password", desc: "感受大自然，轻呼吸", disable: false },
            { hid: 6, htitle: "美食", icon: "message", desc: "做一个努力的吃货" },
            { hid: 7, htitle: "旅游", icon: "search", desc: "美景使人流连忘返" }
        ]
    }
}