<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UICarouselItem](./nodomui.uicarouselitem.md) &gt; [template](./nodomui.uicarouselitem.template.md)

## UICarouselItem.template() method

模板函数

**Signature:**

```typescript
template(): string;
```
**Returns:**

string

