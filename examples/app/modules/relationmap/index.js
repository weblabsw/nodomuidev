import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import{EXCONFIG} from './config.js'
import{relationmapex1} from './ex1.js'

export class MRelationMap extends Module{
    modules=[ExModule,AttrGrid,EventGrid,MethodGrid,relationmapex1];
    template(props){
        return `
            <div class='--component'>
                <h1>Relationmap 关系图</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='relationmap'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{relationMapAttrs}} />

            </div>
        `
    }
    data(){
        return EXCONFIG;
    }
}