import { Module } from "nodom3";
export declare class UIAvatar extends Module {
    private __shape;
    private __src;
    private __alt;
    private __size;
    private __icon;
    private __method;
    template(props?: object): string;
}
