import { Module } from '/dist/nodomui.min.js'
export class stepsex2 extends Module {
    template(props) {
        return `
            <div >
                <ui-steps 
                    current={{current}} 
                    $data={{steps}}
                    success-select
                    >
                </ui-steps>    
            </div>
        `;
    }
    data() {
        return {
            current: 1,
            steps: [
                {
                    title: "Step 1",
                    desc: "This is a description."
                }, {
                    title: "Step 2",
                    desc: "This is a description."
                }, {
                    title: "Step 3",
                    desc: "This is a description."
                }
            ],
        }
    }
}