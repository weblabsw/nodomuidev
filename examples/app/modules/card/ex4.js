import { Module } from '/dist/nodomui.min.js'

export class cardex4 extends Module {
    template(props) {
        return `
                <div>
                    <div style="display: flex;justify-content: space-around;">
                        <ui-card width='200px' height='140px' shadow="always">
                            always
                        </ui-card>

                        <ui-card width='200px' height='140px' shadow="hover">
                            hover
                        </ui-card>

                        <ui-card width='200px' height='140px' shadow="never">
                            never
                        </ui-card>
                    </div>
                </div>

            `;
    }

    data() {
        return {
            rows: [
                { name: "Nodom", age: 6 },
                { name: "Noomi", age: 4 },
                { name: "Relaen", age: 3 }
            ]
        }
    }
}