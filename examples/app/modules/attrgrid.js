import {Module} from '/dist/nodomui.min.js'

export class AttrGrid extends Module{
    template(){
        return `
            <ui-grid $data={{rows}} grid-line='rows'>
                <ui-grid-col title='属性名' width='180'>{{name}}</ui-grid-col>
                <ui-grid-col title='说明' >{{desc}}</ui-grid-col>
                <ui-grid-col title='类型' width='100'>{{type}}</ui-grid-col>
                <ui-grid-col title='可选值' width='150'>{{values}}</ui-grid-col>
                <ui-grid-col title='默认值' width='100'>{{defaults}}</ui-grid-col>
            </ui-grid>
        `
    }
}