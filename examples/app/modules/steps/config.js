export const EXCONFIG = {
    name:'Step',
    desc:'使用ui-steps进行设置',
    stepsAttrs:[{
        name:'$data',
        desc:'数据项配置，必要属性：title(标题)；可选属性：desc(描述)，icon(图标)',
        type:'array',
        values:'-',
        defaults:'-'
    },{
        name:'direction',
        desc:'步骤条方向',
        type:'string',
        values:'horizontal,vertical',
        defaults:'horizontal'
    },{
        name:'current',
        desc:'设置当前步骤节点',
        type:'number',
        values:'-',
        defaults:'-'
    },{
        name:'finish-status',
        desc:'设置已完成步骤的状态',
        type:'string',
        values:'success,active',
        defaults:'success'
    },{
        name:'process-status',
        desc:'设置当前步骤的状态，如果值为error，则当前图标显示为"x"',
        type:'string',
        values:'success,active,error,warn',
        defaults:'-'
    },{
        name:'success-select',
        desc:'设置完成时步骤节点图标显示"√"，默认为数字',
        type:'-',
        values:'-',
        defaults:'-'
    }],
    examples:[{
        title:'基础用法',
        desc:'简单的带默认状态的步骤条',
        name:'ex1'
    },{
        title:'带详细描述的步骤条',
        desc:'数据项中属性desc设置，success-select设置完成时显示"√"',
        name:'ex2'
    },{
        title:'自定义状态',
        desc:'通过设置finish-status，process-status自定义完成时和当前状态的样式，process-status值为error时显示"×"',
        name:'ex3'
    },{
        title:'带图标的步骤条',
        desc:'步骤条内可以启用各种自定义的图标，通过数据项中属性icon进行设置，值参考icon教程',
        name:'ex4'
    },{
        title:'竖式步骤条',
        desc:'竖直方向的步骤条，通过direction参数设置，通过style设置竖式步骤条的高度',
        name:'ex5'
    }]
}