export const EXCONFIG = {
    desc: '使用ui-form组件和ui-form-item组件进行设置。',
    gridAttrs: [{
        name: 'label-width',
        desc: 'label所占宽度',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'unit-width',
        desc: '单位(m,m/s,...)所占宽度',
        type: 'string',
        values: '-',
        defaults: '-'
    }],
    gridColAttrs: [{
        name: 'label',
        desc: '输入项label',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'field',
        desc: '绑定字段',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: ' required',
        desc: '是否必填',
        type: 'boolean',
        values: 'true/false',
        defaults: 'false'
    }, {
        name: 'invalid-msg',
        desc: '校验失败消息，其中内置校验器有自己的消息，method方式也会返回错误消息，但是设置了invalidMsg，则其它消息失效',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'validator',
        desc: '校验器，支持内置校验器(参见UIValidator.rules)，regexp(正则表达式)，method(模块方法校验) 使用方式:validator="validatorName:param1:param2"，其中validatorName为内置校验器名，regexp和method  param1,param2为校验器参数，可选',
        type: 'any',
        values: '-',
        defaults: '-'
    }],
    gridMethods: [{
        name: '__addValidator',
        desc: '添加校验器',
        params: 'props',
        returns: '-'
    }, {
        name: '__verify',
        desc: '校验',
        params: ' 值',
        returns: 'true(通过)/false(失败)'
    }],
    examples: [{
        title: '简单应用',
        desc: '配合ui-input组件使用',
        name: 'ex1'
    }]

}
