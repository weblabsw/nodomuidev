import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
/**
 * $data：          列表数据数组
 * field:           绑定父模块的字段
 * value-field：    值字段名
 * disable-field：  禁用字段名
 */
/**
 * UIListTransfer 穿梭框
 * @public
 */
export class UIListTransfer extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__valueField = props['value-field'];
        this.__disableField = props['disable-field'];
        //删除多余属性
        this.setExcludeProps(['field', 'value-field', 'disable-field']);
        let disableCtx = '';
        if (this.__disableField) {
            disableCtx = "+ (" + this.__disableField + "?' ui-list-item-disable':'')";
        }
        return `
            <div class="ui-listtransfer">
                <div class='ui-list'>
                    <div x-repeat={{__getLeftRows()}} class={{'ui-list-item' + (__selected?' ui-list-item-active':'')  ${disableCtx} }} 
                        e-click='__clickItem'>
                        <div class='ui-list-itemcontent'>
                            <slot innerRender/>
                        </div>
                        <b class="ui-list-icon"></b>
                    </div>
                </div>
                <div class='ui-listtransfer-btngrp'>
                    <div>
                    <ui-button icon='double-arrow-right' e-click='__rightClick1'/>
                    <ui-button icon='arrow-right' e-click='__rightClick'/>
                    <ui-button icon='arrow-left' e-click='__leftClick'/>
                    <ui-button icon='double-arrow-left' e-click='__leftClick1'/>
                    </div>
                </div>
                <div class='ui-list'>
                    <div x-repeat={{__selectedRows}} class={{'ui-list-item' + (__selected?' ui-list-item-active':'')  ${disableCtx} }} 
                        e-click='__clickItem'>
                        <div class='ui-list-itemcontent'>
                            <slot innerRender/>
                        </div>
                        <b class="ui-list-icon"></b>
                    </div>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __selectedRows: []
        };
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender() {
        if (this.model['__value']) {
            this.__setValue(this.model['__value']);
        }
    }
    /**
     * 获取左边列表数据
     * @returns
     */
    __getLeftRows() {
        if (!this.model['data']) {
            return;
        }
        return this.model['data'].filter(item => !item.__valued);
    }
    /**
     * 点击item
     * @param model - 点击dom的model
     */
    __clickItem(model) {
        if (this.__disableField && model[this.__disableField]) {
            return;
        }
        model['__selected'] = !model['__selected'];
    }
    /**
     * 设置值
     * @param value - 需设置的值
     */
    __setValue(value) {
        const rows = this.model['data'];
        if (!this.__field || !Array.isArray(rows)) {
            return;
        }
        if (!Array.isArray(value)) {
            value = [value];
        }
        //值相同则不执行
        if (value.join(',') === this.__getSelectedValue().join(',')) {
            return;
        }
        for (const m of rows) {
            if (value.indexOf(m[this.__valueField]) !== -1) {
                this.__addSelect(m);
            }
            else {
                this.__removeSelect(m);
            }
        }
        this.__value = value;
    }
    /**
     * 把选中节点传递到右边
     */
    __rightClick() {
        if (!this.model['data']) {
            return;
        }
        this.model['data'].filter(item => item.__selected).forEach(r => {
            this.__addSelect(r);
        });
    }
    /**
     * 把所有节点传递到右边
     */
    __rightClick1() {
        if (!this.model['data']) {
            return;
        }
        this.model['data'].filter(item => (!this.__disableField || !item[this.__disableField]) && !item.__valued).forEach(r => {
            this.__addSelect(r);
        });
    }
    /**
     * 把选中节点传递到左边
     */
    __leftClick() {
        const rows = this.model['__selectedRows'].filter(item => item.__selected);
        if (rows) {
            for (let i = 0; i < rows.length; i++) {
                if (this.__removeSelect(rows[i])) {
                    i--;
                }
            }
        }
    }
    /**
     * 把所有节点传递到左边
     */
    __leftClick1() {
        for (let i = 0; i < this.model['__selectedRows'].length; i++) {
            if (this.__removeSelect(this.model['__selectedRows'][i])) {
                i--;
            }
        }
    }
    /**
     * 添加选中
     * @param m - 选中的model
     */
    __addSelect(m) {
        m.__valued = true;
        m.__selected = false;
        if (!this.model['__selectedRows'].find(item => item[this.__valueField] === m[this.__valueField])) {
            this.model['__selectedRows'].push(m);
            this.__updateValue();
        }
    }
    /**
     * 移除选中
     * @param m - 选中的model
     */
    __removeSelect(m) {
        m.__valued = false;
        m.__selected = false;
        const index = this.model['__selectedRows'].indexOf(m);
        if (index !== -1) {
            this.model['__selectedRows'].splice(index, 1);
            this.__updateValue();
            return true;
        }
        return false;
    }
    /**
     * 更新值
     * @returns
     */
    __updateValue() {
        if (!this.__valueField || !this.__field) {
            return;
        }
        const v = this.__getSelectedValue();
        //设置值
        this.set(this.srcDom.model, this.__field, v);
    }
    /**
     * 获取值
     * @returns 值数组
     */
    __getSelectedValue() {
        const v = [];
        for (const r of this.model['__selectedRows']) {
            v.push(r[this.__valueField]);
        }
        return v;
    }
}
Nodom.registModule(UIListTransfer, 'ui-listtransfer');
//# sourceMappingURL=listtransfer.js.map