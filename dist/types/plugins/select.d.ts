import { BaseInput } from "./baseinput";
/**
 * 配置说明
 * $data：              列表数据数组
 * field:               绑定父模块的字段
 * value-field：        值字段
 * disable-field：      禁用字段（表示记录项不可点击）
 * list-width:          下拉框宽度，默认为select宽度，单位px
 * list-height:         下拉框最大高度，单位px
 * allow-search          是否需要输入搜索
 */
/**
 * UISelect 下拉框
 * @public
 */
export declare class UISelect extends BaseInput {
    /**
     * 值field
     */
    private __valueField;
    /**
     * 显示字段
     */
    private __displayField;
    /**
     * 禁用字段名
     */
    private __disableField;
    /**
     * 多选
     */
    private __multiple;
    /**
     * 下拉框宽度，默认与插件宽度一致
     */
    private __listWidth;
    /**
     * 下拉框最大高度
     */
    private __listHeight;
    /**
     * 是否允许空选择
     */
    private __allowEmpty;
    /**
     * 是否允许搜索查值
     */
    private __allowSearch;
    /**
     * 改变select的是否隐藏值
     */
    private __hidden;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        __open: boolean;
        rows: any[];
        showText: string;
        __x: number;
        __y: number;
        __width: number;
        __height: number;
        __content: string;
    };
    /**
    * 选中后控制输入框
    * @param model - 对应model
    */
    private __changeInput;
    /**
     * 输入内容进行筛选
     * @param model -     模型
     * @param dom -       当前节点
     * @param ev -        event object
     * @param e -         html event
     */
    private __filterItem;
    /**
     * 交替 list box
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __toggleBox;
    /**
     * 打开list box
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __openBox;
    /**
     * 关闭list box
     */
    private __closeBox;
    /**
     * 点击item
     * @param model - 点击的item对应model
     * @returns
     */
    private __clickItem;
    /**
     * 设置值
     * @returns
     */
    protected __initValue(): void;
    /**
     * 计算位置
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __cacLoc;
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender(): void;
}
