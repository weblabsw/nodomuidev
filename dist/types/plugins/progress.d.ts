import { Module } from "nodom3";
/**
 * 参数说明
 * percent:       当前进度
 * format:        内容模板函数 `(percent) => text | HtmlElementString`
 * hide-text:     是否隐藏文本
 * type:          类型 line | circle
 * size:          尺寸 small | normal | large
 * stroke-color:  已完成进度条色
 * trail-color:   未完成进度条色
 */
/**
 * UIProgress 进度条
 * @public
 */
export declare class UIProgress extends Module {
    /**
     * 进度条类型
     */
    private __type;
    /**
     * 已完成进度条色
     */
    private __strokeColor;
    /**
     * 未完成进度条色
     */
    private __trailColor;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 获取圆圈样式
     * @param part -  左半或右办
     * @returns style 样式内容
     */
    private __getCircleStyle;
}
