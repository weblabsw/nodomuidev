
import { Module } from '/dist/nodomui.min.js'
export class sliderex2 extends Module {
    template(props) {
        return `
        <div>
            <span>拖动进度：{{outValue}}</span>
            <ui-button e-click="change" size="small" title="改变show-step状态" style="margin-bottom:10px;margin-left:20px"/>
            <ui-slider  field='outValue'  style="left:100px ;width:200px"  max={{10}} min={{0}} height={{200}} step={{1}} show-step={{showStep}}/>
        </div>
    `;
    }
    data() {
        return {
            outValue: 5,
            showStep : true,
        }
    }
    change(model){
        model['showStep'] = !model['showStep'];
    }
}