<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UIPanelBody](./nodomui.uipanelbody.md) &gt; [template](./nodomui.uipanelbody.template.md)

## UIPanelBody.template() method

模板函数

**Signature:**

```typescript
template(): string;
```
**Returns:**

string

