import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import{EXCONFIG} from './config.js'
import{badgeex1} from './ex1.js'
import{badgeex2} from './ex2.js'
import{badgeex3} from './ex3.js'
import{badgeex4} from './ex4.js'

export class MBadge extends Module{
    modules=[ExModule,AttrGrid,EventGrid,MethodGrid,badgeex1,badgeex2,badgeex3,badgeex4];
    template(props){
        return `
            <div class='--component'>
                <h1>Badge 标记</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='badge'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{badgeAttrs}} />
            </div>
        `
    }
    data(){
        return EXCONFIG;
    }
}