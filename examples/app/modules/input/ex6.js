import {Module} from "/dist/nodomui.min.js";
export class inputex6 extends Module{
    template(props) {
        return`
              <ui-input field='f6' type="textarea" placeholder='请输入' resize='both'/>
        `;
    }
    data(){
        return{
            f6:''
        }
    }
}