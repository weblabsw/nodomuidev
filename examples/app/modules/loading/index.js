import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { loadingex1 } from './ex1.js'
import { loadingex2 } from './ex2.js'
import { loadingex3 } from './ex3.js'
import { loadingex4 } from './ex4.js'
import { loadingex5 } from './ex5.js'
import { loadingex6 } from './ex6.js'
import { loadingex7 } from './ex7.js'

export class MLoading extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, loadingex1, loadingex2, loadingex3, loadingex4, loadingex5, loadingex6,loadingex7];
    template(props) {
        return `
            <div class='--component'>
                <h1>Loading 加载</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='loading'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{loadingAttrs}} />

                <h2>方法</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <MethodGrid $rows={{loadingMethods}} />

                <ui-loading color='red'/>
            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}