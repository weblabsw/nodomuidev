import { Module } from '/dist/nodomui.min.js'
export class drawerex3 extends Module {
    template(props) {
        return `
            <div>
                <ui-button e-click='openLeft' title="从左往右开"></ui-button>
                <ui-drawer title='drawer-left' closable direction='left' size="400px" onOpen='onOpen' onClose='onClose' onBeforeOpen='onBeforeOpen' onBeforeClose='onBeforeClose'>
                    <ui-drawer-body>
                        left to right
                    </ui-drawer-body>
                    <ui-button e-click='closeLeft' theme='error' title='关闭'/>
                </ui-drawer>
            </div>
        `
    }
    openLeft() {
        this.getModule('UIDrawer').__open();
    }
    closeLeft() {
        this.getModule('UIDrawer').__close();
    }

    onOpen(model, module) {
        console.log('open');
    }

    onBeforeOpen(model, module) {
        console.log('before open');
    }

    onBeforeClose(model, module) {
        console.log('before close');
    }

    onClose(model, module) {
        console.log('close');
    }
}