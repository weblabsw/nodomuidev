import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { sliderex1 } from './ex1.js'
import { sliderex2 } from './ex2.js'
import { sliderex3 } from './ex3.js'
import { sliderex4 } from "./ex4.js";
import { sliderex5 } from "./ex5.js";


export class MSlider extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, sliderex1, sliderex2, sliderex3, sliderex4, sliderex5];
    template(props) {
        return `
            <div class='--component'>
                <h1>Slider 滑动条</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='slider'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{sliderArr}} />


            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}