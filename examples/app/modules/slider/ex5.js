
import { Module } from '/dist/nodomui.min.js'
export class sliderex5 extends Module {

    template(props) {
        return `
            <div>
                <span>拖动进度：{{outValue}}</span>
                <ui-slider  field='outValue'  style="width:200px;left:100px" disabled min={{min}} max={{max}} step={{10}} />
            </div>
        `;
    }
    data() {
        return {
            outValue: 20,
            min: 0,
            max: 80
        }
    }
}