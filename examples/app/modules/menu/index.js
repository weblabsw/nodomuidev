import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import{EXCONFIG} from './config.js'
import{menuex1} from './ex1.js'
import{menuex2} from './ex2.js'
import{menuex3} from './ex3.js'
import {menuex4} from "./ex4.js";


export class MMenu extends Module{
    modules=[ExModule,AttrGrid,EventGrid,MethodGrid,menuex1,menuex2,menuex3,menuex4];
    template(props){
        return `
            <div class='--component'>
                <h1>Menu 菜单</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='menu'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{menuAttrs}} />


                <h2>事件</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <EventGrid $rows={{menuEvents}} />

            </div>
        `
    }
    data(){
        return EXCONFIG;
    }
}